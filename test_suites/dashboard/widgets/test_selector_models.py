from typing import List

from shyft.dashboard.base.ports import connect_ports, Receiver
from shyft.dashboard.base.selector_presenter import SelectorPresenter
from shyft.dashboard.base.selector_views import MultiSelect, Select
from shyft.dashboard.widgets.selector_models import LabelDataSelector, LabelData, LabelDataSelectorClickPolicy


def test_label_data_selector_send_selected_policy():

    # on click policy SEND_SELECTED
    labels = ["A", "B", "C", "D", "E", "F", "G"]

    view = MultiSelect(title='Available Labels', size=10, width=300)
    label_presenter = SelectorPresenter(name='Available Labels', view=view)
    label_selector = LabelDataSelector(presenter=label_presenter,
                                       on_click_policy=LabelDataSelectorClickPolicy.SEND_SELECTED)

    call_count = [0]

    def _receive_selection_to_assert(selection: LabelData):
        assert selection == ['A']
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='_receive_selection_to_assert',
                                           func=_receive_selection_to_assert, signal_type=LabelData)
    # connect our function to selector model
    connect_ports(label_selector.send_selected_labels, receive_selection_to_assert)

    label_selector.receive_labels(["K"])
    assert label_selector.presenter.selector_options == ['', 'K']

    label_selector.receive_labels_to_add(labels)
    assert label_selector.presenter.selector_options == ['', "A", "B", "C", "D", "E", "F", "G", 'K']

    # trigger the selection
    label_presenter.set_selector_value(['A'], callback=True)
    assert call_count[0] == 1


def test_label_data_selector_remove_selected_policy():
    # on click policy REMOVE_SELECTED_AND_SEND_REST
    labels = ["A", "B", "C", "D", "E", "F", "G"]

    view = MultiSelect(title='Available Labels', size=10, width=300)
    label_presenter = SelectorPresenter(name='Available Labels', view=view)
    label_selector = LabelDataSelector(presenter=label_presenter,
                                       on_click_policy=LabelDataSelectorClickPolicy.REMOVE_SELECTED_AND_SEND_REST)

    call_count = [0]

    def _receive_selection_to_assert(selection: LabelData):
        assert selection == ["A", "B", "C", "D", "E", "F", "G"]
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert_selection', func=_receive_selection_to_assert, signal_type=LabelData)

    label_selector.receive_labels(["K"])
    assert label_selector.presenter.selector_options == ['', 'K']

    label_selector.receive_labels_to_add(labels)
    assert label_selector.presenter.selector_options == ['', "A", "B", "C", "D", "E", "F", "G", 'K']

    # connect our function to selector model
    connect_ports(label_selector.send_labels, receive_selection_to_assert)

    # trigger the selection
    label_presenter.set_selector_value(['K'], callback=True)
    assert call_count[0] == 1


def test_run_tag_selector_basics():
    available_tags = ["Banana", "Foo", "Bar", "Dragon", "Dragon"]
    selected_tag = "Banana"

    # check RunTagClickPolicy.SEND_SELECTED
    available_tags_view = Select(title='Tags')
    available_tags_presenter = SelectorPresenter(name='Tags', view=available_tags_view)
    available_tags_selector = LabelDataSelector(presenter=available_tags_presenter,
                                                on_click_policy=LabelDataSelectorClickPolicy.SEND_SELECTED)
    call_count = [0]

    def _receive_selection_to_assert(tags: List[str]):
        assert isinstance(tags, list)
        assert len(tags) == 1
        assert tags[0] == selected_tag
        call_count[0] += 1

    # create an assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert_run', func=_receive_selection_to_assert, signal_type=List[str])
    # connect our function to selector model
    connect_ports(available_tags_selector.send_selected_labels, receive_selection_to_assert)

    # add run matches
    available_tags_selector.receive_labels(available_tags)
    # check if set correctly
    assert set(available_tags_selector.presenter.selector_options).symmetric_difference(set(available_tags)) == {available_tags_presenter.default}

    # set selection
    available_tags_presenter.set_selector_value([selected_tag], callback=True)
    assert call_count[0] == 1

    available_tags_selector.receive_labels_to_add(['Coconut'])
    assert set(available_tags_selector.presenter.selector_options).symmetric_difference(set(available_tags)) == {
    available_tags_presenter.default, 'Coconut'}

    ###############

    call_count = [0]
    # Check RunTagClickPolicy.REMOVE_SELECTED_AND_SEND_REST
    selected_tags_view = Select(title='Tags')
    selected_tags_presenter = SelectorPresenter(name='Tags', view=available_tags_view)
    selected_tags_selector = LabelDataSelector(presenter=selected_tags_presenter,
                                               on_click_policy=LabelDataSelectorClickPolicy.REMOVE_SELECTED_AND_SEND_REST)

    def _receive_selection_to_assert2(tags: List[str]):
        assert isinstance(tags, list)
        assert set(available_tags).symmetric_difference(tags) == {selected_tag}
        call_count[0] += 1

    # create an assert port
    receive_selection_to_assert2 = Receiver(parent='parent', name='assert_run', func=_receive_selection_to_assert2, signal_type=List[str])
    # connect our function to selector model
    connect_ports(selected_tags_selector.send_labels, receive_selection_to_assert2)

    # add run matches
    selected_tags_selector.receive_labels(available_tags)
    # check if set correctly
    assert set(selected_tags_selector.presenter.selector_options).symmetric_difference(set(available_tags)) == {selected_tags_presenter.default}

    # set selection
    selected_tags_presenter.set_selector_value([selected_tag], callback=True)
    assert call_count[0] == 1
