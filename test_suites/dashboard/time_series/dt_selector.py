import shyft.dashboard as b
print(b)

from shyft.dashboard.time_series.dt_selector import dt_to_str


from shyft.time_series import Calendar


def dt_to_string():

    assert dt_to_str(Calendar.YEAR) == 'Year'
    assert dt_to_str(Calendar.QUATER) == 'Quater'
    assert dt_to_str(Calendar.MONTH) == 'Month'
    assert dt_to_str(Calendar.WEEK) == 'Week'
    assert dt_to_str(Calendar.DAY) == 'Day'
    assert dt_to_str(Calendar.HOUR) == 'Hour'
    assert dt_to_str(Calendar.MINUTE) == 'Minute'
    assert dt_to_str(1) == 'Second'

    assert dt_to_str(2*Calendar.YEAR) == '2 Years'
    assert dt_to_str(2*Calendar.QUATER) == '2 Quaters'
    assert dt_to_str(2*Calendar.MONTH) == '2 Months'
    assert dt_to_str(2*Calendar.WEEK) == '2 Weeks'
    assert dt_to_str(2*Calendar.DAY) == '2 Days'
    assert dt_to_str(2*Calendar.HOUR) == '2 Hours'
    assert dt_to_str(2*Calendar.MINUTE) == '2 Minutes'
    assert dt_to_str(2) == '2 Seconds'

