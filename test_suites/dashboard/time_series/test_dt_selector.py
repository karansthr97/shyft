from shyft.dashboard.time_series.dt_selector import dt_to_str


from shyft.time_series import Calendar


def test_dt_to_string():

    assert dt_to_str(Calendar.YEAR) == '1 Year'
    assert dt_to_str(Calendar.MONTH*3) == '1 Quarter'
    assert dt_to_str(Calendar.MONTH) == '1 Month'
    assert dt_to_str(Calendar.WEEK) == '1 Week'
    assert dt_to_str(Calendar.DAY) == '1 Day'
    assert dt_to_str(Calendar.HOUR) == '1 Hour'
    assert dt_to_str(Calendar.MINUTE) == '1 Minute'
    assert dt_to_str(1) == '1 Second'

    assert dt_to_str(2*Calendar.YEAR) == '2 Years'
    assert dt_to_str(2*Calendar.MONTH*3) == '2 Quarters'
    assert dt_to_str(2*Calendar.MONTH) == '2 Months'
    assert dt_to_str(2*Calendar.WEEK) == '2 Weeks'
    assert dt_to_str(2*Calendar.DAY) == '2 Days'
    assert dt_to_str(2*Calendar.HOUR) == '2 Hours'
    assert dt_to_str(2*Calendar.MINUTE) == '2 Minutes'
    assert dt_to_str(2) == '2 Seconds'

