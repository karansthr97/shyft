from concurrent.futures.thread import ThreadPoolExecutor


import pytest
from shyft.time_series import TimeAxis, TsVector, point_interpretation_policy, TimeSeries, DoubleVector, Calendar,time

from shyft.dashboard.time_series.renderer import BaseFigureRenderer
from shyft.dashboard.time_series.state import State
from shyft.dashboard.time_series.ts_viewer import TsViewer, TsViewerError
from shyft.dashboard.time_series.sources.source import DataSource, Source, TsAdapterRequestParameter
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle
from shyft.dashboard.time_series.view import Line, FigureView
from shyft.dashboard.time_series.tools.ts_viewer_tools import ResetTool
from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.view_container.figure import Figure
from shyft.dashboard.time_series.view_container.table import Table
from shyft.dashboard.time_series.bindable import BindableError
from shyft.dashboard.time_series.axes_handler import DsViewTimeAxisType
from shyft.dashboard.time_series.view_time_axes import ViewTimeAxisProperties


class MockTimeAxisHandler:
    def __init__(self):
        view_time_axis = TimeAxis(60, 60, 11)
        padded_view_time_axis = TimeAxis(0, 60, 12)
        self.view_time_axis_properties = ViewTimeAxisProperties(dt=time(60), cal=Calendar(),
                                                           view_period=view_time_axis.total_period(),
                                                           padded_view_period=padded_view_time_axis.total_period(),
                                                           extend_mode=False)


class MockViewContainer:
    def __init__(self):
        self.view_data = {}
        self.bound = None
        self.state_port = None

    def update_view_data(self, view_data):
        self.view_data = view_data

    def bind(self, parent):
        pass

    def unbind(self):
        pass

    def set_unit_registry(self, unit_registry):
        pass


def test_ts_viewer_init(mock_bokeh_document):

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer", tools=[ResetTool()])

    # assert viewer._state == States.DEACTIVE
    # assert viewer.time_axis_handler._state == States.DEACTIVE
    # assert viewer.view_time_axis._state == States.DEACTIVE


def test_ds_view_handles(mock_bokeh_document, test_ts_line):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    ds_view_handles = []
    figure = Figure(viewer=viewer)
    for _ in range(2):
        data_source = DataSource(ts_adapter=test_ts_line, unit='MW')
        fig_view = Line(unit='W', color='blue', label='test', view_container=figure, index=0)
        ds_view_handles.append(DsViewHandle(data_source=data_source, views=[fig_view]))
    viewer.add_ds_view_handles(ds_view_handles=ds_view_handles)
    assert len(viewer.ds_view_handles) == 2
    viewer.remove_ds_view_handles(ds_view_handles=[ds_view_handles[1]])
    assert len(viewer.ds_view_handles) == 1
    with pytest.raises(TsViewerError):
        viewer.add_ds_view_handles(ds_view_handles=[ds_view_handles[0]])


def test_tools(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert not viewer.tools
    viewer.add_tool(tool=ResetTool())
    assert viewer.tools


def test_figure_state(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert viewer._state == States.ACTIVE
    assert viewer._receive_state(States.DEACTIVE) is None
    assert viewer._state == States.DEACTIVE
    assert viewer._receive_state(States.ACTIVE) is None
    assert viewer._state == States.ACTIVE
    assert viewer._receive_state(States.LOADING) is None
    assert viewer._receive_state(States.READY) is None
    assert viewer._state == States.ACTIVE
    viewer._receive_state(States.ACTIVE)
    viewer.clear()
    assert viewer._state == States.DEACTIVE


def test_add_and_remove_view_container(mock_bokeh_document):

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    figure = Figure(viewer=viewer)
    table = Table(viewer=viewer)

    assert figure.parent == viewer
    assert table.parent == viewer

    with pytest.raises(BindableError):
        viewer.add_view_container(figure)

    viewer.remove_view_container(table)
    assert table not in viewer.view_container

    with pytest.raises(BindableError):
        viewer.remove_view_container(table)


def test_auto_dt_figure_width(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert viewer.time_axis_handler.auto_dt_figure_width == viewer.time_axis_handler.default_auto_dt_width
    Figure(viewer=viewer, width=1500)
    assert viewer.time_axis_handler.auto_dt_figure_width == 1500
    Figure(viewer=viewer, width=500)
    assert viewer.time_axis_handler.auto_dt_figure_width == 500


def test_add_remove_view_container(mock_bokeh_document, test_ts_line):
    ts_viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    ts_viewer.time_axis_handler = MockTimeAxisHandler()
    ts_viewer.should_wait_async_view_data = True
    assert len(ts_viewer.view_container) == 0
    assert len(ts_viewer.collected_async_view_data) == 0

    view_container = MockViewContainer()
    view_container.state_port = ts_viewer.state_port
    view_container.parent = ts_viewer
    ts_viewer.add_view_container(view_container)
    assert len(ts_viewer.view_container) == 1
    assert len(ts_viewer.collected_async_view_data) == 1

    view_container_2 = MockViewContainer()
    view_container_2.state_port = ts_viewer.state_port
    view_container_2.parent = ts_viewer
    ts_viewer.add_view_container(view_container_2)
    assert len(ts_viewer.view_container) == 2
    assert len(ts_viewer.collected_async_view_data) == 2

    ts_viewer.remove_view_container(view_container_2)
    assert len(ts_viewer.view_container) == 1
    assert len(ts_viewer.collected_async_view_data) == 1

    ts_viewer.remove_view_container(view_container)
    assert len(ts_viewer.view_container) == 0
    assert len(ts_viewer.collected_async_view_data) == 0


def create_data_source_with_view(mock_bokeh_document, test_ts_line, thread_pool_executor, view_container):
    data_source = DataSource(ts_adapter=test_ts_line, unit='MW',
                                     request_time_axis_type=DsViewTimeAxisType.view_time_axis)
    view = FigureView(view_container=view_container, color='blue', visible=True, label='Katze',
                      renderer_class=BaseFigureRenderer, unit="MW",
                      )
    source = Source(bokeh_document=mock_bokeh_document, data_source=data_source, views=[view],
                    unit_registry=State.unit_registry, thread_pool_executor=thread_pool_executor)
    return data_source, view, source


def test_trigger_view_update(mock_bokeh_document, test_ts_line):
    thread_pool_executor = ThreadPoolExecutor(1)
    ts_viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    ts_viewer.time_axis_handler = MockTimeAxisHandler()
    view_container = MockViewContainer()
    view_container.state_port = ts_viewer.state_port
    view_container.parent = ts_viewer
    data_source, view, source = create_data_source_with_view(mock_bokeh_document, test_ts_line,
                                                                  thread_pool_executor, view_container)
    request_param = source._make_request_parameter(ts_viewer.time_axis_handler.view_time_axis_properties)
    ts_vector = data_source.ts_adapter(**request_param.request_parameter)
    ts_viewer.sources[data_source] = source
    ts_viewer.add_view_container(view_container)

    # -------------------------- should_wait_async_view_data = False
    ts_viewer.should_wait_async_view_data = False
    assert len(view_container.view_data) == 0
    ts_viewer.trigger_view_update({view: ts_vector})
    # view_data is sent to view_container when trigger_view_update() is called
    assert len(view_container.view_data) == 1
    assert view in view_container.view_data.keys()

    _, view_2, _ = create_data_source_with_view(mock_bokeh_document, test_ts_line,
                                                             thread_pool_executor, view_container)
    ts_viewer.trigger_view_update({view_2: ts_vector})
    # the second view_data is sent to view_container when trigger_view_update() is called
    assert len(view_container.view_data) == 1
    assert view_2 in view_container.view_data.keys()
    ts_viewer.remove_view_container(view_container)

    # -------------------------- should_wait_async_view_data = True
    view_container.view_data.clear()
    ts_viewer.should_wait_async_view_data = True
    ts_viewer.add_view_container(view_container)
    ts_viewer.source_starting_async_data_update(source)
    ts_viewer.trigger_view_update({view: ts_vector})
    # view_data is not sent to view_container yet. It is stored in ts_viewer.collected_async_view_data
    assert len(view_container.view_data) == 0
    assert len(ts_viewer.collected_async_view_data[view_container]) == 1
    # source_completed_async_data_update() must send all collected view_data to the view_container
    ts_viewer.source_completed_async_data_update(source)
    assert len(view_container.view_data) == 1
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    assert view in view_container.view_data.keys()

    # -------------------------- should_wait_async_view_data = True with multiple source
    view_container.view_data.clear()
    ts_viewer.should_wait_async_view_data = True
    ts_viewer.add_view_container(view_container)
    # source registers itself to ts_viewer as it starts data update
    ts_viewer.source_starting_async_data_update(source)
    assert ts_viewer.sources_async_data_update_running == [source]
    ts_viewer.trigger_view_update({view: ts_vector})
    # view_data is not sent to view_container yet. It is stored in ts_viewer.collected_async_view_data
    assert len(view_container.view_data) == 0
    assert len(ts_viewer.collected_async_view_data[view_container]) == 1
    _, view_2, source_2 = create_data_source_with_view(mock_bokeh_document, test_ts_line,
                                                             thread_pool_executor, view_container)
    # the second source registers itself to ts_viewer as it starts data update
    ts_viewer.source_starting_async_data_update(source_2)
    assert ts_viewer.sources_async_data_update_running == [source, source_2]
    ts_viewer.trigger_view_update({view_2: ts_vector})
    assert len(view_container.view_data) == 0
    assert len(ts_viewer.collected_async_view_data[view_container]) == 2

    # assume source_2 has completed first. It calls ts_viewer.source_completed_async_data_update()
    ts_viewer.source_completed_async_data_update(source_2)
    # Nothing is sent to view_container since the first source hasn't completed yet.
    assert ts_viewer.sources_async_data_update_running == [source]
    assert len(view_container.view_data) == 0
    assert len(ts_viewer.collected_async_view_data[view_container]) == 2

    # When second source is also complete, all collected view_data is sent to view_container.
    ts_viewer.source_completed_async_data_update(source)
    assert ts_viewer.sources_async_data_update_running == []
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    assert len(view_container.view_data) == 2
    assert view in view_container.view_data.keys()
    assert view_2 in view_container.view_data.keys()


def test_trigger_data_update(mock_bokeh_document, test_ts_line):
    """ This test covers trigger_data_update() and trigger_view_update() """
    thread_pool_executor = ThreadPoolExecutor(1)
    ts_viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    view_container = MockViewContainer()
    view_container.state_port = ts_viewer.state_port
    data_source_view_ts = DataSource(ts_adapter=test_ts_line, unit='MW',
                                     request_time_axis_type=DsViewTimeAxisType.view_time_axis)
    view = FigureView(view_container=view_container, color='blue', visible=True, label='Katze',
                      renderer_class=BaseFigureRenderer, unit="MW",
                      )
    source = Source(bokeh_document=mock_bokeh_document, data_source=data_source_view_ts, views=[view],
                    unit_registry=State.unit_registry, thread_pool_executor=thread_pool_executor)
    source.bind(parent=ts_viewer)
    ts_viewer.sources[data_source_view_ts] = source
    ts_viewer.should_wait_async_view_data = True
    ts_viewer.time_axis_handler = MockTimeAxisHandler()
    ts_viewer.add_view_container(view_container)

    assert source.async_observer_ts_viewer is None
    assert ts_viewer.sources_async_data_update_running == []
    assert list(ts_viewer.collected_async_view_data.keys()) == [view_container]

    # --------------------------
    ts_viewer.trigger_data_update()
    assert source.async_observer_ts_viewer == ts_viewer
    assert ts_viewer.sources_async_data_update_running == [source]
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    assert len(view_container.view_data) == 0

    assert mock_bokeh_document.next_tick()
    assert ts_viewer.sources_async_data_update_running == [source]
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    assert len(view_container.view_data) == 0

    assert mock_bokeh_document.next_tick()
    assert ts_viewer.sources_async_data_update_running == []
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    assert len(view_container.view_data) == 1

    # -------------------------- Scenario with 2 sources -------
    view_container.view_data.clear()
    source.current_request_parameter = TsAdapterRequestParameter.create_empty()
    data_source_view_ts_2 = DataSource(ts_adapter=test_ts_line, unit='MW',
                                     request_time_axis_type=DsViewTimeAxisType.view_time_axis)
    view_2 = FigureView(view_container=view_container, color='blue', visible=True, label='Katze',
                      renderer_class=BaseFigureRenderer, unit="MW")
    source_2 = Source(bokeh_document=mock_bokeh_document, data_source=data_source_view_ts_2, views=[view_2],
                    unit_registry=State.unit_registry, thread_pool_executor=thread_pool_executor)
    source_2.bind(parent=ts_viewer)

    ts_viewer.trigger_data_update()
    assert source.async_observer_ts_viewer == ts_viewer
    # Source is expected to register its "running" state to ts_viewer...
    assert ts_viewer.sources_async_data_update_running == [source]

    assert mock_bokeh_document.next_tick()
    assert ts_viewer.sources_async_data_update_running == [source]
    # simulation of a second source getting started while the first source is still processing...
    ts_viewer.sources[data_source_view_ts_2] = source_2
    ts_viewer.trigger_data_update()
    # both sources are still processing
    assert ts_viewer.sources_async_data_update_running == [source, source_2]
    assert source_2.async_observer_ts_viewer == ts_viewer

    assert mock_bokeh_document.next_tick()
    # the first source is completed. only source_2 is left
    assert ts_viewer.sources_async_data_update_running == [source_2]
    # view_data for the first source is ready and stored in ts_viewer.collected_async_view_data
    assert len(ts_viewer.collected_async_view_data[view_container]) == 1
    # since source_2 is not complete, nothing is sent to view_container yet
    assert len(view_container.view_data) == 0

    assert mock_bokeh_document.next_tick()
    assert ts_viewer.sources_async_data_update_running == [source_2]
    assert len(ts_viewer.collected_async_view_data[view_container]) == 1
    assert len(view_container.view_data) == 0

    assert mock_bokeh_document.next_tick()
    # source_2 is also complete. all stacks are cleared in ts_viewer, and view_container receives the updated view_data.
    assert ts_viewer.sources_async_data_update_running == []
    assert len(ts_viewer.collected_async_view_data[view_container]) == 0
    # view_container has 2 view_data from 2 sources.
    assert len(view_container.view_data) == 2
