import pytest
from shyft.dashboard.time_series.bindable import Bindable, BindableToMany, BindableError


def test_bindable():

    class Foo(Bindable):

        def __init__(self):
            super().__init__()
            pass

    class Bar(Bindable):

        def __init__(self):
            super().__init__()
            self.bound_counter = 0

        def on_bind(self, *, parent):
            self.bound_counter += 1

        def on_unbind(self, *, parent):
            self.bound_counter -= 1

    p = Foo()
    c = Bar()

    assert not c.bound
    assert c.parent is None
    assert c.bound_counter == 0

    # bind c
    c.bind(parent=p)

    assert c.bound
    assert c.parent is p
    assert c.bound_counter == 1

    with pytest.raises(BindableError):
        c.bind(parent=p)

    # unbind c
    c.unbind()

    assert not c.bound
    assert c.parent is None
    assert c.bound_counter == 0


def test_bindable_to_many():

    class Foo(BindableToMany):

        def __init__(self):
            super().__init__()

    class Bar(BindableToMany):

        def __init__(self, *, parent_limit=None):
            super().__init__(parent_limit=parent_limit)
            self.bound_counter = 0

        def on_bind(self, *, parent):
            self.bound_counter += 1

        def on_unbind(self, *, parent):
            self.bound_counter -= 1

    child = Bar()
    parent_imposter = Foo()
    parents = [Foo() for _ in range(4)]

    assert not child.bound
    for parent in parents:
        assert not parent.bound
        assert not parent.parents

    for i, parent in enumerate(parents):
        child.bind(parent=parent)
        assert child.bound_counter == i + 1

    for parent in parents:
        with pytest.raises(BindableError):
            child.bind(parent=parent)

    for parent in parents:
        assert not parent.bound

    assert len(child.parents) == len(parents)
    child.unbind(parent=parent_imposter)
    assert child.bound_counter == len(parents)
    assert len(child.parents) == len(parents)
    child.unbind(parent=parents[1])
    assert len(child.parents) == len(parents) - 1
    assert child.bound_counter == len(parents) - 1

    for parent in child.parents:
        assert parent != parents[1]

    parents = [Foo() for _ in range(2)]
    child = Bar(parent_limit=1)
    child.bind(parent=parents[0])
    with pytest.raises(BindableError):
        child.bind(parent=parents[1])
