from datetime import datetime

from shyft.time_series import Calendar

# from shyft.dashboard.test.time_series.test_time_series_fixtures import mock_bokeh_document
# from shyft.dashboard.test.base.test_base_fixtures import logger_and_list_handler

import numpy as np
from shyft.time_series import UtcPeriod, TimeSeries, DoubleVector, point_interpretation_policy, TimeAxis, TsVector

from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.state import State, Unit
from shyft.dashboard.time_series.ts_viewer import TsViewer
# from shyft.dashboard.time_series.axes_handler import BokehViewTimeAxis
from shyft.dashboard.time_series.view_container.figure import Figure
from shyft.dashboard.time_series.renderer import LineRenderer, FillInBetweenRenderer
from shyft.dashboard.time_series.view import Line, FillInBetween, DiamondScatter, SquareScatter, TriangleScatter, \
    CircleScatter
from shyft.dashboard.time_series.tools.figure_tools import FigureTool
from shyft.dashboard.time_series.axes import YAxis


def test_figure_init(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    figure_tool = FigureTool()
    yaxis = YAxis(label="test axis", unit="MW")
    figure = Figure(viewer=viewer, y_axes=yaxis, tools=figure_tool)
    assert yaxis in figure.y_axes
    assert len(figure.tools) == 1
    figure.add_tool(FigureTool())
    assert len(figure.tools) == 2


def test_renderers_line(mock_bokeh_document, logger_and_list_handler):
    # logger, log_list = logger_and_list_handler

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    view_range = UtcPeriod(0, 11)
    # view_time_axis = BokehViewTimeAxis(bokeh_document=mock_bokeh_document, init_view_range=init_view_range)
    figure = Figure(viewer=viewer)  # , shared_x_range=view_time_axis.shared_x_range)
    print('\n')

    figure.set_title(title="changed")
    assert figure.title == "changed"

    new_renderers = figure.generate_renderers(renderer_type=LineRenderer)

    assert len(new_renderers) == 1
    assert len(figure.idle_renderer) == 1

    class MockViewContainer:
        def __init__(self):
            self.update_count: int = 0

        def update_y_range(self):
            self.update_count += 1

    vc = MockViewContainer()
    line_view = Line(view_container=vc, color='blue', label='Test view', unit='NOK',
                     index=0)
    figure.add_view(view=line_view)
    assert len(list(figure.renderers.keys())) == 1
    assert len(figure.idle_renderer) == 0

    figure.clear_views()
    assert len(list(figure.renderers.keys())) == 0
    assert len(figure.idle_renderer) == 1

    figure.add_view(view=line_view)
    renderer = list(figure.renderers.values())[0]
    np.testing.assert_array_equal(renderer.y_range(view_range=view_range), np.asarray([np.nan, np.nan]))
    ta = TimeAxis(view_range.start, 1, 10)
    ts = TimeSeries(ta, DoubleVector.from_numpy(np.linspace(0, 10, 10)),
                    point_interpretation_policy.POINT_INSTANT_VALUE)
    tsv = State.unit_registry.Quantity(TsVector([ts]), "NOK")
    renderer.update_view_data(ts_vector=tsv)
    assert tsv == renderer.ts_vector
    min_y, max_y = renderer.y_range(view_range=view_range)
    assert min_y == 0
    assert max_y == 10

    assert figure.draw_figure(y_axis=list(figure.renderers.values())[0].y_axis) is None
    assert figure.update_view_data(view_data={line_view: tsv}) is None
    figure.views[0].visible = False
    assert not list(figure.renderers.values())[0].visible


def test_renderers(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    view_range = UtcPeriod(0, 11)
    figure = Figure(viewer=viewer)
    fill_inbetween = FillInBetween(view_container=figure, color="black", label="test percentile",
                                   unit="MW", indices=(0, 0))
    figure.add_view(view=fill_inbetween)
    renderer = list(figure.renderers.values())[0]
    renderer.y_range(view_range=view_range)
    ta = TimeAxis(view_range.start, 1, 3)
    ts = TimeSeries(ta, DoubleVector.from_numpy(np.linspace(0, 10, 3)),
                    point_interpretation_policy.POINT_INSTANT_VALUE)
    tsv = State.unit_registry.Quantity(TsVector([ts]), "MW")
    renderer.update_view_data(ts_vector=tsv)
    assert len(renderer.bokeh_data_source.data["t"][0]), 2*len(ts.v)
    for i in range(len(ts.v)):
        assert renderer.bokeh_data_source.data["t"][0][i] == renderer.bokeh_data_source.data["t"][0][-i - 1]
    assert renderer.bokeh_data_source.data["color"][0] == "black"
    fill_inbetween.color = "white"
    assert renderer.bokeh_data_source.data["color"][0] == "white"


def test_datetime_renderers(mock_bokeh_document):
    t0 = Calendar().time(2019, 1, 1, 0, 0, 0)
    ta = TimeAxis(t0, 1, 3)
    ts = TimeSeries(ta, 1.0, point_interpretation_policy.POINT_INSTANT_VALUE)
    line_data = State.unit_registry.Quantity(TsVector([ts]), "MW")
    time_str = '2019-01-01 00:00:00'

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert viewer.time_zone == 'UTC'
    figure = Figure(viewer=viewer)

    # Line view
    line_view = Line(view_container=figure, color='blue', label='Line', unit='MW', index=0)
    figure.add_view(view=line_view)
    figure.update_view_data(view_data={line_view: line_data})
    renderer = figure.renderers[line_view]

    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0][0], datetime)
    #assert str(renderer.bokeh_data_source.data['t'][0][0]) == time_str
    assert renderer.bokeh_data_source.data['t'][0][0] == float(t0)*1000

    # Fill in between view
    fill_view = FillInBetween(view_container=figure, color="black", label="FillIn", unit="MW", indices=(0, 0))
    figure.add_view(view=fill_view)
    figure.update_view_data(view_data={fill_view: line_data})

    renderer = figure.renderers[fill_view]
    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0][0], datetime)
    assert renderer.bokeh_data_source.data['t'][0][0] == t0*1000

    # Diamond scatter view
    scatter_view = DiamondScatter(view_container=figure, color='red', label='Diamond', unit='MW', index=0)
    figure.add_view(view=scatter_view)
    figure.update_view_data(view_data={scatter_view: line_data})
    renderer = figure.renderers[scatter_view]

    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0], datetime)
    assert renderer.bokeh_data_source.data['t'][0]== t0 * 1000
    #assert str(renderer.bokeh_data_source.data['t'][0]) == time_str

    # Diamond scatter view
    scatter_view = SquareScatter(view_container=figure, color='red', label='Diamond', unit='MW', index=0)
    figure.add_view(view=scatter_view)
    figure.update_view_data(view_data={scatter_view: line_data})
    renderer = figure.renderers[scatter_view]

    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0], datetime)
    #assert str(renderer.bokeh_data_source.data['t'][0]) == time_str
    assert renderer.bokeh_data_source.data['t'][0] == t0*1000

    # Diamond scatter view
    scatter_view = TriangleScatter(view_container=figure, color='red', label='Diamond', unit='MW', index=0)
    figure.add_view(view=scatter_view)
    figure.update_view_data(view_data={scatter_view: line_data})
    renderer = figure.renderers[scatter_view]

    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0], datetime)
    #assert str(renderer.bokeh_data_source.data['t'][0]) == time_str
    assert renderer.bokeh_data_source.data['t'][0] == t0 * 1000

    # Diamond scatter view
    scatter_view = CircleScatter(view_container=figure, color='red', label='Diamond', unit='MW', index=0)
    figure.add_view(view=scatter_view)
    figure.update_view_data(view_data={scatter_view: line_data})
    renderer = figure.renderers[scatter_view]

    assert renderer.time_zone == 'UTC'
    #assert isinstance(renderer.bokeh_data_source.data['t'][0], datetime)
    #assert str(renderer.bokeh_data_source.data['t'][0]) == time_str
    assert renderer.bokeh_data_source.data['t'][0] == t0*1000


def test_datetime_renderer_with_utc_offset(mock_bokeh_document):
    time_zone = 'Europe/Oslo'
    t0 = Calendar().time(2018, 7, 1, 0, 0, 0)
    time_str = '2018-07-01 02:00:00'
    cal = Calendar(time_zone)
    ta = TimeAxis(t0, 1, 3)
    ts = TimeSeries(ta, 1.0, point_interpretation_policy.POINT_INSTANT_VALUE)
    line_data = State.unit_registry.Quantity(TsVector([ts]), "MW")

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer", time_zone=time_zone)
    assert viewer.time_zone == time_zone
    figure = Figure(viewer=viewer)

    line_view = Line(view_container=figure, color='blue', label='Test', unit='MW', index=0)
    figure.add_view(view=line_view)
    figure.update_view_data(view_data={line_view: line_data})
    renderer = figure.renderers[line_view]

    assert renderer.time_zone == time_zone
    #assert isinstance(renderer.bokeh_data_source.data['t'][0][0], datetime)
    #assert str(renderer.bokeh_data_source.data['t'][0][0]) == time_str
    assert renderer.bokeh_data_source.data['t'][0][0]/1000.0 == t0+ cal.tz_info.utc_offset(t0)

    #cc = renderer.bokeh_data_source.data['t'][0][0]  # this is calendar coordinates, we know it should be osl
    #assert cal.time(cc.year, cc.month, cc.day, cc.hour, cc.minute, cc.second) == t0


def test_figure_state(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    title = "test figure"
    figure = Figure(viewer=viewer, title=title)

    assert figure._state == States.ACTIVE
    assert figure._receive_state(States.DEACTIVE) is None
    assert figure._state == States.DEACTIVE
    assert figure._receive_state(States.ACTIVE) is None
    assert figure._state == States.ACTIVE
    assert figure._receive_state(States.LOADING) is None
    assert figure.bokeh_figure.title.text == ' '.join([figure.title, "LOADING DATA ..."])
    assert figure._receive_state(States.READY) is None
    assert figure._state == States.ACTIVE
    assert figure.bokeh_figure.background_fill_color == 'white'


def test_figure_init_renderer(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    title = "test figure"
    init_renderers = {LineRenderer: 4, FillInBetweenRenderer: 2}
    figure = Figure(viewer=viewer, title=title, init_renderers=init_renderers)
    assert len(figure.idle_renderer) == 6
    n_in_between = 0
    n_line = 0
    for r in figure.idle_renderer:
        if isinstance(r, LineRenderer):
            n_line += 1
        if isinstance(r, FillInBetweenRenderer):
            n_in_between += 1
    assert n_in_between == 2
    assert n_line == 4


def test_figure_font_size(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    figure = Figure(viewer=viewer, title_text_font_size=10)
    figure._receive_fontsize(15)
    assert figure.bokeh_figure.title.text_font_size == '15pt'
