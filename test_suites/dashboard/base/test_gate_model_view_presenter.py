from bokeh.models import (Button, Panel, Row, Tabs, CheckboxGroup, Toggle)
from bokeh.events import ButtonClick
import pytest
from shyft.dashboard.base.gate_presenter import GatePresenter
from shyft.dashboard.base.ports import Sender, Receiver, connect_ports
from shyft.dashboard.base.gate_model import SingleGate



def test_gate_presenter_with_button_view():
    call_count = [0]
    send_10_port = Sender(parent='send_10', name='send 10 port', signal_type=int)

    def _receive_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    receive_10 = Receiver(parent=_receive_10, name='_receive_10', func=_receive_10, signal_type=int)

    # create view for the gate, for example a button here
    button = Button(label='Activate Gate 10', width=20)
    # create gate
    gate10 = SingleGate(sender=send_10_port, receiver=receive_10, connect_function=connect_ports)
    # create gate presenter, connecting button and gate
    button_gate_presenter = GatePresenter(view=button, gates=[gate10])

    # send a number
    send_10_port(10)
    # nothing should be received
    assert call_count[0] == 0
    # close the gate by clicking on the button
    # TODO: verify that test works with new bokeh version, button.clicks not propagating changes
    button._trigger_event(ButtonClick(button))  # sih: replaces obsolete .clicks = 1
    # now number should be received
    assert call_count[0] == 1


def test_gate_presenter_with_tabs_view():
    call_count_20 = [0]
    call_count_30 = [0]
    send_20_port = Sender(parent='send 20', name='send 20 port', signal_type=int)
    send_30_port = Sender(parent='send_30', name='send 30 port', signal_type=int)

    def _receive_20(number: int) -> None:
        assert number == 20
        call_count_20[0] += 1

    receive_20 = Receiver(parent=_receive_20, name='_recieve_20', func=_receive_20, signal_type=int)

    def _receive_30(number: int) -> None:
        assert number == 30
        call_count_30[0] += 1

    receive_30 = Receiver(parent=_receive_30, name='_recieve_30', func=_receive_30, signal_type=int)

    # create tabs view for the gate, with active send 20
    tab1 = Panel(child=Row(), title="Send 20")
    tab2 = Panel(child=Row(), title="Send 30")
    tabs = Tabs(tabs=[tab1, tab2], width=500, active=0)

    gate20 = SingleGate(sender=send_20_port, receiver=receive_20, buffer=False)
    gate30 = SingleGate(sender=send_30_port, receiver=receive_30, buffer=False)

    tabs_gate_presenter = GatePresenter(view=tabs, gates=[gate20, gate30])  # assign 1 gate per tab

    # send numbers
    send_20_port(20)
    send_30_port(30)
    # tabs 20 should have received a value tabs 30 not
    assert call_count_20[0] == 1
    assert call_count_30[0] == 0
    # open tab 30
    tabs.active = 1
    # nothing changed since we habe no use_buffer in gates
    assert call_count_20[0] == 1
    assert call_count_30[0] == 0
    # send numbers
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 1
    assert call_count_30[0] == 1


def test_gate_presenter_with_CheckboxGroup_view():
    call_count_20 = [0]
    call_count_30 = [0]
    send_20_port = Sender(parent='send 20', name='send 20 port', signal_type=int)
    send_30_port = Sender(parent='send_30', name='send 30 port', signal_type=int)

    def _receive_20(number: int) -> None:
        assert number == 20
        call_count_20[0] += 1

    receive_20 = Receiver(parent=_receive_20, name='_recieve_20', func=_receive_20, signal_type=int)

    def _receive_30(number: int) -> None:
        assert number == 30
        call_count_30[0] += 1

    receive_30 = Receiver(parent=_receive_30, name='_recieve_30', func=_receive_30, signal_type=int)

    # --- View: RadioButtonGroup, RadioGroup, CheckboxGroup, CheckboxButtonGroup ---
    # create view for gates: one of theses is possible here:
    # try it out! RadioButtonGroup, RadioGroup, CheckboxGroup, CheckboxButtonGroup
    check_box = CheckboxGroup(labels=['Activate Gate 40', 'Activate Gate 50'], active=[])

    # for this gates we set the use_buffer=True, but clear_buffer_after_send on True, thus the use_buffer will be send only once!
    gate40 = SingleGate(sender=send_20_port, receiver=receive_20, connect_function=connect_ports, buffer=False,
                        clear_buffer_after_send=True)
    gate50 = SingleGate(sender=send_30_port, receiver=receive_30, connect_function=connect_ports, buffer=False,
                        clear_buffer_after_send=True)

    check_box_presenter = GatePresenter(view=check_box, gates=[gate40, gate50])

    # send numbers
    send_20_port(20)
    send_30_port(30)
    # tabs 20 should have received a value tabs 30 not
    assert call_count_20[0] == 0
    assert call_count_30[0] == 0
    # open tab 30
    check_box.active = [1]
    # nothing changed since we habe no use_buffer in gates
    assert call_count_20[0] == 0
    assert call_count_30[0] == 0
    # send numbers
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 0
    assert call_count_30[0] == 1
    # activate the other one
    check_box.active = [0]
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 1
    assert call_count_30[0] == 1
    # close both
    check_box.active = []
    send_20_port(20)
    send_30_port(30)
    assert call_count_20[0] == 1
    assert call_count_30[0] == 1


def test_gate_presenter_with_toggle_view():
    call_count = [0]
    send_10_port = Sender(parent='send_10', name='send 10 port', signal_type=int)

    def _receive_10(number: int) -> None:
        assert number == 10
        call_count[0] += 1

    receive_10 = Receiver(parent=_receive_10, name='_receive_10', func=_receive_10, signal_type=int)
    # create view for the gate, for example a button here

    toggle = Toggle(label='Activate Gate 60', width=20)

    # for this gates we set the use_buffer=False,
    gate60 = SingleGate(sender=send_10_port, receiver=receive_10, connect_function=connect_ports,
                        buffer=False, clear_buffer_after_send=False)

    toggle_gate_presenter = GatePresenter(view=toggle, gates=[gate60])

    # send a number
    send_10_port(10)
    # nothing should be received
    assert call_count[0] == 0
    # close the gate by clicking on the button
    toggle.active = True
    # now number should be received
    assert call_count[0] == 0
    send_10_port(10)
    assert call_count[0] == 1
    send_10_port(10)
    assert call_count[0] == 2
    toggle.active = False
    send_10_port(10)
    assert call_count[0] == 2
