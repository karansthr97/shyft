import math

from shyft.time_series import utctime_now, deltahours, TimeAxisFixedDeltaT, TimeSeries, point_interpretation_policy, extend_split_policy, extend_fill_policy, TimeAxis, POINT_AVERAGE_VALUE, Calendar, POINT_INSTANT_VALUE


def test_ts_extend():
    t0 = utctime_now()
    dt = deltahours(1)
    n = 512
    ta_a = TimeAxisFixedDeltaT(t0, dt, 2*n)
    ta_b = TimeAxisFixedDeltaT(t0 + n*dt, dt, 2*n)
    ta_c = TimeAxisFixedDeltaT(t0 + 2*n*dt, dt, 2*n)
    ta_d = TimeAxisFixedDeltaT(t0 + 3*n*dt, dt, 2*n)

    a = TimeSeries(ta=ta_a, fill_value=1.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    b = TimeSeries(ta=ta_b, fill_value=2.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    c = TimeSeries(ta=ta_c, fill_value=4.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    d = TimeSeries(ta=ta_d, fill_value=8.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)

    # default behavior: extend from end of a
    ac = a.extend(c)

    for i in range(2*n):  # valus from first ts
        assert ac(t0 + i*dt) == 1.0
    for i in range(2*n):  # values from extension ts
        assert ac(t0 + (i + 2*n)*dt) == 4.0

    # default behavior: extend from end of a, fill gap with nan
    ad = a.extend(d)

    for i in range(2*n):  # values from first
        assert ad(t0 + i*dt) == 1.0
    for i in range(n):  # gap
        assert math.isnan(ad(t0 + (i + 2*n)*dt))
    for i in range(2*n):  # extension
        assert ad(t0 + (i + 3*n)*dt) == 8.0

    # split at the first value of d instead of last of c
    cd = c.extend(d, split_policy=extend_split_policy.RHS_FIRST)

    for i in range(n):  # first, only until the extension start
        assert cd(t0 + (2*n + i)*dt) == 4.0
    for i in range(2*n):  # extension
        assert cd(t0 + (3*n + i)*dt) == 8.0

    # split at a given time step, and extend the last value through the gap
    ac = a.extend(c, split_policy=extend_split_policy.AT_VALUE, split_at=(t0 + dt*n//2),
                  fill_policy=extend_fill_policy.USE_LAST)

    for i in range(n//2):  # first, only until the given split value
        assert ac(t0 + i*dt) == 1.0
    for i in range(3*n//2):  # gap, uses last value before gap
        assert ac(t0 + (n//2 + i)*dt) == 1.0
    for i in range(2*n):  # extension
        assert ac(t0 + (2*n + i)*dt) == 4.0

    # split at the beginning of the ts to extend when the extension start before it
    cb = c.extend(b, split_policy=extend_split_policy.AT_VALUE, split_at=(t0 + 2*n*dt))

    for i in range(n):  # don't extend before
        assert math.isnan(cb(t0 + (n + i)*dt))
    for i in range(n):  # we split at the beginning => only values from extension
        assert cb(t0 + (2*n + i)*dt) == 2.0
    for i in range(n):  # no values after extension
        assert math.isnan(cb(t0 + (3*n + i)*dt))

    # extend with ts starting after the end, fill the gap with a given value
    ad = a.extend(d, fill_policy=extend_fill_policy.FILL_VALUE, fill_value=5.5)

    for i in range(2*n):  # first
        assert ad(t0 + i*dt) == 1.0
    for i in range(n):  # gap, filled with 5.5
        assert ad(t0 + (2*n + i)*dt) == 5.5
    for i in range(2*n):  # extension
        assert ad(t0 + (3*n + i)*dt) == 8.0

    # check extend with more exotic combination of time-axis(we had an issue with this..)
    a = TimeSeries(TimeAxis(0, 1, 10), fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    b = TimeSeries(TimeAxis(Calendar(), 0, 1, 20), fill_value=2.0, point_fx=POINT_AVERAGE_VALUE)
    ab = a.extend(b)
    ba = b.extend(a, split_policy=extend_split_policy.AT_VALUE, split_at=a.time_axis.time(5))
    assert round(abs(ab.value(0) - 1.0), 7) == 0
    assert round(abs(ab.value(11) - 2.0), 7) == 0
    assert round(abs(ba.value(0) - 2.0), 7) == 0
    assert round(abs(ab.value(7) - 1.0), 7) == 0


def test_ts_extend_with_points_fail():
    # testcase from issue https://github.com/statkraft/shyft/issues/414
    # reported by Tobias:
    # Extend fails when extender and extendee time_axis do not overlap:
    tsp_1_fail = TimeSeries(TimeAxis([0, 1, 1.5]), [1, 2], POINT_INSTANT_VALUE)
    tsp_2_fail = TimeSeries(TimeAxis([2, 3, 4]), [3, 4], POINT_INSTANT_VALUE)
    extend_fail = tsp_1_fail.extend(tsp_2_fail)
    assert len(extend_fail.time_axis) == 5
    tsp_1 = TimeSeries(TimeAxis([0, 1, 2]), [1, 2], POINT_INSTANT_VALUE)
    tsp_2 = TimeSeries(TimeAxis([2, 3, 4]), [3, 4], POINT_INSTANT_VALUE)

    extend = tsp_1.extend(tsp_2)
    assert len(extend.time_axis) == 4


def test_ts_extend_exact_at_end():
    a = TimeSeries(TimeAxis([0, 1, 1.5]), [1, 2], POINT_INSTANT_VALUE)
    b = TimeSeries(TimeAxis([1.5, 3, 4]), [3, 4], POINT_INSTANT_VALUE)
    r = a.extend(b)
    assert r is not None
    assert r.values is not None
    assert len(r.time_axis) == 4
