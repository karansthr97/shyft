from shyft.time_series import Calendar, time, TimeAxis, TimeSeries, POINT_AVERAGE_VALUE as stair_case


def test_mix_1():
    ta_1 = TimeAxis(time('2019-01-01T00:00:00Z'), Calendar.MONTH, 1)
    ta_2 = TimeAxis(time('2018-12-31T00:00:00Z'), Calendar.WEEK, 4)
    ta_3 = TimeAxis(time('2018-12-31T00:00:00Z'), 4*Calendar.WEEK, 1)
    a = TimeSeries(ta_1, fill_value=1, point_fx=stair_case)
    b = TimeSeries(ta_2, fill_value=2, point_fx=stair_case)
    c = TimeSeries(ta_3, fill_value=3, point_fx=stair_case)
    x = a - b
    y = c - b
    assert x == TimeSeries(TimeAxis([ta_1.time(0), ta_2.time(1), ta_2.time(2), ta_2.time(3)], ta_2.total_period().end), fill_value=-1, point_fx=stair_case)
    assert y == TimeSeries(ta_2, fill_value=1.0, point_fx=stair_case)
