import pytest
from .utility import create_model_with_hydro
from shyft.energy_market.core.model_repository import ModelRepository
from shyft.energy_market.core import create_model_service
from shyft.time_series import utctime_now
import tempfile


def test_create_read_delete():
    """
    Run a test-story where we use all the functions of the repository
    :return:
    """
    with tempfile.TemporaryDirectory() as mdir:
        # first check that zero models give zero back
        ms = ModelRepository(mdir)
        model_list = ms.get_model_infos()
        assert model_list is not None
        assert len(model_list) == 0

        # create one model and verify we get it listed
        m = create_model_with_hydro(name='test_one')
        m_id = ms.save_model(m)
        assert m_id > 0
        model_list = ms.get_model_infos()
        assert len(model_list) == 1
        mi = model_list[0]
        assert mi.id == 1
        assert mi.name == 'test_one'
        assert abs(int(utctime_now()) - mi.created) < 100

        # now read the model and compare with original
        mr = ms.get_model(m_id)
        assert mr.equal_structure(m)
        assert mr.area[1].detailed_hydro is not None
        mr_list = ms.get_models([m_id])  # also test reading by list work
        assert len(mr_list) == 1

        # then remove the model and verify it's away
        ms.delete_model(m_id)
        model_list = ms.get_model_infos()
        assert len(model_list) == 0


def test_create_model_service():
    with tempfile.TemporaryDirectory() as mdir:
        ms = create_model_service(mdir)  # works on existing directory
    with pytest.raises(RuntimeError):
        create_model_service(r'dir_does_not_exist')  # fail on non-existing
    with pytest.raises(RuntimeError):
        create_model_service(__file__)  # file should also fail
