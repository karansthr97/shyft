from shyft.energy_market.core import Model, Gate
from shyft.energy_market.core import HydroPowerSystem, ConnectionRole


def create_hydro_power_system(*, hps_id: int = 1, name: str = 'ulla-førre') -> HydroPowerSystem:
    """
    Demonstrates how to build an inmemory representation of  HydroPowerSystem
    corresponding to the term 'detailed-hydro' in EMPS,
    using part of the blåsjø/ulla-førre systems

    Returns
    -------
    HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
    """
    hps = HydroPowerSystem(hps_id, name)
    blasjo = hps.create_reservoir(16606, 'blåsjø')
    saurdal = hps.create_aggregate(1, 'saurdal')
    saurdal_ps = hps.create_power_station(1, 'saurdal ps')
    saurdal_ps.add_aggregate(saurdal)

    tunx = hps.create_tunnel(1, 'blåsjø-saurdal')
    blasjo.output_to(tunx, ConnectionRole.main)
    saurdal.input_from(tunx)
    sandsavatn = hps.create_reservoir(16602, 'sandsvatn')
    lauvastolsvatn = hps.create_reservoir(16603, 'lauvastølsvatn')
    kvilldal = hps.create_aggregate(165061, 'kvilldal', 'info {Ek=1.3,520m,4x310 MW max,260 m3/s, utl?p=70moh}')
    kvilldal2 = hps.create_aggregate(165062, 'kvilldal2')

    t_kvilldal = hps.create_tunnel(100, 'kvilldal hovedtunnel', 'alpha=0.000053 m/(m3/s)^2')
    kvilldal_penstock_1 = hps.create_tunnel(101, 'kvilldal penstock 1', 'alpha=0.000053 m/(m3/s)^2')
    kvilldal_penstock_2 = hps.create_tunnel(102, 'kvilldal penstock 2', 'alpha=0.000053 m/(m3/s)^2')
    t_saur_kvill = hps.create_tunnel(103, 'saurdal-kvilldal-hoved-tunnel')
    t_sandsa_kvill = hps.create_tunnel(104, 'sandsavatn-til-kvilldal')
    t_lauvas_kvill = hps.create_tunnel(105, 'lauvastølsvatn-til-kvilldal')
    g0 = Gate(1, "Gate1", "{type:'binary'}")
    t_lauvas_kvill.add_gate(g0)
    assert len(t_lauvas_kvill.gates) == 1
    assert g0.water_route.name == t_lauvas_kvill.name
    t_saur_kvill.input_from(saurdal).output_to(t_kvilldal)
    t_kvilldal.output_to(kvilldal_penstock_1)
    t_kvilldal.output_to(kvilldal_penstock_2)
    kvilldal_penstock_1.output_to(kvilldal)
    kvilldal_penstock_2.output_to(kvilldal2)

    t_sandsa_kvill.input_from(sandsavatn).output_to(t_kvilldal)
    t_lauvas_kvill.input_from(lauvastolsvatn).output_to(t_kvilldal)

    vassbotvatn = hps.create_reservoir(106, 'vassbotvatn')
    stoelsdal_pumpe = hps.create_aggregate(16510, 'stølsdal pumpe', 'pump-curve[[120,2.1],[145,1.6],[151,1.5]], p_avg=6MW')

    hps.create_tunnel(107, 'stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra') \
        .input_from(stoelsdal_pumpe) \
        .output_to(vassbotvatn)
    hps.create_tunnel(108, 'fra sandsvatn til stølsdal pump') \
        .input_from(sandsavatn) \
        .output_to(stoelsdal_pumpe)

    suldalsvatn = hps.create_reservoir(16500, 'suldalsvatn')
    hylen = hps.create_aggregate(16501, 'hylen', "info {uid:16508', '2x80MW,2x95m3/s, Ek=0.165, utl?ps=0.0moh, nom fallh=66m }")
    hps.create_river(200, 'fra kvilldal til suldalsvatn', 'info {max_cap:260 m3/s}') \
        .input_from(kvilldal) \
        .input_from(kvilldal2) \
        .output_to(suldalsvatn)

    hps.create_tunnel(1107, 'hylen-tunnel', 'info {max_cap:275 m3/s}') \
        .input_from(suldalsvatn) \
        .output_to(hylen)

    havet = hps.create_reservoir(1, 'havet')

    hps.create_river(1108, 'utløp hylen', 'info {max=275 m3/s, alpha=0.000003 m/(m3/s)^2}') \
        .input_from(hylen) \
        .output_to(havet)

    hps.create_river(1109, 'bypass suldal til havet', 'info {maxcap:=30}') \
        .input_from(suldalsvatn, ConnectionRole.bypass) \
        .output_to(havet)

    hps.create_river(1110, 'flom suldal til havet', 'info {max_cap:10000}') \
        .input_from(suldalsvatn, ConnectionRole.flood) \
        .output_to(havet)
    return hps


def create_model(name='m1'):
    """
    Create and return a test model for an EMPS system with areas, transmission lines, and within the
     model areas also power-modules.
     Used as a skeleton for testing routines below.
    """
    m = Model(1, name)

    a1 = m.create_model_area(1, 'a1')
    a2 = m.create_model_area(2, 'a2')
    a3 = m.create_model_area(3, 'a3')
    a1.create_power_module(11, "production.nuclear")  # .id = 11
    a1.create_power_module(12, "consumption.temperature dependent")  # .id = 12
    a1.create_power_module(13, "export.russia")  # .id = 13
    a1.create_power_module(14, "wind.kjøllefjord")  # .id = 14

    a1.create_power_module(15, "load")  # .id = 15
    a1.create_power_module(16, "powertype")  # .id = 16
    a2.create_power_module(39, "powertype2")  # .id = 39
    a1.create_power_module(17, "tempprofile")  # .id = 17
    a1.create_power_module(122, "tempprofile_2")  # .id = 112

    a1.create_power_module(18, "revision")  # .id = 18
    a1.create_power_module(19, "fleks")  # .id = 19

    a1.create_power_module(20, "kraftvarme")  # .id = 20

    a2.create_power_module(21, "production.hydro")  # ".id = 21
    a2.create_power_module(22, "consumption.temperature dependent")  # ".id = 22
    a2.create_power_module(23, "tempprofile_3")  # .id = 23
    a2.create_power_module(26, "tempprofile_4")  # .id = 26
    a2.create_power_module(24, "kraftvarme2")  # .id = 24
    a2.create_power_module(25, "kraftvarme3")  # .id = 25
    a3.create_power_module(31, "production.bio")  # .id = 31
    a3.create_power_module(32, "production.solar")  # .id = 32
    a3.create_power_module(33, "production.coal.new")  # .id = 33
    a3.create_power_module(34, "import.denmark")  # .id = 34
    a3.power_modules[34].other_json_data = "Something extra for this one"

    a1.create_power_module(111, "price_sensitive_powertype")  # .id = 111
    pl1 = m.create_power_line(a1, a2, 1, "1_A1-A2")
    pl2 = m.create_power_line(a2, a3, 2, "2_A2-A3")
    pl3 = m.create_power_line(a2, a1, 3, "3_A2-A1")
    # a1.detailed_hydro = create_hydro_power_system(hps_id=1, name='a1.hps')
    # a2.detailed_hydro = create_hydro_power_system(hps_id=2, name='a2.hps')
    return m


def create_model_with_hydro(name='m1'):
    """

    :param name:
    :return:
    """
    m = create_model(name)
    m.area[1].detailed_hydro = create_hydro_power_system(hps_id=1, name=f'{name}.detailed_hydro')
    return m


def create_model2(name='m2'):
    """
    Create and return a model with real EMPS data for an EMPS system with areas, transmission lines, and within the
     model areas also power-modules.
    """
    m = Model(name)

    a1 = m.create_model_area(1, 'REG1NORD')
    a2 = m.create_model_area(2, 'SORLAND')
    a3 = m.create_model_area(3, 'VESTSYD')
    a4 = m.create_model_area(4, 'VESTMIDT')
    a12 = m.create_model_area(12, 'DANM-VEST')
    a1.create_power_module(1, "alm forsyning q1", 'L_6')  # .id = 1
    a2.create_power_module(2, "INDUSTRI", 'L_2')  # .id = 2
    a2.create_power_module(3, 'NO2_ASEN2', 'L_3')  # .id = 3

    a1.create_power_module(4, "GJENKJOP_q1_1", 'P_4')  # .id = 4
    a3.create_power_module(5, "UGARANT IND 50", 'P_5')  # .id = 5

    a1.create_power_module(61, "CONS_CORR_WARM_WTR", 'P_61')  # .id = 61
    a3.create_power_module(998, "FLOMKRAFT", 'P_998')  # .id = 998

    a12.create_power_module(6, "KV SENTRAL VINTER", 'P_10')  # .id = 6

    pl1 = m.create_power_line(a2, a1, 1, "1_SORLAND-REG1NORD")
    pl2 = m.create_power_line(a1, a2, 2, "2_REG1NORD-SORLAND")
    pl3 = m.create_power_line(a3, a2, 3, "3_VESTSYD-SORLAND")
    pl4 = m.create_power_line(a2, a3, 4, "4_SORLAND-VESTSYD")
    a1.detailed_hydro = create_hydro_power_system(hps_id=1, name='SORLAND.hps')
    return m


def create_model_with_hydro2(name='m2'):
    """

    :param name:
    :return:
    """
    m = create_model2(name)
    m.area['SORLAND'].detailed_hydro = create_hydro_power_system(hps_id=111, name='SORLAND.detailed_hydro')
    return m
