from .models import create_test_hydro_power_system
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, StmRun, ModelState, SharedMutex
from shyft.energy_market.stm import t_xy
from shyft.energy_market.core import Point, PointList, XyPointCurve
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE


def test_can_create_hps():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)


# TODO: .. important for validity of other tests.
# def test_equal_structure():
#    pass  # not started yet
# a = create_hydro_power_system(hps_id=1, name='a')
# b = create_hydro_power_system(hps_id=2, name='b')
# assert a.equal_structure(b)


def test_serialize_and_deserialize_blob():
    a = create_test_hydro_power_system(hps_id=1, name='hps')
    assert a is not None
    a_blob = a.to_blob()
    assert len(a_blob) > 0
    b = HydroPowerSystem.from_blob(a_blob)
    assert b is not None
    assert b.equal_structure(a)


def test_can_set_rsv_volume_descr():
    a = HydroPowerSystem(1, 'a')
    a.create_ids()
    r = a.create_reservoir(1, "x", "json{'a':1}")
    assert not r.volume_descr.exists, 'no volume_descr set yet'
    v = t_xy()
    v[time('2018-10-17T00:00:00Z')] = XyPointCurve(PointList([Point(0.0, 0.0), Point(10.0, 10.0)]))
    r.volume_descr.value = v
    assert r.volume_descr.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    b_r = b.reservoirs[0]
    assert b_r.volume_descr.exists


def test_rsv_plan_mm3():
    a = HydroPowerSystem(1, 'a')
    a.create_ids()
    r = a.create_reservoir(1, "x", "json{'a':1}")
    assert not r.level_schedule.exists, 'no level_schedule set yet'
    r.level_schedule.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_INSTANT_VALUE)
    assert r.level_schedule.exists
    a_blob = a.to_blob()
    b = HydroPowerSystem.from_blob(a_blob)
    b_r = b.reservoirs[0]
    assert b_r.level_schedule.exists


def test_can_create_stm_system():
    a = StmSystem(1, "A", "json{}")
    assert a
    assert a.id == 1
    assert a.name == 'A'
    assert a.json == 'json{}'
    assert len(a.hydro_power_systems) == 0
    assert len(a.market_areas) == 0


def test_can_blobify_stm_system():
    """ verify serialization roundtrip for blob"""
    a = StmSystem(1, "A", "json{}")
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.id == a.id
    assert b.name == a.name
    assert b.json == a.json
    assert len(b.hydro_power_systems) == len(a.hydro_power_systems)
    assert len(b.market_areas) == len(a.market_areas)

def test_stm_system_proxy_attributes():
    """ Verify the proxy attributes in StmSystem"""
    a = StmSystem(1, "A", "{misc}")
    rp = a.run_parameters
    assert not rp.n_inc_runs.exists
    assert not rp.run_time_axis.exists
    rp.n_inc_runs.value = 2
    rp.run_time_axis.value = TimeAxis(time('2018-01-01T00:00:00Z'), time(3600), 240)
    assert rp.n_inc_runs.exists
    assert rp.n_inc_runs.value == 2
    assert rp.run_time_axis.exists
    a_blob = a.to_blob()
    b = StmSystem.from_blob(a_blob)
    assert b
    assert b.run_parameters.n_inc_runs.value == rp.n_inc_runs.value
    assert b.run_parameters.run_time_axis.value == rp.run_time_axis.value

def test_hps_can_find_by_name():
    s = create_test_hydro_power_system(hps_id=1, name='hps')
    w=s.find_waterway_by_id(1)
    assert w and 'saurdal' in w.name
    g1 = w.add_gate(1,'tunx_g1') # we can add gate to a model
    assert g1
    assert not g1.opening_schedule.exists, 'check that we got stm gate'
    p=s.find_power_plant_by_id(1)
    r=s.find_reservoir_by_id(1)
    u=s.find_unit_by_id(165061)
    assert p and r and u
    assert p.id == s.find_power_plant_by_name(p.name).id
    assert r.id == s.find_reservoir_by_name(r.name).id
    assert u.id == s.find_unit_by_name(u.name).id
    # ensure we got real stm objects back
    assert not p.outlet_level.exists
    assert not r.hrl.exists
    assert not u.production.exists
    assert not w.discharge.exists


def test_stm_system_context():
    s = StmSystem(1, "A", "")
    ctx = StmRun(ModelState.IDLE, s)
    assert hasattr(ctx, "state")
    assert ctx.state == ModelState.IDLE
    assert hasattr(ctx, "system")
    assert s.name == ctx.system.name
    assert hasattr(ctx, "mutex")
    assert isinstance(ctx.mutex, SharedMutex)



