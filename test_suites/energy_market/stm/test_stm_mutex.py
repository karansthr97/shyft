from shyft.energy_market import stm
import asyncio
import pytest
import sys


if sys.version_info < (3, 7):
    pytest.skip("requires python3.6 or higher",allow_module_level=True)


async def read_insert(mtx, alist):
    await asyncio.sleep(0.1)
    with stm.ReadAccess(mtx):
        alist.append("read")
    pass


async def write_insert(mtx, alist):
    with stm.WriteAccess(mtx):
        await asyncio.sleep(0.1)
        alist.append("write")


async def read_and_write(mtx, alist):
    await asyncio.gather(write_insert(mtx, alist), read_insert(mtx, alist))


async def read_then_write(mtx, alist):
    with stm.UpgradableAccess(mtx) as rl:
        alist.append("first")
        await asyncio.sleep(0.1)
        with stm.WriteAccess(rl):
            alist.append("second")


async def upgrade_lock(mtx, alist):
    await asyncio.gather(read_insert(mtx, alist), read_then_write(mtx, alist))


def test_mutex():
    """
    Basic functionality for read- and write access to a variable associated with a mutex
    """
    alist = []
    mtx = stm.SharedMutex()
    asyncio.run(read_and_write(mtx, alist))
    assert len(alist) == 2
    assert alist[0] == "write"
    assert alist[1] == "read"


def test_upgrade_lock():
    alist = []
    mtx = stm.SharedMutex()
    asyncio.run(upgrade_lock(mtx, alist))
    assert len(alist) == 3
    assert alist[0] == "first"
    assert alist[1] == "read"
    assert alist[2] == "second"
