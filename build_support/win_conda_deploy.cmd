@echo off
@rem simple conda build/upload script that
@rem a) set version
@rem b) conda build 
@rem c) anaconda upload, force user SHYFT_OS_ANACONDA_USER, default shyft-os, using token SHYFT_OS_ANACONDA_TOKEN
@rem preconditions: shyft is built and tested
@SETLOCAL ENABLEEXTENSIONS
@setlocal EnableDelayedExpansion
pushd "%~dp0"\..

@for /f "tokens=* USEBACKQ" %%x in (`python -c "import numpy;print(numpy.version.version[:-2])"`) do @set np_version=%%x
@for /f "tokens=* USEBACKQ" %%x in (`python -c "from sysconfig import get_python_version;print(get_python_version())"`)do @set py_version=%%x

set /p SHYFT_VERSION=<VERSION
@rem args are special in dos as well
@set arg_x=%1
@set cmd_x=%2

@set label_name=%arg_x:master=main%
@set git_branch=%arg_x%
if [%SHYFT_DEPENDENCIES_DIR%]==[] (
    set SHYFT_DEPENDENCIES_DIR="%~dp0"\..\shyft_dependencies
    echo WARNING: SHYFT_DEPENDENCIES_DIR was not set, using default %SHYFT_DEPENDENCIES_DIR%
)

if [%cmd_x%]==[local_dist] (
    @echo Local install the package to local dependency dir using py/py-version/shyft_version scheme
    @echo Also creates the symbolic link to most recent version of %git_branch% to %SHYFT_VERSION%
    python setup.py bdist_wheel --dist-dir=dist/%shyft_version%
	if not %errorlevel%==0 goto finale
    pip install --no-deps --no-index --upgrade --target %SHYFT_DEPENDENCIES_DIR%/py/%py_version%/%SHYFT_VERSION% --find-links=dist/%shyft_version% shyft
	if not %errorlevel%==0 goto finale
    if exist "%SHYFT_DEPENDENCIES_DIR%/py/%py_version%/%git_branch%" (
        rmdir "%SHYFT_DEPENDENCIES_DIR%/py/%py_version%/%git_branch%"
    )
    mklink /J "%SHYFT_DEPENDENCIES_DIR%/py/%py_version%/%git_branch%" "%SHYFT_DEPENDENCIES_DIR%/py/%py_version%/%SHYFT_VERSION%"
	goto finale
)

if [%cmd_x%]==[build] (
    @echo Starting to build %label_name% shyft.time_series  %SHYFT_VERSION% py %py_version% numpy %np_version%
    call conda build --python=%py_version% --numpy %np_version% --no-test --no-copy-test-source-files --no-anaconda-upload --no-activate --no-verify --output-folder dist/conda --label %label_name% conda_recipe\time_series
	if not %errorlevel%==0 goto finale
    call conda build --python=%py_version% --numpy %np_version% --no-test --no-copy-test-source-files --no-anaconda-upload --no-activate --no-verify --output-folder dist/conda --label %label_name% conda_recipe\all
    goto finale
)
if [%SHYFT_OS_ANACONDA_TOKEN%]==[] (
    echo WARNING: SHYFT_OS_ANACONDA_TOKEN is unset, assuming anaconda login is done in the build-context
    set token_arg=
) else (
    set token_arg=--token %SHYFT_OS_ANACONDA_TOKEN%
)

if [%SHYFT_OS_ANACONDA_USER%]==[] (
    echo WARNING: SHYFT_OS_ANACONDA_USER is unset, using default
    set user_arg=shyft-os
) else (
    set user_arg=%SHYFT_OS_ANACONDA_USER%
)

if [%cmd_x%]==[upload] (
    setlocal
    @echo Starting to upload %label_name% to %user_arg% shyft.time_series  %SHYFT_VERSION% py %py_version% numpy %np_version%
    for /f "tokens=* USEBACKQ" %%x in (`call conda-build --python=%py_version% --numpy %np_version% --output-folder dist\conda --output conda_recipe\time_series`) do @set pkg_ts=%%x
    anaconda %token_arg% upload --no-progress --force --user %user_arg% --label %label_name% !pkg_ts!
	if not %errorlevel%==0 goto finale
    for /f "tokens=* USEBACKQ" %%x in (`call conda build --python=%py_version% --numpy %np_version% --output-folder dist\conda --output conda_recipe\all`) do @set pkg_all=%%x
    anaconda %token_arg% upload --no-progress --force --user %user_arg% --label %label_name% !pkg_all!
)
:finale
popd
exit /b %errorlevel%
