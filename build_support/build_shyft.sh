#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
set -o nounset # exit on undeclared vars set -u 
set -o pipefail # exit status of the last command that threw non-zero exit code returned

# Debugging set -x 
# set -o xtrace

exec 3>&1 4>&2
SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/..)}
SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
# automatically update shyft-data
if [ ! "${SHYFT_DATA}" ]; then
    cd ${SHYFT_WORKSPACE}
    if [ -d shyft-data ]; then
        pushd shyft-data
        git pull >/dev/null
        popd
    else
        git clone https://gitlab.com/shyft-os/shyft-data.git
    fi;
else
    pushd ${SHYFT_DATA}
    git pull >/dev/null
    popd
fi;
# leave update of 3dparty libraries out of build : bash build_support/build_dependencies.sh
export LD_LIBRARY_PATH="${SHYFT_DEPENDENCIES_DIR}/lib:${LD_LIBRARY_PATH:+LD_LIBRARY_PATH:}"
mkdir -p build
cd build
cmake ..
cmake --build . --config Release --target install

