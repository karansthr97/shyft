#!/bin/bash
set -o errexit # exit on failure set -e
set -o nounset # exit on undeclared vars set -u
set -o pipefail # exit status of the last command that threw non-zero exit code returned
# Debugging set -x
# set -o xtrace

export SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../..)}
build_support_dir=$(readlink --canonicalize --no-newline `dirname ${0}`)
# to align the cmake support:
export SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
armadillo_name=armadillo-9.860.2
dlib_ver=${SHYFT_DLIB_VERSION:-19.19}
dlib_name=dlib-${dlib_ver}
boost_ver=${SHYFT_BOOST_VERSION:-1_72}_0
numpy_ver=${SHYFT_BOOST_NUMPY_VERSION:-1.18}
cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER  -DCMAKE_SYSTEM_VERSION=10.0"
miniconda_ver=latest
echo ---------------
echo Windows Update/build shyft-dependencies
echo SHYFT_WORKSPACE........: ${SHYFT_WORKSPACE}
echo SHYFT_DEPENDENCIES_DIR.: ${SHYFT_DEPENDENCIES_DIR}
echo PACKAGES...............: miniconda ${miniconda_ver} w/shyft_env,
echo .......................: doctest, boost_${boost_ver}, ${armadillo_name}, ${dlib_name}, numpy=${numpy_ver}, openssl, otlv4
WGET='curl -L -O'

# A helper function to compare versions
function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# the current versions we are building
mkdir -p "${SHYFT_DEPENDENCIES_DIR}"
cd "${SHYFT_DEPENDENCIES_DIR}"
cmake_ms_flags="-A x64 -T host=x64"
if [ ! -d ${armadillo_name} ]; then 
    echo Building ${armadillo_name}
    if [ ! -f ${armadillo_name}.tar.xz ]; then 
        ${WGET}  http://sourceforge.net/projects/arma/files/${armadillo_name}.tar.xz
    fi;
    7z -y -bd e ${armadillo_name}.tar.xz >/dev/null
    7z -y -bd x ${armadillo_name}.tar >/dev/null
    pushd ${armadillo_name}
    mkdir -p build
    cd build && cmake ${cmake_ms_flags} -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DCMAKE_INSTALL_LIBDIR=lib -DDETECT_HDF5=0 -DARMA_USE_WRAPPER=FALSE -DARMA_USE_LAPACK=TRUE -DARMA_USE_BLAS=TRUE ${cmake_common} ..
    cmake --build . --config Release 
    cmake -P cmake_install.cmake
	  cp ../examples/lib_win64/* ${SHYFT_DEPENDENCIES_DIR}/lib
    popd
fi;
echo Done ${armadillo_name}


if [ ! -d "${dlib_name}" ]; then
    echo Building "${dlib_name}"
    dlib_archive="v${dlib_ver}.zip"
    if [ ! -f "${dlib_archive}" ]; then
        ${WGET} "https://github.com/davisking/dlib/archive/${dlib_archive}"
    fi;
    7z -y -bd x "${dlib_archive}" >/dev/null
    pushd "${dlib_name}"
    if [ ${dlib_ver} == "19.19" -o ${dlib_ver} == "19.18" ]; then
      patch -b dlib/sockets/sockets_kernel_1.cpp "${build_support_dir}/patch_dlib_nagle.win.diff"
      echo "dlib socket connection patched with disable_nagle"
    fi;

    mkdir -p build
    dlib_cfg="-DDLIB_PNG_SUPPORT=0 -DDLIB_GIF_SUPPORT=0 -DDLIB_LINK_WITH_SQLITE3=0 -DDLIB_NO_GUI_SUPPORT=1 -DDLIB_DISABLE_ASSERTS=1 -DDLIB_JPEG_SUPPORT=0 -DDLIB_USE_BLAS=0 -DDLIB_USE_LAPACK=0 -DBUILD_SHARED_LIBS=0"
    cd build
 	  cmake ${cmake_ms_flags} .. -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common} ${dlib_cfg}
	  cmake --build . --config Release --target install
	  cmake --build . --config Debug --target install
    popd
fi;
echo Done ${dlib_name}

if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/onqtam/doctest
    pushd doctest
    cmake ${cmake_ms_flags} -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} && cmake -P cmake_install.cmake
    popd
fi;
echo Done doctest

if [ ! -d date ]; then
    echo Building Howard Hinnant date extensions to chrono
    git clone https://github.com/HowardHinnant/date
    pushd date
    cmake ${cmake_ms_flags} . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    cmake --build . --target install
    popd
fi;
echo Done HowardHinnant date extensions

cd "${SHYFT_WORKSPACE}"
python_tst=`type python >> /dev/null; echo $?`
if [ ! ${python_tst} -eq 0 ]; then
	if [ ! -d miniconda/Scripts ]; then
		echo Missing python install. try to make one using miniconda
		if [ -d miniconda ]; then
			rm -rf miniconda
		fi;
		if [ ! -f Miniconda3-${miniconda_ver}-Windows-x86_64.exe ]; then
			${WGET}  http://repo.continuum.io/miniconda/Miniconda3-${miniconda_ver}-Windows-x86_64.exe
		fi;
    if [ ! -f Miniconda3-Windows-x86_64.exe ]; then
        cp Miniconda3-${miniconda_ver}-Windows-x86_64.exe Miniconda3-Windows-x86_64.exe
    fi;
		echo 'start /wait "" .\Miniconda3-Windows-x86_64.exe /InstallationType=JustMe /S /D=%cd%\miniconda' >install_miniconda.cmd
		./install_miniconda.cmd
		# Update conda to latest version, assume we start with 4.3 which
		# requires PATH to be set
		OLDPATH=${PATH}
		export PATH="${SHYFT_WORKSPACE}/miniconda:${SHYFT_WORKSPACE}/miniconda/Scripts:${SHYFT_WORKSPACE}/miniconda/Dlls:$PATH"

		old_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
		echo "Old conda version is ${old_conda_version}"
		#activate
		conda config --set always_yes yes --set changeps1 no
		conda update conda
		new_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
		echo "New conda version is ${new_conda_version}"
		py_build_packs="conda-build conda-verify setuptools anaconda-client pip"
		py_dashboard_packs="bokeh sphinx sphinx_rtd_theme pydot"

		conda install ${py_build_packs}
		py_shyft_packs="pyyaml numpy netcdf4 cftime matplotlib requests pytest coverage pytest-cov pip shapely  pyproj"
    conda create -n shyft_36 python=3.6 ${py_shyft_packs} ${py_build_packs} ${py_dashboard_packs}
    conda create -n shyft_37 python=3.7 ${py_shyft_packs} ${py_build_packs} ${py_dashboard_packs}
    echo 'call conda activate shyft_36' >pip_installs.cmd
    echo 'pip install sphinx-autodoc-typehints pint'>>pip_installs.cmd
    echo 'call conda activate shyft_37'>>pip_installs.cmd
    echo 'pip install sphinx-autodoc-typehints pint'>>pip_installs.cmd
    ./pip_installs.cmd
    conda activate base
  else
    export PATH="${SHYFT_WORKSPACE}/miniconda:${SHYFT_WORKSPACE}/miniconda/Scripts:${SHYFT_WORKSPACE}/miniconda/Dlls:$PATH"
  fi;
 else
	echo using pre-installed `type -p python`
fi;
echo Done minconda/python
py_root=`type -p python |  sed  -e 's_/python__'`
export BOOST_PYTHONHOME=`type -p python |  sed  -e 's_/python__' -e 's/^\///' -e 's_/_\\\\_g' -e 's/^./\0:/'`
echo Setting BOOST_PYTHONHOME to  ${BOOST_PYTHONHOME}
open_ssl_dir=${py_root}/Library
echo openssl from pythoh install located at ${open_ssl_dir}
if [ -d ${open_ssl_dir}/include/openssl ]; then
    if [ ! -d ${SHYFT_DEPENDENCIES_DIR}/include/openssl ]; then
        cp -R ${open_ssl_dir}/include/openssl ${SHYFT_DEPENDENCIES_DIR}/include
        cp ${open_ssl_dir}/lib/libssl.lib ${SHYFT_DEPENDENCIES_DIR}/lib
        cp ${open_ssl_dir}/lib/libcrypto.lib ${SHYFT_DEPENDENCIES_DIR}/lib
    else
        echo Done - openssl library seems to be in place, skip
    fi;
else
    echo "opensll not found in " ${open_ssl_dir} ", use conda to install it first"
    exit 1
fi;

cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        ${WGET} https://dl.bintray.com/boostorg/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    echo extracting archive pass1
    7z -y -bd e boost_${boost_ver}.tar.gz >/dev/null
    echo unzipping all the files..
    7z -y -bd x boost_${boost_ver}.tar >/dev/null
    echo building the stuff
    pushd boost_${boost_ver}
    #echo "set PYTHONHOME=%BOOST_PYTHONHOME%" >bboost.cmd
    echo "call bootstrap.bat" > bboost.cmd
    ./bboost.cmd
    py_envs=`echo ${py_root}/envs | sed -e 's@^/.@\0:@g' -e 's@^/@@g'`  # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    mv -f project-config.jam x.jam
    cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
    echo "using python : 3.7 : ${py_envs}/shyft_37/python : ${py_envs}/shyft_37/include : ${py_envs}/shyft_37/libs ;" >>project-config.jam
    echo "using python : 3.6 : ${py_envs}/shyft_36/python : ${py_envs}/shyft_36/include : ${py_envs}/shyft_36/libs ;" >>project-config.jam
    echo "b2 -d0 -j 6 define=BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE link=shared variant=release,debug threading=multi runtime-link=shared address-model=64 --with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-thread --with-atomic --with-math python=3.7,3.6 --prefix=%cd%\.. install" > bboost2.cmd
    ./bboost2.cmd
    popd
fi;
echo  Done boost_${boost_ver}
export WIN_SHYFT_DEPENDENCIES_DIR=`echo ${SHYFT_DEPENDENCIES_DIR} |   sed -e 's@^/.@\0:@g' -e 's@^/@@g'`
cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d pybind11 ]; then
    git clone https://github.com/pybind/pybind11.git
	pushd pybind11
	mkdir -p build
	cd build
	echo set PYTHONHOME=%BOOST_PYTHONHOME% >bdlib.cmd
	echo cmake ${cmake_ms_flags} -DCMAKE_INSTALL_PREFIX=${WIN_SHYFT_DEPENDENCIES_DIR} -DPYBIND11_TEST=0 ${cmake_common} .. >>bdlib.cmd
	echo cmake -P cmake_install.cmake >>bdlib.cmd
	./bdlib.cmd
	popd
fi;
echo Done pybind11
echo Doing the otl header-only otlv4.h
if [ ! -f include/otlv4.h ]; then
    echo ..missing, then download and install otlv4.h
    ${WGET} http://otl.sourceforge.net/otlv4_h2.zip
    7z -y -bd -oinclude x otlv4_h2.zip >/dev/null
fi;
echo Done otlv4.h

cd "${SHYFT_WORKSPACE}"
if [ -d shyft-data ]; then 
    pushd shyft-data
    git pull >/dev/null
    popd
else 
    git clone https://gitlab.com/shyft-os/shyft-data
fi;
echo Done shyft-data


