#include "test_pch.h"
#include <shyft/web_api/targetver.h>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>
#include <csignal>

#include <shyft/dtss/dtss.h>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/web_api/beast_server.h>
#include <test/test_utils.h>

namespace test {
    namespace {
    using std::vector;
    using std::string;
    using std::string_view;

    using namespace shyft::dtss;
    using namespace shyft::core;
    using namespace shyft;

    /** test server for web_api */
    struct test_server:server {
        ~test_server();
        web_api::request_handler bg_server;///< handle web-api requests
        mutable std::recursive_mutex x_srv;//protect the below stuff.
        std::future<int> web_srv;///< mutex,

        test_server():server(
            [=](id_vector_t const &ts_ids,utcperiod p) ->ts_vector_t { return this->test_read_cb(ts_ids,p); },
            [=](std::string search_expression) -> vector<ts_info> { return this->test_find_cb(search_expression); },
            [=](const ts_vector_t& tsv)->void { this->test_store_cb(tsv); }
        ) {
            bg_server.srv=this;
        }


        ts_vector_t test_read_cb(vector<string> const&,utcperiod const&) {
            // recordthe event here
            ts_vector_t r;
            // TODO: make result return
            return r;
        }

        vector<ts_info> test_find_cb(string ) {
            vector<ts_info> r;
            //TODO: fill in results
            return r;
        }

        void test_store_cb(ts_vector_t const& ) {
            //TODO: record the what happen
        }

        void start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads) {
            std::scoped_lock<decltype(x_srv)> sl(x_srv);
            if(!web_srv.valid()) {
                bg_server.running=false;
                web_srv= std::async(std::launch::async,
                    [this,host_ip,port,doc_root,fg_threads,bg_threads]()->int {
                        return web_api::start_web_server(
                        bg_server,
                        host_ip,
                        static_cast<unsigned short>(port),
                        make_shared<string>(doc_root),
                        fg_threads,
                        bg_threads);

                    }
                );
                do {
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                } while(!bg_server.running);
            }
        }
        bool web_api_running() const {
            std::scoped_lock<decltype(x_srv)> sl(x_srv);
            return web_srv.valid();
        }
        void stop_web_api() {
            std::scoped_lock<decltype(x_srv)> sl(x_srv);
            if(web_srv.valid()) {
                std::raise(SIGTERM);
                (void) web_srv.get();
            }
        }
    };
    test_server::~test_server(){
        stop_web_api();
    }
    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;
    
    /** @brief ws session for driving tests.
     *
     * Sends a WebSocket message and prints the response, 
     * from examples made by boost.beast/Vinnie Falco 
     * 
     */
    class session : public std::enable_shared_from_this<session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string text_;
        string response_;
        string fail_;
        std::function<void(string const&)> report_response;
        std::function<string()> next_write_message;
        int responses_left_count{0};
        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        session(boost::asio::io_context& ioc)
            : resolver_(ioc)
            , ws_(ioc)
        {
        }
        string response() const {return response_;}
        string diagnostics() const {return fail_;}
        // Start the asynchronous operation
        template <class Fx,class Wx>
        void
        run(string_view host, int port, string_view text, Fx&& rep_response, Wx&& next_write_msg) {
            // Save these for later
            host_ = host;
            text_ = text;
            port_ = std::to_string(port);
            report_response=rep_response;
            next_write_message=next_write_msg;
            resolver_.async_resolve(host_,port_,// Look up the domain name
                [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                    me->on_resolve(ec,results);
                }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                std::bind(&session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")

            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                [me=shared_from_this()](error_code ec) {me->on_handshake(ec);}
            );
        }

        void
        on_handshake(error_code ec) {
            fail_on_error(ec, "handshake")
            if(text_.size()) {
                ws_.async_write( // Send the message
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // empty text to send means we are done and should close connection
                ws_.async_close(websocket::close_code::normal,
                    [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_write(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "write")
            responses_left_count++;
            text_=next_write_message(); 
            if(text_.size()) { // more messages to send?
                ws_.async_write( // send the message..
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // else start reading responses
                ws_.async_read(buffer_, // Read a message into our buffer
                [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                    me->on_read(ec,bytes_transferred);
                }
                );                            
            }
        }

        void
        on_read(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            response_=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());
            report_response(response_);
            responses_left_count--;
            if(responses_left_count==0) {
                ws_.async_close(websocket::close_code::normal,
                    [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            } else {
                ws_.async_read(buffer_, // Read a message into our buffer
                [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                    me->on_read(ec,bytes_transferred);
                }
                ); 
            }
        }

        void
        on_close(error_code ec) {
            fail_on_error(ec,"close")
        }
        #undef fail_on_error
    };

    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }

    /** engine that perform a publish-subscribe against as specified host
     *
     * Since this is async, I found it easier to just use that code-flow
     * and make a custom class that do the needed minimal work at high speed.
    */
    class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        bool waiting_first_read=true;
        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        pub_sub_session(boost::asio::io_context& ioc):resolver_(ioc),ws_(ioc){}
        vector<string> responses_;
        string diagnostics() const {return fail_;}

        // Start the asynchronous operation
        void
        run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_,port_,// Look up the domain name
                [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                    me->on_resolve(ec,results);
                }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                std::bind(&pub_sub_session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")
            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                [me=shared_from_this()](error_code ec) {me->store_first_ts(ec);}
            );
        }

        void
        store_first_ts(error_code ec) {
            fail_on_error(ec, "store_first_ts")
            ws_.async_write( // start storing the first time-series
                boost::asio::buffer(
                    R"_(store_ts {
                        "request_id"  : "1w",
                        "merge_store" : false,
                        "recreate_ts" : false,
                        "cache"       : true,
                        "tsv"         : [
                                        {
                                            "id": "shyft://test/a1",
                                            "pfx":true,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [1,2,3]
                                        }
                                        ,
                                        {
                                            "id": "shyft://test/a2",
                                            "pfx":false,
                                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                            "values": [4,5,6]
                                        }
                                    ]
                    })_"),
                [me=shared_from_this()](error_code /*ec*/,size_t /*bytes_transferred*/){
                    //fail_on_error(ec,"failed to send first write");
                }
            );
            ws_.async_read(buffer_, // and also start read incoming messages
                [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                    me->on_read(ec,bytes_transferred);
                }
            );
        }

        void
        send_read_and_subscribe(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "send_read_and_subscribe")
            ws_.async_write( // once we have stored the time-series, we just follow up with subscribe_and_read
                boost::asio::buffer(R"_(read {
                                    "request_id": "subscribe_and_read",
                                    "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "clip_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "cache": true,
                                    "ts_ids":["shyft://test/a1","shyft://test/a2"],
                                    "subscribe": true
                                })_"),
                [me=shared_from_this()](error_code /*ec*/,size_t /*bytes_transferred*/){
                    //me->wait_for_first_read_response(ec,bytes_transferred);
                }
            );
        }


        void
        on_read(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            string response=boost::beast::buffers_to_string(buffer_.data());
            responses_.push_back(response);
            buffer_.consume(buffer_.size());
            //MESSAGE("got:"<<response);
            if(response.find("finale")!=string::npos) {
                ws_.async_close(websocket::close_code::normal,
                    [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            } else {
                if(response.find("1w")!=string::npos) {
                    ws_.async_write( // once we have stored the time-series, we just follow up with subscribe_and_read
                        boost::asio::buffer(R"_(read {
                                    "request_id": "subscribe_and_read",
                                    "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "clip_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
                                    "cache": true,
                                    "ts_ids":["shyft://test/a1","shyft://test/a2"],
                                    "subscribe": true
                                })_"),
                        [me=shared_from_this()](error_code /*ec*/,size_t /*bytes_transferred*/){
                            // maybe fail here if ec is not ok
                        }
                    );
                } else if(response.find("subscribe_and_read")!=string::npos) {
                    if(waiting_first_read) {// first time we send an update to the time-series
                        waiting_first_read=false;
                        ws_.async_write(// send and update to the time-series, that should trigger a publish on this socket
                            boost::asio::buffer(
                                R"_(store_ts {
                                    "request_id"  : "2w",
                                    "merge_store" : false,
                                    "recreate_ts" : false,
                                    "cache"       : true,
                                    "tsv"         : [
                                                    {
                                                        "id": "shyft://test/a1",
                                                        "pfx":true,
                                                        "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                                        "values": [3,2,1]
                                                    }
                                                    ,
                                                    {
                                                        "id": "shyft://test/a2",
                                                        "pfx":false,
                                                        "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                                        "values": [6,5,4]
                                                    }
                                                ]
                                })_"
                            ),
                            [me=shared_from_this()](error_code /*ec*/,size_t /*bytes_transferred*/){
                                // nothing to do here, just wait for the subscription update
                            }
                        );
                    } else { // second time, we unsubscribe the read-request
                        ws_.async_write(// when we receive the answer to this, finale, we close the socket.
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale","subscription_id":"subscribe_and_read"})_"),
                            [me=shared_from_this()](error_code /*ec*/,size_t /*bytes_transferred*/){
                                // nothing to do here, just wait for the subscription update
                            }
                        );
                    }
                }
                //--anyway, always continue to read (unless we hit the finale request-id sent with the unsubscribe message
                ws_.async_read(buffer_, // Read next message into our buffer
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                        me->on_read(ec,bytes_transferred);
                    }
                );
            }
        }

        void
        on_close(error_code ec) {
            fail_on_error(ec,"close")
        }
        #undef fail_on_error
    };
    }
}

using namespace test;
using std::string;


TEST_SUITE("web_api_server") {
TEST_CASE("web_api_server_basics") {
    test_server a;
    string host_ip{"127.0.0.1"};
    int port=get_free_port();
    utils::temp_dir tmp_dir("shyft.web_api.");
    string doc_root=(tmp_dir/"doc_root").string();
    string ts_root=(tmp_dir/"ts_root").string();
    a.add_container("test",ts_root);
    a.start_web_api(host_ip,port,doc_root,1,1);

    REQUIRE_EQ(true,a.web_api_running());

    vector<string> responses;
    vector<string> requests{
        R"_(info {"request_id":"1"})_",
        R"_(info {"request_id":"2"})_",
        R"_(store_ts {
                "request_id"  : "3",
                "merge_store" : false,
                "recreate_ts" : false,
                "cache"       : true,
                "tsv"         : [
                                {
                                    "id": "shyft://test/a1",
                                    "pfx":true,
                                    "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [1,2,3]
                                }
                             ,
                                {
                                    "id": "shyft://test/a2",
                                    "pfx":false,
                                    "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [4,5,6]
                                }
                            ]
            })_",
        R"_(find { "request_id": "4", "find_pattern": "shyft://test/a.*"})_",
        R"_(read {
            "request_id": "5",
            "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
            "clip_period": ["2018-01-01T01:00:00Z", "2018-01-01T03:00:00Z"],
            "cache": true,
            "ts_ids":["shyft://test/a1","shyft://test/a2"]
        })_"
    };
    size_t r=0;

    {
        boost::asio::io_context ioc;
        auto s1=std::make_shared<session>(ioc);
        s1->run(host_ip, port, requests[r],//
            [&responses](string const&web_response)->void{
                responses.push_back(web_response);
            },
            [&r,&requests] () ->string {
                ++r;
                return r >= requests.size()?string(""):requests[r];
            }
        );
        // Run the I/O service. The call will return when
        // the socket is closed.
        ioc.run();
        s1.reset();
    }
    a.stop_web_api();
    REQUIRE_GT(responses.size(),0);
    CHECK_EQ(responses.size(),requests.size());
    vector<string> e {
        R"_({ "request_id" : "1","diagnostics": "not implemented" })_",
        R"_({ "request_id" : "2","diagnostics": "not implemented" })_",
        R"_({ "request_id" : "3","diagnostics": "" })_",
        //R"_({"request_id":"4","result":[{"name":"a2","pfx":false,"delta_t":null,"olson_tz_id":"","data_period":[1546300800.0,1546311600.0],"created":null,"modified":1555932102.0},{"name":"a1","pfx":true,"delta_t":null,"olson_tz_id":"","data_period":[1514764800.0,1514775600.0],"created":null,"modified":1555932102.0}]})_",
        R"_()_", // response vary with the time we run the test
        R"_({"request_id":"5","tsv":[{"pfx":true,"data":[[1514768400.0,2.0],[1514772000.0,3.0]]},{"pfx":false,"data":[[1514768400.0,5.0],[1514772000.0,6.0]]}]})_"
    };
    REQUIRE_EQ(e.size(),responses.size());
    for(size_t i=0;i<e.size();++i) {
        if(e[i].size()) {
            CHECK_EQ(responses[i],e[i]);
        } else {// only require response to be larger than 10
            CHECK_GT(responses[i].size(),10);
        }
    }
}
TEST_CASE("web_api_server_stress") {
    test_server a;
    string host_ip{"127.0.0.1"};
    int port=get_free_port();
    utils::temp_dir tmp_dir("shyft.web_api.stress.");
    string doc_root=(tmp_dir/"doc_root").string();
    string ts_root=(tmp_dir/"ts_root").string();
    a.add_container("test",ts_root);
    a.start_web_api(host_ip,port,doc_root,4,4);
    REQUIRE_EQ(true,a.web_api_running());

    vector<string> responses;
    string store_ts_request=
    R"_(store_ts {
        "request_id"  : "3",
        "merge_store" : false,
        "recreate_ts" : false,
        "cache"       : true,
        "tsv"         : [
                        {
                            "id": "shyft://test/a1",
                            "pfx":true,
                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                            "values": [1,2,3]
                        }
                        ,
                        {
                            "id": "shyft://test/a2",
                            "pfx":false,
                            "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                            "values": [4,5,6]
                        }
                    ]
    })_";

    //string info_request=   R"_(info {"request_id":"1"})_";
    string find_request=   R"_(find { "request_id": "4", "find_pattern": "shyft://test/a.*"})_";
    string read_request=
    R"_(read {
        "request_id": "5",
        "read_period": ["2018-01-01T00:00:00Z", "2018-01-01T03:00:00Z"],
        "clip_period": ["2018-01-01T01:00:00Z", "2018-01-01T03:00:00Z"],
        "cache": true,
        "ts_ids":["shyft://test/a1","shyft://test/a2"]
    })_";
    size_t r=0;
    size_t max_requests=1000;
    auto t0=timing::now();
    {
        boost::asio::io_context ioc;
        auto s1=std::make_shared<session>(ioc);
        s1->run(host_ip, port, store_ts_request,//
            [&responses](string const&web_response)->void{
                responses.push_back(web_response);
            },
            [&r,&max_requests,read_request,find_request] () ->string {
                ++r;
                if(r>=max_requests)
                    return string("");//
                return r%2?read_request:find_request;// alternate read/find request.
            }
        );
        // Run the I/O service. The call will return when
        // the socket is closed.
        ioc.run();
        s1.reset();
    }
    auto t1=timing::now();
    a.stop_web_api();
    REQUIRE_GT(responses.size(),0);
    CHECK_EQ(responses.size(),max_requests);
    auto us=elapsed_us(t0,t1);
    MESSAGE("web_api server "<<max_requests<<" done in "<< double(us)/1000.0<<"ms ~ " << double(max_requests)/(double(us)/1e3)  << " kilo req/sec\n");

}
TEST_CASE("web_api_server_subscription") {
    test_server a;
    string host_ip{"127.0.0.1"};
    int port=get_free_port();
    utils::temp_dir tmp_dir("shyft.web_api.sub.");
    string doc_root=(tmp_dir/"doc_root").string();
    string ts_root=(tmp_dir/"ts_root").string();
    a.add_container("test",ts_root);
    a.start_web_api(host_ip,port,doc_root,4,4);
    REQUIRE_EQ(true,a.web_api_running());
    boost::asio::io_context ioc;
    auto s1=std::make_shared<pub_sub_session>(ioc);
    s1->run(host_ip, port);
    ioc.run();
    vector<string> expected_responses{
        string(R"_({ "request_id" : "1w","diagnostics": "" })_"),
        string(R"_({"request_id":"subscribe_and_read","tsv":[{"pfx":true,"data":[[1514764800.0,1.0],[1514768400.0,2.0],[1514772000.0,3.0]]},{"pfx":false,"data":[[1514764800.0,4.0],[1514768400.0,5.0],[1514772000.0,6.0]]}]})_"),
        string(R"_({ "request_id" : "2w","diagnostics": "" })_"),
        string(R"_({"request_id":"subscribe_and_read","tsv":[{"pfx":true,"data":[[1514764800.0,3.0],[1514768400.0,2.0],[1514772000.0,1.0]]},{"pfx":false,"data":[[1514764800.0,6.0],[1514768400.0,5.0],[1514772000.0,4.0]]}]})_"),
        string(R"_({ "request_id" : "finale","diagnostics": "" })_")
    };
	auto responses = s1->responses_;
    REQUIRE_EQ(responses,expected_responses);
    s1.reset();
    a.stop_web_api();
}
}
