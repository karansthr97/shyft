#include "test_pch.h"
#include <shyft/web_api/web_api_generator.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/generators/proxy_attr.h>
#include "test_parser.h"
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

// play-ground for stuff that will be promoted once it's ok'
namespace shyft::web_api::generator {
    //-- working with the stm reseroir as example,
    //   now we have to deal with properties (proxy_attr)
    //   that can be 'null', or have a value
    //    - note: most value-types are shared_ptr, so they might be null as well
    //
    //   json: matches to null or object
    //         and boils down to abilitiy to emit
    //         any of the basic proxy_attr value types as
    //         defined in stm/attribute_types.h
    //         we can represent map<key,value>
    //              as
    //                 [ [key,value],...]
    //        the basic value-types are mostly from hydro_power/xy_point_curve.h
    //           that deals with point(x,y) derived descriptions of volume-,gate-,turbine-,generator
    //           descriptions.
    //
    //   t_double_: (to be deprecated, replaced by apoint_ts type)
    //           null | [ [<time>, double_<precision>],.. ]
    //
    //   t_xy_ : (typical reservoir volume-description, Mm3 vs masl etc.)
    //           null | [ [<time>, <xy>],...]
    //    <xy> : (null|[ [double_,double_],... ])]
    //
    //   t_xyz_: (typical gate-opening-flow descr: given gate opening z, water-level x, then flow is y)
    //          null  | [ [<time>, <xyz>],...]
    //    <xyz>: {'z': double_, 'xy':<xy> }
    //
    // t_xyz_list_:
    //         null | [ [<time>], <xyz_list>],...]
    //
    // <xyz_list> :
    //           [ <xyz>,...]
    //
    // t_turbine_description_ :
    //


    


    using shyft::energy_market::stm::t_xy_;
    using shyft::energy_market::stm::t_xyz_;
    using shyft::energy_market::stm::t_xyz_list_;
    using shyft::energy_market::stm::t_turbine_description_;


    #undef x_emit_ptr_t_map
    /* -- now stm::reservoir, with proxy attributes
     *
     * Given that we only provide raw-topology for the topology response,
     * how to we provide a best possible web_api to retrieve/set the values?
     *
     * From the user-interface side, what we would like is to make request that
     * fills up the component-model for visualization
     *
     * like:
     *  request{request_id:888,hps_id:3,reservoir_ids:[1,3,4],attr_ids:[lrl,volume_descr],t=now}
     *
     * then get back a response(s) from the server-socket like:
     *
     * make_response(object,attr-list[lrl,volume_descr,..],valid_at=t)
     * ->[object.id,object.lrl(t),object.volume_descr(t),]
     *
     * e.g.     lrl(t)   volume_descr(t)
     *     id   lrl    Mm3 masl   Mm3   masl
     *   [123,433.2,[[0.0,433.2],[400.0,500.0]]]
     *
     * Q1: So what is the perfect attr-list ?
     *  A1:the int-version of the rsv_attr enum.
     *  why ?
     *   1. because on server-side it resolves at zero overhead to its value using
     *    proxy_attr<reservoir,t_double,rsv_attr,attr_id,reservoir::ids> a(object);
     *    then a.exists() -> true if it is set/exists
     *     and a.get() -> a_v, that is a value of value-type, given true above
     *     and valid_at(a_v,t) -> a_v_t, value valid at time t.
     *       the type of a_v_t above would be double in this example
     *       that we feed into the boost::spirit::karma double_ emitter.
     *   !!! ease mapping work:
     *    let reservoir provide the methods:
     *       has_attr<value_type>(rsv_attr|int,valid_at:utctime) -> value_type
     *       get_attr<value_type>(rsv_attr|int,valid_at:utctime) -> value_type
     *       set_attr(rsv_attr|int,valid_at:utctime,value_type val)->ok
     *    and for time-series, similar, but adapted:
     *       get_ts(rsv_attr|int, transform_spec, period_spec..)->time-series
     *     .. the above require you to compile-time know the value-type of rsv_attr..
     *         like value_type<rsv_attr,..> ::type
     *
     *    2. minimal consistent repr.
     * Q2, given A1,how to provide the enum to type-script ?
     *   A2: several options,
     *       as checked in library (generated from c++)
     *       as separate service call (would that be useful ?)
     *
     * Similar for setting the values (e.g. lrl):
     *
     *  request  {request_id:111,reservoir_ids:[1,2,3],attr_ids:[lrl],values:[100,200,300]}
     *
     * response: {request_id:111, result: "ok" }
     *
     */


    //-- end our classes emitters

    //----------------------------------
    // vectors of object (given object is defined)
    //
}
namespace test::web_api {
    using namespace shyft::energy_market::hydro_power;
    using std::make_shared;
    using std::make_unique;

    inline hydro_power_system_ build_hps(string name) {
        auto hpsm = make_shared<hydro_power_system>(name);
        auto hps = make_unique<hydro_power_system_builder>(hpsm);
        auto r1 = hps->create_reservoir(1,"r1", R"(xxxr)");
        auto r2 = hps->create_reservoir(2,"r2", R"(reservoir_data(100, 110, 10))");
        auto r3 = hps->create_reservoir(3,"r3", R"(reservoir_data(100, 110, 10))");
        auto o = hps->create_reservoir(4,"ocean",R"( reservoir_data(0, 0.01, 1e100))");
        auto p1 = hps->create_unit (1,"the one",R"(power_station_data...)");
        auto p2 = hps->create_unit (2,"the other", R"(power_station_data()");
        auto p = hps->create_power_plant (3,"pp","json{}");
        power_plant::add_unit(p,p1);
        power_plant::add_unit(p,p2);

        auto t1 = hps->create_tunnel(1, "r1-p1", "");
        waterway::add_gate(t1, make_shared<gate>(1, "2", "json{3:'x'}"));
        connect(t1).input_from(r1).output_to(p1);
        connect(hps->create_river(2,"r2-r1 river","")).input_from(r2).output_to(r1);
        connect(hps->create_river(3,"p to ocean", "")).input_from(p1).output_to(o);
        connect(hps->create_tunnel(4,"r3-p2", "")).input_from(r3).output_to(p2);
        connect(hps->create_river(5,"p2 to ocean", "")).input_from(p2).output_to(o);
        connect(hps->create_river(6,"r3-bypass ocean", "")).input_from(r3, connection_role::bypass).output_to(o);
        connect(hps->create_river(7,"r2-flood ocean", "")).input_from(r2, connection_role::flood).output_to(o);

        auto wms1 = hps->create_catchment(1,"9471-U", "some jason metadata");
        return hpsm;
    }
}

using namespace shyft::core;
using shyft::energy_market::srv::model_info;
using shyft::energy_market::hydro_power::power_plant;
using shyft::energy_market::hydro_power::hydro_connection;
using shyft::energy_market::hydro_power::waterway;
using shyft::energy_market::hydro_power::reservoir;
using shyft::energy_market::hydro_power::unit;
using shyft::energy_market::hydro_power::connection_role;
using shyft::energy_market::hydro_power::hydro_component;

using std::string;using std::vector;using std::make_shared;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::stm::t_xy_;
using namespace shyft::web_api::generator;
template struct emit_object<std::back_insert_iterator<std::string>>;

TEST_SUITE("em/web_api/generator") {
TEST_CASE("em_xy_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    xy_point a{1.0,2.0};
    auto ok=generate(sink,xy_generator<decltype(sink)>(),a);
    REQUIRE_EQ(ok,true);
    CHECK_EQ(R"_((1.0,2.0))_",ps);

    vector<xy_point> aa;
    ps.clear();
    REQUIRE_EQ(true,generate(sink,xy_point_curve_generator<decltype(sink)>(),aa));
    CHECK_EQ(R"_([])_",ps);
    //REQUIRE_EQ(true,generate(sink,xyv_generator<decltype(sink)>(),aa));
    //CHECK_EQ(R"_([[1.0,2.0],[3.0,4.0]])_",ps);
    ps.clear();
    aa.push_back(a);aa.push_back(xy_point{3.0,4.0});
    xy_point_curve aaa;aaa.points=aa;
    REQUIRE_EQ(true,generate(sink,xy_point_curve_generator<decltype(sink)>(),aa));
    CHECK_EQ(R"_([(1.0,2.0),(3.0,4.0)])_",ps);
}
TEST_CASE("em_xyz_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    xy_point_curve_with_z  a;
    REQUIRE_EQ(true,generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"z":0.0,"points":[]})_",ps);
    ps.clear();
    a.z=1.0;
    a.xy_curve.points.push_back(xy_point{3.0,4.0});
    REQUIRE_EQ(true,generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"z":1.0,"points":[(3.0,4.0)]})_",ps);
    // step up to list
    xyz_point_curve_list aa;
    ps.clear();
    REQUIRE_EQ(true,generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa));
    CHECK_EQ(R"_([])_",ps);
    aa.push_back(a);
    ps.clear();
    REQUIRE_EQ(true,generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa));
    CHECK_EQ(R"_([{"z":1.0,"points":[(3.0,4.0)]}])_",ps);

}
TEST_CASE("em_turbine_eff_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    turbine_efficiency  a;
    REQUIRE_EQ(true,generate(sink,turbine_efficiency_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"production_min":0.0,"production_max":0.0,"efficiency_curves":[]})_",ps);
    ps.clear();
    a.production_max=100.0;
    a.production_min=9.0;
    a.efficiency_curves.push_back(xy_point_curve_with_z{});
    REQUIRE_EQ(true,generate(sink,turbine_efficiency_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"production_min":9.0,"production_max":100.0,"efficiency_curves":[{"z":0.0,"points":[]}]})_",ps);
}

TEST_CASE("em_turbine_description_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    turbine_description  a;
    REQUIRE_EQ(true,generate(sink,turbine_description_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"turbine_effiencies":[]})_",ps);
    ps.clear();
    a.efficiencies.push_back(turbine_efficiency{});
    REQUIRE_EQ(true,generate(sink,turbine_description_generator<decltype(sink)>(),a));
    CHECK_EQ(R"_({"turbine_effiencies":[{"production_min":0.0,"production_max":0.0,"efficiency_curves":[]}]})_",ps);
}



TEST_CASE("model_info_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    model_info a{
        1,
        string("aname"),
        utc.time(2018,1,2,3,4,5),
        string("somejson")
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"id":1,"name":"aname","created":1514862245.0,"json":"somejson"})_",ps);
}
TEST_CASE("em_t_xy_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xy_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xy_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{1.0,2.0}});
    (*a)[utc.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{3.0,4.0}});
    emit(sink,a);
    CHECK_EQ(R"_({[3.0:[(1.0,2.0)]],[4.0:[(3.0,4.0)]]})_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,6)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},1.0);
    (*a)[utc.time(1970,1,1,0,0,7)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{3.0,4.0}}},2.0);
    emit(sink,a);
    CHECK_EQ(R"_({[6.0:{"z":1.0,"points":[(1.0,2.0)]}],[7.0:{"z":2.0,"points":[(3.0,4.0)]}]})_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_list_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_list_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_list_::element_type>();
    auto c1=make_shared<xyz_point_curve_list>();
    xy_point_curve_with_z e1{xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},3.0};
    c1->push_back(e1);
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    CHECK_EQ(R"_({[6.0:[{"z":3.0,"points":[(1.0,2.0)]}]]})_",ps);
}

TEST_CASE("em_t_turbine_description_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_turbine_description_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_turbine_description_::element_type>();
    auto c1=make_shared<turbine_description>();
    c1->efficiencies.push_back(turbine_efficiency{});
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    CHECK_EQ(R"_({[6.0:{"turbine_effiencies":[{"production_min":0.0,"production_max":0.0,"efficiency_curves":[]}]}]})_",ps);
}

TEST_CASE("em_t_str_generator") {
    string ps;
    auto sink = std::back_inserter(ps);
    pair<utctime, string> a{from_seconds(1), "test message"};
    emit(sink, a);
    CHECK_EQ(R"_((1.0,"test message"))_", ps);
}

TEST_CASE("proxy_attribute_variant_generator") {
    string ps;
    auto sink = std::back_inserter(ps);
    string var_ps;
    auto var_sink = std::back_inserter(var_ps);

    calendar utc;

    // t_xy_
    t_xy_ a2;
    // Emit regular:
    ps.clear();
    a2=make_shared<t_xy_::element_type>();
    (*a2)[utc.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{1.0,2.0}});
    (*a2)[utc.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{3.0,4.0}});
    emit(sink,a2);
    // Emit as variant:
    var_ps.clear();
    proxy_attr_range b = a2;
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

    // t_turbine_description_
    // Emit regular:
    t_turbine_description_ a3;
    ps.clear();
    a3=make_shared<t_turbine_description_::element_type>();
    auto c1=make_shared<turbine_description>();
    c1->efficiencies.push_back(turbine_efficiency{});
    (*a3)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a3);
    // Emit as variant:
    b = a3;
    var_ps.clear();
    emit(var_sink,b);
    CHECK_EQ(var_ps, ps);

    // apoint_ts:
    // Emit regular:
    size_t n=10;
    gta_t ta(from_seconds(0),from_seconds(1),n);
    vector<double> v(n,1.2);
    v[1]= shyft::nan;
    apoint_ts a4(ta,v,ts_point_fx::POINT_AVERAGE_VALUE);

    ps.clear();
    emit(sink, a4);
    // Emit as variant:
    b = a4;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

    // t_xyz_list_:
    // Emit regular:
    t_xyz_list_ a5;
    ps.clear();
    a5=make_shared<t_xyz_list_::element_type>();
    auto c2=make_shared<xyz_point_curve_list>();
    xy_point_curve_with_z e1{xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},3.0};
    c2->push_back(e1);
    (*a5)[utc.time(1970,1,1,0,0,6)]=c2;
    emit(sink,a5);

    // Emit as variant:
    b = a5;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

}


TEST_CASE("model_info_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    std::vector<model_info> a{
        model_info{
            1,
            string("aname"),
            utc.time(2018,1,2,3,4,5),
            string("somejson")
        },
        model_info{}
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"#([{"id":1,"name":"aname","created":1514862245.0,"json":"somejson"},{"id":0,"name":"","created":null,"json":""}])#",ps);
    ps.clear();
    a.clear();
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ("[]",ps);
}

TEST_CASE("hyd_connect_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    hydro_connection a{connection_role::main,nullptr};
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"role":"main","target":null})_",ps);
    ps.clear();
    a.role=connection_role::input;
    a.target=std::make_shared<waterway>(123,"wx");
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"role":"input","target":"W123"})_",ps);
}
TEST_CASE("hyd_connect_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto wr=std::make_shared<waterway>(123,"wx");
    auto rsv=std::make_shared<reservoir>(456,"rx");
    std::vector<hydro_connection> a{
        {connection_role::main,wr},
        {connection_role::main,nullptr},
        {connection_role::input,rsv}
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_([{"role":"main","target":"W123"},{"role":"main","target":null},{"role":"input","target":"R456"}])_",ps);
}
TEST_CASE("hydro_component_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto s=std::make_shared<hydro_power_system>(1,"s0");
    auto wru=std::make_shared<waterway>(1,"wru","",s);
    auto wr=std::make_shared<waterway>(3,"wrd","",s);
    auto rsv=std::make_shared<reservoir>(2,"rx","json",s);
    s->waterways.push_back(wru);
    s->waterways.push_back(wr);
    s->reservoirs.push_back(rsv);
    hydro_component::connect(rsv,connection_role::main,wr);
    hydro_component::connect(wru,rsv);
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(rsv));
    CHECK_EQ(R"_({"id":2,"name":"rx","json":"json","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]})_",ps);
    ps.clear();
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(wr));
    CHECK_EQ(R"_({"id":3,"name":"wrd","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[]})_",ps);

}

TEST_CASE("power_station_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto s=std::make_shared<hydro_power_system>(1,"s0");
    auto a1=std::make_shared<unit>(1,"a1","",s);
    auto a2=std::make_shared<unit>(2,"a2","", s);
    auto p=std::make_shared<power_plant>(3,"p1","",s);
    s->power_plants.push_back(p);
    power_plant::add_unit(p,a1);
    power_plant::add_unit(p,a2);
    shyft::web_api::generator::emit(sink,p);
    CHECK_EQ(R"_({"id":3,"name":"p1","json":"","units":[1,2]})_",ps);
}

TEST_CASE("web_api_hps_gen") {
    auto hps=test::web_api::build_hps("A");
    std::string ps;
    auto sink=std::back_inserter(ps);
    shyft::web_api::generator::emit(sink,hps);
    CHECK_EQ(R"_({"id":0,"name":"A","created":null,"units":[{"type":"A","hydro_component":{"id":1,"name":"the one","json":"power_station_data...","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]}},{"type":"A","hydro_component":{"id":2,"name":"the other","json":"power_station_data(","upstreams":[{"role":"input","target":"W4"}],"downstreams":[{"role":"main","target":"W5"}]}}],"waterways":[{"type":"W","hydro_component":{"id":1,"name":"r1-p1","json":"","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]},"gates":[{"id":1,"name":"2","json":"json{3:'x'}"}]},{"type":"W","hydro_component":{"id":2,"name":"r2-r1 river","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R1"}]},"gates":[]},{"type":"W","hydro_component":{"id":3,"name":"p to ocean","json":"","upstreams":[{"role":"input","target":"A1"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":4,"name":"r3-p2","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"A2"}]},"gates":[]},{"type":"W","hydro_component":{"id":5,"name":"p2 to ocean","json":"","upstreams":[{"role":"input","target":"A2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":6,"name":"r3-bypass ocean","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":7,"name":"r2-flood ocean","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]}],"reservoirs":[{"type":"R","hydro_component":{"id":1,"name":"r1","json":"xxxr","upstreams":[{"role":"input","target":"W2"}],"downstreams":[{"role":"main","target":"W1"}]}},{"type":"R","hydro_component":{"id":2,"name":"r2","json":"reservoir_data(100, 110, 10)","upstreams":[],"downstreams":[{"role":"main","target":"W2"},{"role":"flood","target":"W7"}]}},{"type":"R","hydro_component":{"id":3,"name":"r3","json":"reservoir_data(100, 110, 10)","upstreams":[],"downstreams":[{"role":"main","target":"W4"},{"role":"bypass","target":"W6"}]}},{"type":"R","hydro_component":{"id":4,"name":"ocean","json":" reservoir_data(0, 0.01, 1e100)","upstreams":[{"role":"input","target":"W3"},{"role":"input","target":"W5"},{"role":"input","target":"W6"},{"role":"input","target":"W7"}],"downstreams":[]}}],"power_plants":[{"id":3,"name":"pp","json":"json{}","units":[1,2]}]})_",ps);

}


}

namespace experiment {
    using shyft::energy_market::core::proxy_attr_m;
    using namespace shyft::energy_market;
    //using namespace shyft::web_api::generator;
     // -- experiment section: 
     // -- goal: want one line pr. attr for set/get/exist.
     //             declarative
     //             if possible, derived from the proxy-types in the stm::reservoir class.
     // 
     //--ok, now lets' pretend we got a web-request, with 16606, and rsv_attr::hrl, lrl'
     ///  web-request: reservoir(oid).attr(aid) [.a_value_type.]
     ///  web-request: ot, oid, aid, value?
     /// -reply:: json (type-script-type)
     // for req:requests:
     ///  generate_json( req.o_d, req.a_id ,req.type_id)
     //
    
    /** @brief proxy_attr_exists
     * 
     * @detail just an example, the function could be generate_json etc..
     * 
     * @tparam C the class, like stm::reservoir
     * @tparam E the enum class, like stm::rsv_attr
     * @tparam E e the enum member, like stm::rsv_attr::lrl
     * @param o the const object ref to class C, e.g. stm::reservoir
     * @return true if the C::o have attribute e set to a value
     */
    
    template<typename C, typename E, E  e>
    bool proxy_attr_exists(C const & o) noexcept {
      return (o.*proxy_attr_m<C,E,e>::member()).exists();
    }
    

    
    
    
    /**  fx to compile time roll out the table map [int alias rsv_attr] proxy_attr_exists fx pointers.
     * @note that this template works for any stm class with proxy stuff in it
    */
    template<typename C,typename E, int...a> 
    constexpr std::array<bool(*)(C const&),sizeof...(a)>
    cmake_attr_exists_table(std::integer_sequence<int,a...>) { 
        return {{&proxy_attr_exists<C,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }
    
    // finally, easy to create the function that does the runtime-job task
    // assuming
    // C::e_attr enum is defined 
    // C::e_attr_seq_t is defined
    template <class C>
    bool object_attr_exists(C const&r, typename C::e_attr a) {
        static constexpr auto _attr_exists_fx =cmake_attr_exists_table<C,typename C::e_attr>(typename C::e_attr_seq_t{});
        return _attr_exists_fx[int(a)](r);
    }
    
    // now for the emitter:
    /** @brief emits a proxy attribute
     * 
     * @detail using the generator::emit(oi,T), or emit_null if the 
     *  proxy_attr is null (!exists()).
     * 
     */
    template<typename C,typename OutputIterator, typename E, E e>
    void emit_attr(C const &o,OutputIterator &oi) noexcept {
        auto const &a=o.*proxy_attr_m<C,E,e>::member();
        if(a.exists()) {
            shyft::web_api::generator::emit(oi,a.get());
        } else {
            shyft::web_api::generator::emit_null(oi);
        }
    }
    
    template<typename C,typename OutputIterator, typename E, int...a> 
    constexpr std::array<void(*)(C const&,OutputIterator&),sizeof...(a)>
    cmake_emit_attr_table(std::integer_sequence<int,a...>) { 
        return {{&emit_attr<C,OutputIterator,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }
    
    template<class C,typename OutputIterator>
    void emit_attr_for_object(C const&o,typename C::e_attr a, OutputIterator&oi) {
        static constexpr auto _attr_emit_fx =cmake_emit_attr_table<C,OutputIterator,typename C::e_attr>(typename C::e_attr_seq_t{});
        _attr_emit_fx[int(a)](o,oi);
    }
    
    
}

TEST_SUITE("stm_mp") {
TEST_CASE("stm_attribute_study") {
    // try to figure out the best possible query/response
    // patterns
    using namespace shyft::energy_market;
    using std::runtime_error;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_axis::point_dt;
    using t_xy=stm::t_xy_::element_type;

    utctime t0{ 0 };
    utctime t1{ 1 };
    auto ta = point_dt(vector<utctime>{t0}, t1);
    //--arrange the test
    auto sys=make_shared<stm::stm_system>(1,"first","");
    auto ull=make_shared<stm::stm_hps>(1,"Ulla-førre");
    ull->ids = make_shared<stm::hps_ds>();// attach empty ids
    sys->hps.push_back(ull);
    sys->market.push_back(make_shared<stm::energy_market_area>(1,"NO1","null",sys));
    auto b = make_shared<stm::reservoir>(16606,"blåsjø","",ull);
    ull->reservoirs.push_back(b);
    CHECK_EQ(b->hrl.exists(),false);// none set yet.

	auto t_d = apoint_ts(ta, 1233.0);
	b->hrl = t_d;
	apoint_ts y=b->hrl;
	//-- assert
     CHECK_EQ(y(t0),doctest::Approx(1233.0));
     auto t_r_vol=make_shared<t_xy>();
     auto vol_curve=make_shared<xy_point_curve>(vector<double>{0.0,3500.0},vector<double>{1200.0,1234.0});
     (*t_r_vol)[t0]= vol_curve;
     b->volume_descr=t_r_vol;
     CHECK_EQ(b->volume_descr.exists(),true);
     
     //--- ok now we are ready for the  experiment section: 
     // imagine we got these two in the web-request.
     // knowing from request it was a reservoir, located in this system
     int r_attr=int(stm::rsv_attr::hrl);// from the request,
     int64_t oid=16606;
     // just imagine we looked up b from oid
     auto r=std::dynamic_pointer_cast<stm::reservoir>(sys->hps[0]->find_reservoir_by_id(oid));
     // this we like (could be any fx we want to apply to object.proxy_attr):
     CHECK_EQ(true,experiment::object_attr_exists(*r,static_cast<stm::rsv_attr>(r_attr)));
     CHECK_EQ(false,experiment::object_attr_exists(*r,stm::rsv_attr::lrl));
     CHECK_EQ(true,experiment::object_attr_exists(*r,stm::rsv_attr::volume_descr));
     string ps;
     auto  sink=std::back_inserter(ps);
     experiment::emit_attr_for_object(*r,stm::rsv_attr::hrl,sink);
     CHECK_EQ(true,ps.size()>0);
     // this works: std::cout<<"hrl:"<<ps<<"\n";
     
}

}
