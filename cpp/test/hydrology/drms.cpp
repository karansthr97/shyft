#include "test_pch.h"
#include <shyft/hydrology/srv/client.h>
#include <shyft/hydrology/srv/server.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/stacks/pt_gs_k.h>  // looking for the ptgsk cell modelj
#include <shyft/hydrology/stacks/pt_ss_k.h>
#include <shyft/hydrology/api/api.h>  // looking for region environment
#include <shyft/hydrology/api/api_state.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/variant.hpp>
#include <variant>

using namespace shyft::core;
using namespace shyft::hydrology::srv;
using std::string;
using std::to_string;
using cellstate_t = shyft::api::cell_state_with_id<shyft::core::api::pt_gs_k::state>;
using shyft::time_series::dd::apoint_ts;
using shyft::core::q_adjust_result;

typedef shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> region_model_t_;

template <class T>
static T serialize_loop(const T& o, int c_a_flags = core_arch_flags) {
    stringstream xmls;
    core_oarchive oa(xmls, c_a_flags);
    oa << core_nvp("o", o);
    xmls.flush();
    core_iarchive ia(xmls, c_a_flags);
    T o2;
    ia>>core_nvp("o", o2);
    return o2;
}

TEST_SUITE("drms") {

TEST_CASE("the_story") {
    using shyft::time_series::dd::apoint_ts;
    
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        client c(host_port);
        auto result = c.version_info();
        // check result
        CHECK_EQ(result, s.do_get_version_info());
        
        auto mids=c.get_model_ids();
        CHECK_EQ(mids.size(),0); // it should be 0 models to start with
        
        // create model test
        vector<geo_cell_data> gcd;
        geo_cell_data c1;c1.set_catchment_id(1);
        gcd.push_back(c1);
        auto success = c.create_model("m1", rmodel_type::pt_gs_k, gcd);
        //-- verify we get exception trying to create it once more!
        CHECK_THROWS_AS(c.create_model("m1",rmodel_type::pt_gs_k,gcd),std::runtime_error);
        
        CHECK_EQ(success, true);
        CHECK_EQ(s.model_map.size(), 1);
        // test that get_model_ids now returns 1 model
        mids=c.get_model_ids();
        REQUIRE_EQ(mids.size(),1);
        CHECK_EQ(mids[0],"m1");
        
        // set state test
        shyft::api::cids_t cids;
        state_variant_t s0 = c.get_state("m1",cids);
            boost::apply_visitor(
                [](auto const&sv)->void {
                    
                    for(auto &sid:*sv)
                        sid.state.kirchner.q=0.8;
                }
                ,s0);
        success = c.set_state("m1", s0);
        
        CHECK_EQ(success, true);
        CHECK_EQ(c.set_initial_state("m1"),true);
        
        CHECK_EQ(c.set_state_collection("m1", -1, false), true);
        CHECK_EQ(c.set_snow_sca_swe_collection("m1", -1, true), true);
        
        // test set environment
        calendar utc;
        int ts_size = 5;
        auto ta = time_axis::generic_dt(utc.time(2019,6,1),deltahours(1),ts_size);
        api::a_region_environment r_env;
        auto p0=c1.mid_point();
        apoint_ts ts(ta,0.8,time_series::POINT_AVERAGE_VALUE);
        r_env.temperature->push_back(api::TemperatureSource(p0,ts));
        r_env.precipitation->push_back(api::PrecipitationSource(p0,ts));
        r_env.radiation->push_back(api::RadiationSource(p0,ts));
        r_env.wind_speed->push_back(api::WindSpeedSource(p0,ts));
        r_env.rel_hum->push_back(api::RelHumSource(p0,ts));
        interpolation_parameter ip_param;
        success = c.run_interpolation("m1", ip_param, ta, r_env, true);
        CHECK_EQ(success, true);
        CHECK_EQ(c.is_cell_env_ts_ok("m1"), true); // check cell time axis

        parameter_variant_t p=make_shared<pt_gs_k::parameter>();
        CHECK_EQ(true,c.set_region_parameter("m1",p));
        CHECK_EQ(false,boost::get<region_model_t_>(s.model_map[string("m1")])->has_catchment_parameter(1));
        
        CHECK_EQ(true,c.set_catchment_parameter("m1",p,1));// catchm. id 1
        // verify we did set the catchment parameter
        auto m1_variant=s.model_map.find(string("m1"));
        REQUIRE(m1_variant != s.model_map.end());
        CHECK_EQ(true,boost::get<region_model_t_>(m1_variant->second)->has_catchment_parameter(1));
        // test run cells
        success = c.run_cells("m1");
        CHECK_EQ(success, true);

            
            // test get statistics
            auto cell_index = shyft::core::stat_scope::cell_ix;
            apoint_ts ts_discharge = c.get_discharge("m1", cids, cell_index);
            CHECK_EQ(ts_discharge.time_axis(), ta);
            CHECK_EQ(c.get_charge("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_snow_swe("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_snow_sca("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_temperature("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_precipitation("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_radiation("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_wind_speed("m1", cids, cell_index).time_axis(), ta);
            CHECK_EQ(c.get_rel_hum("m1", cids, cell_index).time_axis(), ta);

            
        // test adjust state
        double wanted_state = ts_discharge.value(0)*1.20;  // increase by 20%
        q_adjust_result res = c.adjust_q("m1", cids, wanted_state);
        CHECK_EQ(res.diagnostics, "");
        double obtained_state = res.q_r;
        CHECK_EQ(abs(obtained_state-wanted_state)<0.01*wanted_state, true); 
        apoint_ts ts_discharge2 = c.get_discharge("m1", cids,shyft::core::stat_scope::cell_ix);
        CHECK_NE(ts_discharge2.value(0), ts_discharge.value(0));
        
        // test catchment calculation filter
        std::vector<int64_t> catchment_ids;
        success = c.set_catchment_calculation_filter("m1", catchment_ids);
        CHECK_EQ(success, true);
                    
        // test revert states
        
        success = c.revert_to_initial_state("m1");
        CHECK_EQ(success, true);
        
        // rename model test
        CHECK_THROWS_AS(c.rename_model("m2","m0"), std::runtime_error); // renaming non-existent model
        CHECK_THROWS_AS(c.rename_model("m1","m1"), std::runtime_error); // renaming to name already in use
        CHECK_EQ(c.rename_model("m1","m0"), true);
        mids=c.get_model_ids();
        CHECK_EQ(mids.size(),1);
        CHECK_EQ(mids[0],"m0");
        
        // clone model test
        CHECK_THROWS_AS(c.clone_model("m1","m3"), std::runtime_error); 
        CHECK_THROWS_AS(c.clone_model("m0","m0"), std::runtime_error);
        CHECK_EQ(c.clone_model("m0","m3"), true);
        mids=c.get_model_ids();
        CHECK_EQ(mids.size(),2);
        CHECK_EQ(true,c.remove_model("m3"));
    
        // copy model test
        CHECK_THROWS_AS(c.copy_model("m1","m4"), std::runtime_error); 
        CHECK_THROWS_AS(c.copy_model("m0","m0"), std::runtime_error);
        c.revert_to_initial_state("m0");
        CHECK_EQ(c.copy_model("m0","m4"), true);
        auto m4_temp=c.get_temperature("m4", cids, cell_index);
        auto m0_temp=c.get_temperature("m0", cids, cell_index);
        CHECK_EQ(m4_temp,m0_temp);
        // check that m4 state is equal to m0 state
        state_variant_t m4_s0 = c.get_state("m4",cids);
            boost::apply_visitor(
                [](auto const&sv)->void {
                    for(auto &sid:*sv)
                        FAST_REQUIRE_EQ(sid.state.kirchner.q,doctest::Approx(0.8));
                }
                ,m4_s0);
        
        mids=c.get_model_ids();
        CHECK_EQ(mids.size(),2);
        CHECK_EQ(true,c.remove_model("m4"));
        
        // test we can remove model
        CHECK_EQ(true,c.remove_model("m0"));
        mids=c.get_model_ids();// verify it's empty..
        CHECK_EQ(mids.size(),0);
        // and verify it throws if you try to remove non-existent model.
        CHECK_THROWS_AS(c.remove_model("m0"),std::runtime_error);
            
        c.close();
        
    } catch (exception const&ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true,false);
        s.clear();
    }
}
    
TEST_CASE("drms_server") {
    if(auto const *args=getenv("SHYFT_DRMS_SERVER")) {
        int port{0};
        if(sscanf(args,"%d",&port)==1 && port>=0 ) {
            MESSAGE("Starting drms server on port "<<port);
            MESSAGE("type q<enter> to terminate");
            server s;
            s.set_listening_port(port);
            auto used_port=s.start_server();
            MESSAGE("Server started on port "<<used_port);
            string cmd;
            while(true) {
                cin >> cmd;
                if(cmd=="q")
                    break;
            }
        } else {
            MESSAGE("Not able to get valid port number from args:"<<args);
        }
    } else {
        MESSAGE("Skipping manual drms-test, use SHYFT_DRMS_SERVER=<port_no> to start a c++ native drms-server");
    }
}    
    
}
