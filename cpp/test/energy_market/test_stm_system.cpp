#include <doctest/doctest.h>
#include "build_stm_system.h"
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/market_ds.h>
#include <shyft/energy_market/stm/hps_ds.h>

#include <serialize_loop.h>

using namespace shyft::energy_market::hydro_power;
using namespace shyft::core;
using test::serialize_loop;

TEST_SUITE("stm_hps") {
TEST_CASE("stm_hydro_power_system serialization") {
    using namespace shyft::energy_market::stm;
    auto a = test::create_stm_hps();
	// just verify that we got a complete hooked up model:
	auto t=a->find_waterway_by_name("lauvastølsvatn-til-kvilldal");
	CHECK(t != nullptr);
	CHECK(t->gates.size() == 1u);// should be one here..
	CHECK(t->gates[0]->wtr_() == t);
	auto t_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(t);
	CHECK(t_stm != nullptr);
	auto t_hps = t->hps_();
	CHECK(t_hps != nullptr);
	auto s_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::stm_hps>(t_hps);
	CHECK(s_stm != nullptr);
	//
    auto b = test::create_stm_hps();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
        auto b_as_blob = stm_hps::to_blob(b);
        CHECK(b_as_blob.size() > 0);
        auto b_from_blob = stm_hps::from_blob(b_as_blob);
        CHECK(b_from_blob->equal_structure(*b));
    }
    a->clear();
    b->clear();
}

TEST_CASE("stm_hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_stm_hps();
    {
        auto blasjo = a->find_reservoir_by_name("blåsjø");
        CHECK(blasjo != nullptr);
        auto blasjo_ds = blasjo->downstream_units();
        auto blasjo_us = blasjo->upstream_units();
        CHECK(blasjo_ds.size() == 1);
        CHECK(blasjo_ds[0]->name == "saurdal");
        CHECK(blasjo_us.size() == 0);
        auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
        CHECK(vassbotvatn != nullptr);
        auto vassbotvatn_us = vassbotvatn->upstream_units();
        auto vassbotvatn_ds = vassbotvatn->downstream_units();
        CHECK(vassbotvatn_us.size() == 1);
        CHECK(vassbotvatn_us[0]->name == "stølsdal pumpe");
        CHECK(vassbotvatn_ds.size() == 0);
        SUBCASE("remove connection to water-route") {
            hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target_());
            CHECK(vassbotvatn->upstream_units().size() == 0);
        }
        SUBCASE("power_stations up and down streams") {
            auto saurdal = a->find_unit_by_name ("saurdal");
            CHECK(saurdal != nullptr);
            auto rsv_us = saurdal->upstream_reservoirs();
            CHECK(rsv_us.size() == 1);
            CHECK(rsv_us[0]->name == "blåsjø");

            // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
            auto kvilldal = a->find_unit_by_name ("kvilldal_1");
            auto rsv_ds = kvilldal->downstream_reservoir();
            CHECK(rsv_ds != nullptr);
            CHECK(rsv_ds->name == "suldalsvatn");
        }
    }
    a->clear();
    a = nullptr;
}

TEST_CASE("stm_system serialization") {
	using namespace shyft::energy_market::stm;
	auto a = make_shared<stm_system>(1, "ull", "aførre");
	a->market.push_back(make_shared<energy_market_area>(1, "NO1", "xx", a));
	a->market.push_back(make_shared<energy_market_area>(2, "NO2", "xx",a));
	a->market.push_back(make_shared<energy_market_area>(3, "NO3", "xx", a));
	a->market.push_back(make_shared<energy_market_area>(4, "NO4", "xx", a));
	a->hps.push_back(test::create_stm_hps(2,"sørland"));
	a->hps.push_back(test::create_stm_hps(1, "oslo"));
	auto a_blob = stm_system::to_blob(a);
	auto b = stm_system::from_blob(a_blob);
	CHECK(b != nullptr);
	CHECK_EQ(b->name , a->name);
	CHECK_EQ(b->id, a->id);
	CHECK_EQ(b->json, a->json);
	CHECK_EQ(b->market.size(), a->market.size());
	CHECK_EQ(b->hps.size(), a->hps.size());
}

TEST_CASE("stm_system proxy_attributes") {
    using namespace shyft::energy_market::stm;
    auto a = make_shared<stm_system>(1, "ulla", "førre");
    // 1. Check non-existence
    CHECK_EQ(false, a->run_params->n_inc_runs.exists());
    CHECK_EQ(false, a->run_params->n_full_runs.exists());
    CHECK_EQ(false, a->run_params->head_opt.exists());
    CHECK_EQ(false, a->run_params->run_time_axis.exists());
    CHECK_EQ(false, a->run_params->fx_log.exists());

    // 2. Set attributes, and check
    a->run_params->n_inc_runs = 2;
    a->run_params->n_full_runs = 3;
    a->run_params->head_opt = true;
    a->run_params->fx_log = {{from_seconds(0), "a string"}, {from_seconds(1), "another string"}};
    generic_dt ta(vector<utctime>{from_seconds(0), from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)});
    a->run_params->run_time_axis = ta;
    CHECK_EQ(true, a->run_params->n_inc_runs.exists());
    CHECK_EQ(true, a->run_params->n_full_runs.exists());
    CHECK_EQ(true, a->run_params->head_opt.exists());
    CHECK_EQ(true, a->run_params->run_time_axis.exists());
    CHECK_EQ(true, a->run_params->fx_log.exists());
    CHECK_EQ(2, a->run_params->n_inc_runs);
    CHECK_EQ(3, a->run_params->n_full_runs);
    CHECK_EQ(true, a->run_params->head_opt);
    CHECK_EQ(ta, a->run_params->run_time_axis);
    auto msgs = a->run_params->fx_log.get();
    CHECK_EQ(msgs.size(), 2);
    CHECK_EQ(msgs[0].second, "a string");
    CHECK_EQ(msgs[1].second, "another string");

    // 3. Serialize and deserialize, and check that values are persisted
    auto a_blob = stm_system::to_blob(a);
    auto b = stm_system::from_blob(a_blob);
    CHECK(b != nullptr);
    CHECK_EQ(true, b->run_params->n_inc_runs.exists());
    CHECK_EQ(true, b->run_params->n_full_runs.exists());
    CHECK_EQ(true, b->run_params->head_opt.exists());
    CHECK_EQ(true, b->run_params->run_time_axis.exists());
    CHECK_EQ(true, b->run_params->fx_log.exists());

    CHECK_EQ(2, b->run_params->n_inc_runs);
    CHECK_EQ(3, b->run_params->n_full_runs);
    CHECK_EQ(true, b->run_params->head_opt);
    CHECK_EQ(ta, b->run_params->run_time_axis);
    auto bmsgs = b->run_params->fx_log.get();
    CHECK_EQ(bmsgs.size(), 2);
    CHECK_EQ(bmsgs[0].second, "a string");
    CHECK_EQ(bmsgs[1].second, "another string");

}

TEST_CASE("stm_hps_proxy_attributes_missing_hps_throws") {
    // Test that access the proxy attribute dataset throws an error when the owning hps no longer exists.
    using shyft::energy_market::stm::unit;
    SUBCASE("ids_non_const_version") {
        auto u = std::static_pointer_cast<unit>(test::create_simple_hps()->find_unit_by_id(1));
        CHECK_THROWS_AS(unit::ids::ds(u.get()), std::runtime_error);
    }
    SUBCASE("ids_const_version") {
        auto u = std::static_pointer_cast<const unit>(test::create_simple_hps()->find_unit_by_id(1));
        CHECK_THROWS_AS(unit::ids::ds(u.get()), std::runtime_error);
    }
    SUBCASE("rds_non_const_version") {
        auto u = std::static_pointer_cast<unit>(test::create_simple_hps()->find_unit_by_id(1));
        CHECK_THROWS_AS(unit::rds::ds(u.get()), std::runtime_error);
    }
    SUBCASE("rds_const_version") {
        auto u = std::static_pointer_cast<const unit>(test::create_simple_hps()->find_unit_by_id(1));
        CHECK_THROWS_AS(unit::rds::ds(u.get()), std::runtime_error);
    }
}

TEST_CASE("stm_sys_proxy_attributes_missing_sys_throws") {
    using shyft::energy_market::stm::energy_market_area;
    SUBCASE("ids_non_const_version") {
        auto m = test::create_simple_system()->market[0];
        CHECK_THROWS_AS(energy_market_area::ids::ds(m.get()), std::runtime_error);
    }
    SUBCASE("ids_const_version") {
        auto m = std::static_pointer_cast<const energy_market_area>(test::create_simple_system()->market[0]);
        CHECK_THROWS_AS(energy_market_area::ids::ds(m.get()), std::runtime_error);
    }
    SUBCASE("rds_non_const_version") {
        auto m = test::create_simple_system()->market[0];
        CHECK_THROWS_AS(energy_market_area::rds::ds(m.get()), std::runtime_error);
    }
    SUBCASE("rds_const_version") {
        auto m = std::static_pointer_cast<const energy_market_area>(test::create_simple_system()->market[0]);
        CHECK_THROWS_AS(energy_market_area::rds::ds(m.get()), std::runtime_error);
    }
}
}