#include <doctest/doctest.h>
#include <build_stm_system.h>
#include <shyft/energy_market/stm/srv/dstm_subscription.h>


TEST_SUITE("dstm") {
    using shyft::core::subscription::manager;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using std::make_shared;
    
    auto mdl = test::create_simple_system();
    TEST_CASE("generate_proxy_id") {
        auto hps = mdl->hps[0];
        auto rsv = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps->reservoirs[0]);
        auto u = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(hps->units[0]);
        auto pp = std::dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(hps->power_plants[0]);
        
        auto sm = make_shared<manager>();
        // Check generating proxy id:
        CHECK_EQ(rsv->inflow.url("M1/"), "M1/HPS1/R1/A14");
        CHECK_EQ(u->production_min.url("M1/"), "M1/HPS1/U1/A3");
        CHECK_EQ(pp->discharge_schedule.url("AnyString"), "AnyStringHPS1/P2/A8");

        // Generating proxy ID for stm_system:
        CHECK_EQ(mdl->run_params->n_inc_runs.url(), "M1/A0");
        CHECK_EQ(mdl->run_params->n_full_runs.url(), "M1/A1");
        CHECK_EQ(mdl->run_params->head_opt.url(), "M1/A2");
    }
}
