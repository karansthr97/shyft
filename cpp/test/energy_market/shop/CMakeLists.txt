# CMake configuration for tests
if(SHYFT_WITH_SHOP)
    set(sources
        runner.cpp
        generate_shop_cxx_api.cpp
        test_basic_api.cpp
        test_basic_object.cpp
        test_model_simple.cpp
        test_model_simple_mt.cpp
        test_shop_proxy.cpp
        test_time_resolution.cpp
        test_stm_shop_adapter.cpp
        test_stm_shop_command.cpp
        test_stm_shop_topology.cpp
        test_stm_shop_optimize.cpp
        test_stm_shop_migrated.cpp
        test_stm_srv_shop_optimize.cpp
    )
else()
    set(sources
        runner.cpp
        test_stm_shop_basic.cpp
        test_stm_shop_optimize.cpp
        test_stm_shop_migrated.cpp
        test_stm_srv_shop_optimize.cpp
    )
endif()
set(target "test_stm_shop")
add_executable(${target} ${sources})
    set_target_properties(${target} PROPERTIES INSTALL_RPATH "$ORIGIN/../../shyft/lib")
    target_include_directories(${target} BEFORE PRIVATE ${CMAKE_SOURCE_DIR}/cpp/test/energy_market/shop)
    target_link_libraries(${target} stm_shop)
    if(APPLE)
        target_link_libraries(${target} "dl")
    elseif(NOT MSVC)
        target_link_libraries(${target} "dl" "stdc++fs")
    endif()
    install(TARGETS ${target} DESTINATION ${CMAKE_SOURCE_DIR}/bin/$<CONFIG>)

if(BUILD_COVERAGE)
    include(${CMAKE_SOURCE_DIR}/build_support/CodeCoverage.cmake)
    APPEND_COVERAGE_COMPILER_FLAGS()
    set(COVERAGE_GCOVR_EXCLUDES '/usr/include/*' '*/boost/*' '*/python/*' '*/armadillo*' '*/dlib/*' '*/doctest/*' '${SHYFT_DEPENDENCIES}/include/*' )
    SETUP_TARGET_FOR_COVERAGE_GCOVR_HTML(
        NAME ${target}_coverage
        EXECUTABLE ${target}
        DEPENDENCIES ${target} stm_shop
    )
endif()

# Copy runtime dependencies to unit test application folder to be able to run tests without prerequisites.
if (NOT DEFINED shop_api_CPLEX_NAME)
    message(FATAL_ERROR "Missing required variable shop_api_CPLEX_NAME from shop_api package")
endif()
add_custom_command(TARGET ${target} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different
    ${SHYFT_DEPENDENCIES}/lib/SHOP_license.dat $<TARGET_FILE_DIR:${target}>)
add_custom_command(TARGET ${target} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different
    ${SHYFT_DEPENDENCIES}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}${shop_api_CPLEX_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX} $<TARGET_FILE_DIR:${target}>)
add_custom_command(TARGET ${target} POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy_if_different
    ${SHYFT_DEPENDENCIES}/lib/$<IF:$<CONFIG:Debug>,debug,release>/shop_cplex_interface${CMAKE_SHARED_LIBRARY_SUFFIX} $<TARGET_FILE_DIR:${target}>)

# Find all doctest test cases and add as CTest tests
# notice that this will fail if cases includes a comma or other stuff
find_file(doctest_ADAPTER NAMES "DoctestAdapter.cmake" PATHS ${CMAKE_SOURCE_DIR}/build_support)
if(doctest_ADAPTER)
    include(${doctest_ADAPTER})
    add_doctests(${target} ${sources} TRUE FALSE)
else()
    message(WARNING "Doctest adapter CMake script not found, CMake tests not generated!")
endif()
