// --------------------------------------------------------------------------------------------------------------------------------------------
// OLD!
// Original version working with Shop api before version 0.3.0, where creek objects were converted to junction internally in shop!
// Keeping a copy of this original version because there were many differences with the new version, and we may need to compare for some time!
// --------------------------------------------------------------------------------------------------------------------------------------------
#if 0
#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/core/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>

#ifdef _DEBUG
const bool silent_mode = false; // Set this to false for improved debugging (in case it gets included in commit we should keep default false for release builds)
const bool print_topology_graph = false; // Set this to true for improved debugging (in case it gets included in commit we should keep default false for release builds)
#else
const bool silent_mode = true;
const bool print_topology_graph = false;
#endif

// Log callbacks
struct test_stm_shop_log_handler : shop_stream_logger {
	test_stm_shop_log_handler() : shop_stream_logger{ "test_stm_shop_log_handler", std::cout } { enter(); }
	~test_stm_shop_log_handler() { exit(); }
};

TEST_SUITE("stm_shop_basic") {

TEST_CASE("Penstock toplogy")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	SUBCASE("toplogy 01")
	{
		// R
		// |
		// | <-- penstock
		// A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);
		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
	}

	SUBCASE("toplogy 02")
	{
		//   R
		//   |
		//  / \  <-- penstocks
		// A   A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		unit_ g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
		connect(wtr_penstock1).output_to(g1);
		connect(wtr_penstock2).output_to(g2);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
		auto penstock_g2 = shop_emitter::get_penstock(g2);
		CHECK(penstock_g2 == wtr_penstock2); // g2: penstock 2
	}

	SUBCASE("toplogy 03")
	{
		//     R
		//     |
		//    /  \  <-- penstocks
		//  / \  / \
		// A   AA   A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		unit_ g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
		unit_ g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
		unit_ g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
		waterway_ wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
		waterway_ wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
		waterway_ wtr_inlet2a = builder.create_tunnel(id++, "waterroute inlet 2A"s, ""s);
		waterway_ wtr_inlet2b = builder.create_tunnel(id++, "waterroute inlet 2B"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
		connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b);
		connect(wtr_penstock2).output_to(wtr_inlet2a).output_to(wtr_inlet2b);
		connect(wtr_inlet1a).output_to(g1);
		connect(wtr_inlet1b).output_to(g2);
		connect(wtr_inlet2a).output_to(g3);
		connect(wtr_inlet2b).output_to(g4);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
		auto penstock_g2 = shop_emitter::get_penstock(g2);
		CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
		auto penstock_g3 = shop_emitter::get_penstock(g3);
		CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
		auto penstock_g4 = shop_emitter::get_penstock(g4);
		CHECK(penstock_g4 == wtr_penstock2); // g4: penstock 2
	}

	SUBCASE("toplogy 04a")
	{
		//   R
		//   |
		//  /  \  <-- penstocks
		// A  / \
		//   A   A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		unit_ g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
		unit_ g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
		unit_ g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
		waterway_ wtr_inlet2a = builder.create_tunnel(id++, "waterroute inlet 2A"s, ""s);
		waterway_ wtr_inlet2b = builder.create_tunnel(id++, "waterroute inlet 2B"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
		connect(wtr_penstock2).output_to(wtr_inlet2a).output_to(wtr_inlet2b);
		connect(wtr_penstock1).output_to(g1);
		connect(wtr_inlet2a).output_to(g2);
		connect(wtr_inlet2b).output_to(g3);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
		auto penstock_g2 = shop_emitter::get_penstock(g2);
		CHECK(penstock_g2 == wtr_penstock2); // g2: penstock 2
		auto penstock_g3 = shop_emitter::get_penstock(g3);
		CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
	}

	SUBCASE("toplogy 04b")
	{
		//     R
		//     |
		//    / \  <-- penstocks
		//  / \  A
		// A   A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		unit_ g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
		unit_ g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
		unit_ g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
		waterway_ wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
		waterway_ wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1).output_to(wtr_penstock2);
		connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b);
		connect(wtr_penstock2).output_to(g3);
		connect(wtr_inlet1a).output_to(g1);
		connect(wtr_inlet1b).output_to(g2);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
		auto penstock_g2 = shop_emitter::get_penstock(g2);
		CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
		auto penstock_g3 = shop_emitter::get_penstock(g3);
		CHECK(penstock_g3 == wtr_penstock2); // g3: penstock 2
	}

	SUBCASE("toplogy 05")
	{
		//         R
		//         |
		//         |
		//         |
		//    /  /   \    \  <-- penstocks
		//  /|\ A   //|\\  A
		//  AAA     AAAAA
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		unit_ g2 = builder.create_unit(id++, "aggregate 2"s, ""s);
		unit_ g3 = builder.create_unit(id++, "aggregate 3"s, ""s);
		unit_ g4 = builder.create_unit(id++, "aggregate 4"s, ""s);
		unit_ g5 = builder.create_unit(id++, "aggregate 5"s, ""s);
		unit_ g6 = builder.create_unit(id++, "aggregate 6"s, ""s);
		unit_ g7 = builder.create_unit(id++, "aggregate 7"s, ""s);
		unit_ g8 = builder.create_unit(id++, "aggregate 8"s, ""s);
		unit_ g9 = builder.create_unit(id++, "aggregate 9"s, ""s);
		unit_ g10 = builder.create_unit(id++, "aggregate 10"s, ""s);
		waterway_ wtr_main1 = builder.create_tunnel(id++, "waterroute main tunnel 1"s, ""s);
		waterway_ wtr_main2 = builder.create_tunnel(id++, "waterroute main tunnel 2"s, ""s);
		waterway_ wtr_main3 = builder.create_tunnel(id++, "waterroute main tunnel 3"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_penstock2 = builder.create_tunnel(id++, "waterroute penstock 2"s, ""s);
		waterway_ wtr_penstock3 = builder.create_tunnel(id++, "waterroute penstock 3"s, ""s);
		waterway_ wtr_penstock4 = builder.create_tunnel(id++, "waterroute penstock 4"s, ""s);
		waterway_ wtr_inlet1a = builder.create_tunnel(id++, "waterroute inlet 1A"s, ""s);
		waterway_ wtr_inlet1b = builder.create_tunnel(id++, "waterroute inlet 1B"s, ""s);
		waterway_ wtr_inlet1c = builder.create_tunnel(id++, "waterroute inlet 1C"s, ""s);
		waterway_ wtr_inlet3a = builder.create_tunnel(id++, "waterroute inlet 3A"s, ""s);
		waterway_ wtr_inlet3b = builder.create_tunnel(id++, "waterroute inlet 3B"s, ""s);
		waterway_ wtr_inlet3c = builder.create_tunnel(id++, "waterroute inlet 3C"s, ""s);
		waterway_ wtr_inlet3d = builder.create_tunnel(id++, "waterroute inlet 3D"s, ""s);
		waterway_ wtr_inlet3e = builder.create_tunnel(id++, "waterroute inlet 3E"s, ""s);
		connect(wtr_main1).input_from(rsv).output_to(wtr_main2);
		connect(wtr_main2).output_to(wtr_main3);
		connect(wtr_main3).output_to(wtr_penstock1).output_to(wtr_penstock2).output_to(wtr_penstock3).output_to(wtr_penstock4);
		connect(wtr_penstock1).output_to(wtr_inlet1a).output_to(wtr_inlet1b).output_to(wtr_inlet1c);
		connect(wtr_penstock2).output_to(g4);
		connect(wtr_penstock3).output_to(wtr_inlet3a).output_to(wtr_inlet3b).output_to(wtr_inlet3c).output_to(wtr_inlet3d).output_to(wtr_inlet3e);
		connect(wtr_penstock4).output_to(g10);
		connect(wtr_inlet1a).output_to(g1);
		connect(wtr_inlet1b).output_to(g2);
		connect(wtr_inlet1c).output_to(g3);
		connect(wtr_inlet3a).output_to(g5);
		connect(wtr_inlet3b).output_to(g6);
		connect(wtr_inlet3c).output_to(g7);
		connect(wtr_inlet3d).output_to(g8);
		connect(wtr_inlet3e).output_to(g9);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
		auto penstock_g2 = shop_emitter::get_penstock(g2);
		CHECK(penstock_g2 == wtr_penstock1); // g2: penstock 1
		auto penstock_g3 = shop_emitter::get_penstock(g3);
		CHECK(penstock_g3 == wtr_penstock1); // g3: penstock 1
		auto penstock_g4 = shop_emitter::get_penstock(g4);
		CHECK(penstock_g4 == wtr_penstock2); // g4: penstock 2
		auto penstock_g5 = shop_emitter::get_penstock(g5);
		CHECK(penstock_g5 == wtr_penstock3); // g5: penstock 3
		auto penstock_g6 = shop_emitter::get_penstock(g6);
		CHECK(penstock_g6 == wtr_penstock3); // g6: penstock 3
		auto penstock_g7 = shop_emitter::get_penstock(g7);
		CHECK(penstock_g7 == wtr_penstock3); // g7: penstock 3
		auto penstock_g8 = shop_emitter::get_penstock(g8);
		CHECK(penstock_g8 == wtr_penstock3); // g8: penstock 3
		auto penstock_g9 = shop_emitter::get_penstock(g9);
		CHECK(penstock_g9 == wtr_penstock3); // g9: penstock 3
		auto penstock_g10 = shop_emitter::get_penstock(g10);
		CHECK(penstock_g10 == wtr_penstock4); // g10: penstock 4
	}

	SUBCASE("toplogy 06")
	{
		// R
		// | <-- penstock or main tunnel?
		// A
		//
		// This should perhaps be considered an illegal topology, and require at least one penstock and one main tunnel waterroutes,
		// this test shows how the penstock algorithm currently handles it: Returning the single waterroute as the penstock.
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
	}

	SUBCASE("toplogy 07")
	{
		// R 
		// |
		// | <-- (modelling this as the penstock, with an inlet downstream, is not supported)
		// | <-- penstock (modelling this as an inlet, with the penstock upstream, is not supported)
		// A 
		//
		// This shows the case where we would have ambiguous topology between
		// aggregate-inlet-penstock-main-rsv and aggregate-penstock-main1-main2-rsv,
		// but where the assumption that inlet can only be modelled when necessary (shared penstocks)
		// makes it unambiguous where the penstock is.
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		waterway_ wtr_main1 = builder.create_tunnel(id++, "waterroute main tunnel 1"s, ""s);
		waterway_ wtr_main2 = builder.create_tunnel(id++, "waterroute main tunnel2 "s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_main1).input_from(rsv).output_to(wtr_main2);
		connect(wtr_main2).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		auto penstock_g1 = shop_emitter::get_penstock(g1);
		CHECK(penstock_g1 == wtr_penstock1); // g1: penstock 1
	}
}

TEST_CASE("Gate topology")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;

	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const shyft::core::utcperiod t_period{ t_begin, t_end };
	const auto t_step = shyft::core::deltahours(1);

	SUBCASE("topology 01")
	{
		//               R1
		//  bypass --> / | <-- main tunnel (but not a junction)
		//            |  | <-- main tunnel (but not a junction)
		//            |  | <-- penstock
		//            |  A
		//             \ |
		//               R2
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
		waterway_ wtr_bypass = builder.create_tunnel(id++, "waterroute bypass"s, ""s);

		connect(wtr_main).input_from(r1).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);
		connect(g1).output_to(wtr_outlet);
		connect(wtr_outlet).output_to(r2);
		connect(wtr_bypass).input_from(r1, hydro_power::bypass).output_to(r2);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
		api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
		api.emitter.handle_reservoir_output(r1);

		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
		CHECK(api.emitter.objects.size<shop_creek_intake>() == 0);
		CHECK(api.emitter.objects.size<shop_junction>() == 0);
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(api.emitter.objects.size<shop_gate>() == 1);
		CHECK(api.emitter.objects.id_of<shop_gate>(wtr_bypass));
	}

	SUBCASE("topology 02")
	{
		// R1
		// | <-- river with gate
		// R2
		// | <-- main
		// | <-- penstock
		// A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_r1_r2 = builder.create_river(id++, "waterroute reservoir 1-2 river"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_r1_r2).input_from(r1).output_to(r2);
		connect(r2).output_to(wtr_main, hydro_power::main);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00013);

		// No stm gate, but shop emitter needs to create one representing the r1-r2 connection
		{
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			api.emitter.handle_reservoir_output(r1);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
			CHECK(api.emitter.objects.size<shop_creek_intake>() == 0);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(api.emitter.objects.size<shop_gate>() == 1);
			CHECK(api.emitter.objects.id_of<shop_gate>(wtr_r1_r2));
		}
		// Add one stm gate, that should be used as the shop gate as well
		{
			//auto wtr_r1_r2_gt = std::make_shared<gate>(id++, "waterroute reservoir 1-2 river gate 1", "");
			auto wtr_r1_r2_gt = waterway::add_gate(wtr_r1_r2, id++, "waterroute reservoir 1-2 river gate 1", "");
			wtr_r1_r2_gt->discharge_schedule= apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
				{ 48.0, 40.0, 0.0, 0.0 } };
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			api.emitter.handle_reservoir_output(r1);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
			CHECK(api.emitter.objects.size<shop_creek_intake>() == 0);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(api.emitter.objects.size<shop_gate>() == 1);
			CHECK(api.emitter.objects.id_of<shop_gate>(wtr_r1_r2_gt)); // Should this time be a shop_gate representing the actual stm gate, not the water route.
		}
		// Add two more stm gates (parallel to the first)
		{
			auto wtr_r1_r2_gt2 = waterway::add_gate(wtr_r1_r2, id++, "waterroute reservoir 1-2 river gate 2", "");
			wtr_r1_r2_gt2->discharge_schedule = apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
				{ 48.0, 40.0, 0.0, 0.0 } };
			auto wtr_r1_r2_gt3 = waterway::add_gate(wtr_r1_r2, id++, "waterroute reservoir 1-2 river gate 3", "");
			wtr_r1_r2_gt3->discharge_schedule= apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
				{ 48.0, 40.0, 0.0, 0.0 } };
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			api.emitter.handle_reservoir_output(r1);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
			CHECK(api.emitter.objects.size<shop_creek_intake>() == 0);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(api.emitter.objects.size<shop_gate>() == 3);
			CHECK(api.emitter.objects.id_of<shop_gate>(wtr_r1_r2_gt2));
			CHECK(api.emitter.objects.id_of<shop_gate>(wtr_r1_r2_gt3));
		}
	}

	SUBCASE("topology 03")
	{
		// R1
		// | <-- river with gate
		// | <-- superfluous segment should be ignored
		// | <-- superfluous segment should be ignored
		// R2
		// | <-- main
		// | <-- penstock
		// A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir up"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir down"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_r1_out1 = builder.create_river(id++, "waterroute reservoir 1 output segment 1"s, ""s);
		waterway_ wtr_r1_out2 = builder.create_river(id++, "waterroute reservoir 1 output segment 2"s, ""s);
		waterway_ wtr_r1_out3 = builder.create_river(id++, "waterroute reservoir 1 output segment 3"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_r1_out1).input_from(r1).output_to(wtr_r1_out2);
		connect(wtr_r1_out2).output_to(wtr_r1_out3);
		connect(wtr_r1_out3).output_to(r2);
		connect(r2).output_to(wtr_main, hydro_power::main);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00013);

		// No stm gate, but shop emitter needs to create one representing the r1-r2 connection
		{
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			api.emitter.handle_reservoir_output(r1);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
			CHECK(api.emitter.objects.size<shop_creek_intake>() == 0);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(api.emitter.objects.size<shop_gate>() == 1);
			CHECK(api.emitter.objects.id_of<shop_gate>(wtr_r1_out1));
		}
	}
}

TEST_CASE("Junction toplogy")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;

	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const shyft::core::utcperiod t_period{ t_begin, t_end };
	const auto t_step = shyft::core::deltahours(1);

	SUBCASE("schedule transformation")
	{
		// JUNCTION_GATE schedule is a value between 1 and n (2) indicating which one branch has open gate.
		// Value 0 or >n means all are closed.
		// With licensed (password protected) functionality "Junction gate as junction" (SHOP_JUNCG_TWO_WAY),
		// a value -2 means the junction gate is to function as an ordinary junction.
		{
			apoint_ts a;
			shyft::time_axis::generic_dt ta{ t_begin, t_step, 5 };
			a = apoint_ts{ ta, { 0.0, 0.0, 20.0, 30.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			auto a_values = a.values();
			CHECK(a_values == std::vector<double>{0.0, 0.0, 20.0, 30.0, 0.0});
		}
		{
			apoint_ts a{ shyft::time_axis::generic_dt{ vector<utctime>{t_begin, t_end} }, 0.0 };
			shyft::time_axis::generic_dt ta{ t_begin, t_step, 5 };
			apoint_ts b{ ta, { 0.0, 0.0, 20.0, 30.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			auto ab = a + b;
			auto ab_values = ab.values();
			auto ab_ta = ab.time_axis();
			CHECK(ab_values == std::vector<double>{0.0, 0.0, 20.0, 30.0, 0.0});
		}
		{
			shyft::time_axis::generic_dt ta{ t_begin, t_step, 5 };
			apoint_ts a{ ta, { 0.0, 10.0, 0.0, 10.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			apoint_ts b{ ta, { 0.0, 0.0, 20.0, 30.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			auto a_bool = a.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 1.0, 0.0);
			auto a_bool_values = a_bool.values();
			auto b_bool = b.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 2.0, 0.0);
			auto b_bool_values = b_bool.values();
			auto ab_code = a_bool + b_bool;
			auto ab_code_values = ab_code.values();
			CHECK(ab_code_values == std::vector<double>{0.0, 1.0, 2.0, 3.0, 0.0});
			// Transform any 3.0 into -2.0
			std::transform(std::cbegin(ab_code_values), std::cend(ab_code_values), std::begin(ab_code_values), [](double v) {return v > 2.5 ? -2.0 : v; });
			CHECK(ab_code_values == std::vector<double>{0.0, 1.0, 2.0, -2.0, 0.0});
		}
		{
			shyft::time_axis::generic_dt ta_hours{ { t_begin, t_begin + deltahours(1), t_begin + deltahours(2), t_begin + deltahours(3), t_begin + deltahours(4), t_begin + deltahours(5) } };
			apoint_ts a{ ta_hours, { 0.0, 10.0, 0.0, 10.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			shyft::time_axis::generic_dt ta_min15{ { t_begin, t_begin + deltaminutes(15), t_begin + 2*deltaminutes(15), t_begin + 3*deltaminutes(15), t_begin + 4*deltaminutes(15), t_begin + 5*deltaminutes(15) } };
			apoint_ts b{ ta_min15, { 0.0, 0.0, 20.0, 30.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			auto a_bool = a.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 1.0, 0.0);
			auto a_bool_values = a_bool.values();
			auto b_bool = b.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 2.0, 0.0);
			auto b_bool_values = b_bool.values();
			auto ab_code = a_bool + b_bool;
			auto ab_code_values = ab_code.values();
			CHECK(ab_code_values == std::vector<double>{0.0, 0.0, 2.0, 2.0, 1.0});
			auto ba_code = b_bool + a_bool;
			auto ba_code_values = ba_code.values();
			CHECK(ba_code_values == ab_code_values);
		}
		{
			/*
			shyft::time_axis::point_dt ta_single{ { t_begin, t_begin + utctimespan(1)} };
			apoint_ts a{ ta_single, { 0.0 }, shyft::time_series::POINT_AVERAGE_VALUE };
			shyft::time_axis::generic_dt ta_min15{ { t_begin, t_begin + deltaminutes(15), t_begin + 2 * deltaminutes(15), t_begin + 3 * deltaminutes(15), t_begin + 4 * deltaminutes(15), t_begin + 5 * deltaminutes(15) } };
			apoint_ts b{ ta_min15, { 0.0, 0.0, 20.0, 30.0, 0.0}, shyft::time_series::POINT_AVERAGE_VALUE };
			auto a_bool = a.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 1.0, 0.0);
			auto a_bool_values = a_bool.values();
			auto b_bool = b.inside(shyft::time_series::EPS, std::numeric_limits<double>::max(), shyft::nan, 2.0, 0.0);
			auto b_bool_values = b_bool.values();
			auto ab_code = a_bool + b_bool;
			auto ab_code_values = ab_code.values();
			CHECK(ab_code_values == std::vector<double>{0.0, 0.0, 2.0, 2.0, 1.0});
			auto ba_code = b_bool + a_bool;
			auto ba_code_values = ba_code.values();
			CHECK(ba_code_values == ab_code_values);
			*/
		}

	}

	SUBCASE("topology 01")
	{
		// R
		// | <-- main tunnel (but not a junction)
		// | <-- main tunnel (but not a junction)
		// | <-- penstock
		// A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_main).input_from(rsv).output_to(wtr_penstock1);
		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		connect(wtr_penstock1).output_to(g1);

		// Find junctions (should be none)
		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(rsv, std::move(api.adapter.to_shop(rsv)));
		if (print_topology_graph) api.export_topology(std::cout);
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		CHECK(api.emitter.objects.size<shop_junction>() == 0);
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(input_sink_id.t_id == shop_reservoir::t_id);
		CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_reservoir>(rsv).id);
	}
	SUBCASE("topology 02")
	{
		// R  R
		// \ /
		//  | <-- main/junction
		//  | <-- penstock
		//  A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_r1_main = builder.create_tunnel(id++, "waterroute reservoir 1 main tunnel"s, ""s);
		waterway_ wtr_r2_main = builder.create_tunnel(id++, "waterroute reservoir 2 main tunnel"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_r1_main).input_from(r1).output_to(wtr_main);
		connect(wtr_r2_main).input_from(r2).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_r1_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r1_main->head_loss_coeff.get()->emplace(t_begin, 0.00011);
		wtr_r2_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r2_main->head_loss_coeff.get()->emplace(t_begin, 0.00012);
		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00013);

		{
			// Find junctions
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(input_sink_id.t_id == shop_junction::t_id);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(api.emitter.objects.size<shop_junction>() == 1);
			auto it = api.emitter.objects.begin<shop_junction>();
			CHECK(it->first.id == wtr_main->id); // The entry in the shop_junctions should be the stm water route wtr_main 
			CHECK(it->second.id == input_sink_id.id); // Return value should be the shop id
		}
		{
			// Add gates
			auto wtr_r1_main_gt = waterway::add_gate(wtr_r1_main, id++, "waterroute reservoir 1 main tunnel gate", "");
			wtr_r1_main_gt->discharge_schedule = apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
				{ 48.0, 40.0, 0.0, 0.0 } };

			//auto wtr_r2_main_gt = std::make_shared<gate>(id++, "waterroute reservoir 2 main tunnel gate", "");
			auto wtr_r2_main_gt = waterway::add_gate(wtr_r2_main, id++, "waterroute reservoir 2 main tunnel gate", "");
			wtr_r2_main_gt->discharge_schedule = apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
				{ 0.0, 40.0, 0.0, 35.0 } };

			// Find junction gates - Note: Requires license!
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
			api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
			auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(input_sink_id.t_id == shop_junction_gate::t_id);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 1);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			auto git = api.emitter.objects.begin<shop_junction_gate>();
			CHECK(git->first.id == wtr_main->id);
			CHECK(git->second.id == input_sink_id.id);
		}
	}
	SUBCASE("topology 03")
	{
		// R  R R  R
		//  \/  \ /
		//    \ /  <-- junctions
		//     | <-- main/junction
		//     | <-- penstock
		//     A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		reservoir_ r3 = builder.create_reservoir(id++, "reservoir 3"s, ""s);
		reservoir_ r4 = builder.create_reservoir(id++, "reservoir 4"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_r1_out = builder.create_tunnel(id++, "waterroute reservoir 1 output tunnel"s, ""s);
		waterway_ wtr_r2_out = builder.create_tunnel(id++, "waterroute reservoir 2 output tunnel"s, ""s);
		waterway_ wtr_r3_out = builder.create_tunnel(id++, "waterroute reservoir 3 output tunnel"s, ""s);
		waterway_ wtr_r4_out = builder.create_tunnel(id++, "waterroute reservoir 4 output tunnel"s, ""s);
		waterway_ wtr_r1_r2 = builder.create_tunnel(id++, "waterroute reservoir 1 and 2 junction"s, ""s);
		waterway_ wtr_r3_r4 = builder.create_tunnel(id++, "waterroute reservoir 3 and 4 junction"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);

		connect(wtr_r1_out).input_from(r1).output_to(wtr_r1_r2);
		connect(wtr_r2_out).input_from(r2).output_to(wtr_r1_r2);
		connect(wtr_r3_out).input_from(r3).output_to(wtr_r3_r4);
		connect(wtr_r4_out).input_from(r4).output_to(wtr_r3_r4);

		connect(wtr_main).input_from(wtr_r1_r2);
		connect(wtr_main).input_from(wtr_r3_r4);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_r1_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r1_out->head_loss_coeff.get()->emplace(t_begin, 0.00011);
		wtr_r2_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r2_out->head_loss_coeff.get()->emplace(t_begin, 0.00012);
		wtr_r3_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r3_out->head_loss_coeff.get()->emplace(t_begin, 0.00013);
		wtr_r4_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r4_out->head_loss_coeff.get()->emplace(t_begin, 0.00014);
		wtr_r1_r2->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r1_r2->head_loss_coeff.get()->emplace(t_begin, 0.00015);
		wtr_r3_r4->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r3_r4->head_loss_coeff.get()->emplace(t_begin, 0.00016);
		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00017);

		// Add gates so that wtr_main becomes a junction_gate, Shop API does not accept connecting two junctions as input to another junction!
		auto wtr_r1_r2_gt = waterway::add_gate(wtr_r1_r2, id++, "waterroute reservoir 1 and 2 junction gate", "");
		wtr_r1_r2_gt->discharge_schedule = apoint_ts{
			point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
			{ 48.0, 40.0, 0.0, 0.0 } };
		auto wtr_r3_r4_gt = waterway::add_gate(wtr_r3_r4, id++, "waterroute reservoir 3 and 4 junction gate", "");
		wtr_r3_r4_gt->discharge_schedule = apoint_ts{
			point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 2 * t_step, t_begin + 2 * t_step, t_end }},
			{ 48.0, 40.0, 0.0, 0.0 } };
		// Find junctions
		shop_system api{  t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
		api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
		api.emitter.objects.add(r3, std::move(api.adapter.to_shop(r3)));
		api.emitter.objects.add(r4, std::move(api.adapter.to_shop(r4)));
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(input_sink_id.t_id == shop_junction_gate::t_id);
		// Should now have 3 junctions, represented by wtr_r1_r2, wtr_r3_r4 and wtr_main
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 1);
		CHECK(api.emitter.objects.size<shop_junction>() == 2);
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_r1_r2));
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_r3_r4));
		CHECK(api.emitter.objects.id_of<shop_junction_gate>(wtr_main));
		CHECK(api.emitter.objects.id_of<shop_junction_gate>(wtr_main).id == input_sink_id.id); // Return value should be the shop id of the lowest input object
	}
}

TEST_CASE("Creek toplogy")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;

	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const shyft::core::utcperiod t_period{ t_begin, t_end };
	const auto t_step = shyft::core::deltahours(1);

	SUBCASE("topology 01")
	{
		// STM:
		//    R C
		//    |/
		//    | <-- main tunnel (junction in stm but not in shop)
		//    | <-- penstock
		//    A
		// Shop-emitter:
		//    R
		//    |
		//    C
		//    |
		//    A
		// Shop API/kernel:
		//    R C
		//    |/
		//    J <-- junction object created automatically when adding the creek
		//    |
		//    A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		reservoir_ crk = builder.create_reservoir(id++, "creek"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_rsv = builder.create_tunnel(id++, "waterroute rsv output"s, ""s);
		waterway_ wtr_crk = builder.create_tunnel(id++, "waterroute crk output"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_rsv).input_from(rsv).output_to(wtr_main);
		connect(wtr_crk).input_from(crk).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		{
			// Traverse tunnel topology (must manually fill shop_reservoirs and shop_creek_intakes since we are bypassing the parent function)
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(rsv, std::move(api.adapter.to_shop(rsv)));
			api.emitter.objects.add(crk, std::move(api.adapter.to_shop_creek(crk)));
			auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(input_sink_id.t_id == shop_creek_intake::t_id);
			//CHECK(api.shop_reservoirs.size() == 1);
			//CHECK(api.shop_creek_intakes.size() == 1);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_creek_intake>(crk).id);
		}
		{
			// Send complete model to shop, which will go through all traversing code without "cheating"
			rsv->volume_descr = make_shared<map<utctime, xy_point_curve_>>(); // Reservoirs must have volume_descr attribute, to separate them from creeks!
			rsv->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(std::vector<double>{0.0, 10.0}, std::vector<double>{100.0, 200.0}));

			stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
			auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
			market->price = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end }},
				{ 48.0, 40.0, 35.0 } };
			market->max_buy = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
				{ 10.0, 9999.0 } };
			market->max_sale = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
				{ 10.0, 9999.0 } };
			market->load = apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
				{ 90.0, 80.0, 60.0, 10.0, 0.0 } };
			stm->market.push_back(market);
			stm->hps.push_back(hps);

			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.install_logger(std::make_shared<test_stm_shop_log_handler>());
			api.emitter.to_shop(stm);
			if (print_topology_graph) api.export_topology(std::cout);
			REQUIRE(api.emitter.objects.size<shop_reservoir>() == 1);
			CHECK(api.emitter.objects.begin<shop_reservoir>()->first.id == rsv->id);
			REQUIRE(api.emitter.objects.size<shop_creek_intake>() == 1);
			CHECK(api.emitter.objects.begin<shop_creek_intake>()->first.id == crk->id);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		}
	}

	SUBCASE("topology 02")
	{
		// STM:
		//    C1 R C2
		//      \|/
		//       | <-- main tunnel (junction in stm but not in shop)
		//       | <-- penstock
		//       A
		// Shop-emitter:
		//       R
		//       |
		//       C1 (or C2?)
		//       |
		//       C2 (or C1?)
		//       |
		//       A
		// Shop API/kernel:
		//    C1 R
		//      \|
		//    C2 J <-- junction object created automatically when adding the creek
		//      \|
		//       J <-- junction object created automatically when adding the creek
		//       A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
		reservoir_ crk1 = builder.create_reservoir(id++, "creek 1"s, ""s);
		reservoir_ crk2 = builder.create_reservoir(id++, "creek 2"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate 1"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_rsv = builder.create_tunnel(id++, "waterroute rsv output"s, ""s);
		waterway_ wtr_crk1 = builder.create_tunnel(id++, "waterroute crk1 output"s, ""s);
		waterway_ wtr_crk2 = builder.create_tunnel(id++, "waterroute crk2 output"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock1 = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_crk1).input_from(crk1).output_to(wtr_main);
		connect(wtr_rsv).input_from(rsv).output_to(wtr_main);
		connect(wtr_crk2).input_from(crk2).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock1);
		connect(wtr_penstock1).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		{
			// Traverse tunnel topology (must manually fill shop_reservoirs and shop_creek_intakes since we are bypassing the parent function)
			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.emitter.objects.add(rsv, std::move(api.adapter.to_shop(rsv)));
			api.emitter.objects.add(crk1, std::move(api.adapter.to_shop_creek(crk1)));
			api.emitter.objects.add(crk2, std::move(api.adapter.to_shop_creek(crk2)));
			auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
			if (print_topology_graph) api.export_topology(std::cout);
			CHECK(input_sink_id.t_id == shop_creek_intake::t_id);
			//CHECK(api.shop_reservoirs.size() == 1);
			//CHECK(api.shop_creek_intakes.size() == 1);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_creek_intake>(crk2).id);
		}
		{
			// Send complete model to shop, which will go through all traversing code without "cheating"
			rsv->volume_descr = make_shared<map<utctime, xy_point_curve_>>(); // Reservoirs must have volume_descr attribute, to separate them from creeks!
			rsv->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(std::vector<double>{0.0, 10.0}, std::vector<double>{100.0, 200.0}));

			stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
			auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
			market->price = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end }},
				{ 48.0, 40.0, 35.0 } };
			market->max_buy = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
				{ 10.0, 9999.0 } };
			market->max_sale = apoint_ts{
				point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
				{ 10.0, 9999.0 } };
			market->load = apoint_ts{
				point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
				{ 90.0, 80.0, 60.0, 10.0, 0.0 } };
			stm->market.push_back(market);
			stm->hps.push_back(hps);

			shop_system api{ t_period, t_step };
			api.set_silent_mode(silent_mode);
			api.install_logger(std::make_shared<test_stm_shop_log_handler>());
			api.emitter.to_shop(stm);
			if (print_topology_graph) api.export_topology(std::cout);
			REQUIRE(api.emitter.objects.size<shop_reservoir>() == 1);
			CHECK(api.emitter.objects.cbegin<shop_reservoir>()->first.id == rsv->id);
			REQUIRE(api.emitter.objects.size<shop_creek_intake>() == 2);
			CHECK(api.emitter.objects.cbegin<shop_creek_intake>()->first.id == crk1->id);
			CHECK(std::next(api.emitter.objects.cbegin<shop_creek_intake>())->first.id == crk2->id);
			CHECK(api.emitter.objects.size<shop_junction>() == 0);
			CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
			if (print_topology_graph) api.export_topology(std::cout);
		}
	}
}

TEST_CASE("Junction and creek topology" * doctest::skip(false))
{
	// NOTE: Several of the topologies tested here may not be currently supported by Shop,
	//       and it will report error upon connecting objects such as this:
	//          DIAGNOSIS ERROR : 3408
	//          Attempted to connect object of type Junction, name %s.
	//          Object not found.
	//       Regardless, the topologies will push the limits of the Shop emitter traversal logic
	//       and could be usefull to avoid any unexpected issues in future.
	if (!silent_mode) { MESSAGE("Several of the topologies tested here may not be currently supported by Shop, and it will report a diagnosis error that can be discarded."); }
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;

	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const shyft::core::utcperiod t_period{ t_begin, t_end };
	const auto t_step = shyft::core::deltahours(1);

	SUBCASE("topology 01")
	{
		// Creek into junction.
		//
		// STM:
		//    R1 C R2
		//     \ | /
		//       | <-- main tunnel (junction in stm but not in shop)
		//       | <-- penstock
		//       A
		//
		// Shop-emitter (shop_id in parens):
		//  R1 (3) R2 (4)
		//     \   /
		//       J (7)
		//       |
		//       C (5)
		//       |
		//       A
		//
		// Shop API/kernel:
		//  R1 (3) R2 (4)   C
		//     \   /        |
		//       J (7)      J  <-- junction object created automatically when adding the creek
		//
		//  Not supported? Shop API seems to accept it, connecting the creek and junction
		//  returns success response, but they are not being connected, and there is an
		//  error message in the log:
		//     DIAGNOSIS ERROR : 3408
		//     Attempted to connect object of type Junction, name waterroute main tunnel.
		//     Object not found.
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		reservoir_ crk = builder.create_reservoir(id++, "creek"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_rsv1 = builder.create_tunnel(id++, "waterroute rsv1 output"s, ""s);
		waterway_ wtr_rsv2 = builder.create_tunnel(id++, "waterroute rsv2 output"s, ""s);
		waterway_ wtr_crk = builder.create_tunnel(id++, "waterroute crk output"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_rsv1).input_from(rsv1).output_to(wtr_main);
		connect(wtr_crk).input_from(crk).output_to(wtr_main);
		connect(wtr_rsv2).input_from(rsv2).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock);
		connect(wtr_penstock).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		// Traverse tunnel topology (must manually fill shop_reservoirs and shop_creek_intakes since we are bypassing the parent function)
		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(rsv1, std::move(api.adapter.to_shop(rsv1)));
		api.emitter.objects.add(rsv2, std::move(api.adapter.to_shop(rsv2)));
		api.emitter.objects.add(crk, std::move(api.adapter.to_shop_creek(crk)));
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(input_sink_id.t_id == shop_creek_intake::t_id);
		//CHECK(api.shop_reservoirs.size() == 1);
		//CHECK(api.shop_creek_intakes.size() == 1);
		CHECK(api.emitter.objects.size<shop_junction>() == 1);
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_creek_intake>(crk).id);
	}

	SUBCASE("topology 02")
	{
		// One branch with creek meet another branch in junction.
		//
		// STM:
		//  C1 R1
		//   \ |  R2
		//     \ /
		//      | <-- main tunnel (junction in stm but not in shop)
		//      | <-- penstock
		//      A
		//
		// Shop-emitter NEW (shop_id in parens):
		// (The way the Shop emitter does with new source/sink return values as of 25.01.2019,
		// recognizing that a branch with a reservoir source and creek inserts in shop is
		// connected as if reservoir was upstream the creek, so the creek represents an actual
		// reservoir-tunnel branch that can be part of a junction - if this is supported in Shop?)
		//     R1 (3)
		//     |
		//     C1 (5)  R2 (4)
		//      \     /
		//        J (7)
		//        |
		//        A
		//
		// Shop-emitter OLD (shop_id in parens):
		// (Alternative Shop emitter as of 24.01.2019, reducing reservoir branch
		// with creek intake into a creek, leading to two connections into same creek,
		// probably not something that is correct?)
		//     R1 (3)  R2 (4)
		//      \    /
		//       C1 (5)
		//       |
		//       A
		//
		// Shop API/kernel:
		//    C1 R1
		//      \|
		//    R2 J <-- junction object created automatically when adding the creek
		//      \|
		//       J <-- main tunnel junction
		//       |
		//       A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		reservoir_ crk1 = builder.create_reservoir(id++, "creek 1"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_rsv1 = builder.create_tunnel(id++, "waterroute rsv1 output"s, ""s);
		waterway_ wtr_crk1 = builder.create_tunnel(id++, "waterroute crk1 output"s, ""s);
		waterway_ wtr_rsv1_crk1 = builder.create_tunnel(id++, "waterroute rsv1 crk1"s, ""s);
		waterway_ wtr_rsv2 = builder.create_tunnel(id++, "waterroute rsv2 output"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_rsv1).input_from(rsv1).output_to(wtr_rsv1_crk1);
		connect(wtr_crk1).input_from(crk1).output_to(wtr_rsv1_crk1);
		connect(wtr_rsv1_crk1).output_to(wtr_main);
		connect(wtr_rsv2).input_from(rsv2).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock);
		connect(wtr_penstock).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		// Traverse tunnel topology (must manually fill shop_reservoirs and shop_creek_intakes since we are bypassing the parent function)
		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(rsv1, std::move(api.adapter.to_shop(rsv1)));
		api.emitter.objects.add(rsv2, std::move(api.adapter.to_shop(rsv2)));
		api.emitter.objects.add(crk1, std::move(api.adapter.to_shop_creek(crk1)));
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(input_sink_id.t_id == shop_junction::t_id);
		CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
		CHECK(api.emitter.objects.size<shop_creek_intake>() == 1);
		CHECK(api.emitter.objects.size<shop_junction>() == 1);
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_junction>(wtr_main).id);
	}

	SUBCASE("topology 03")
	{
		// Two branches with creeks on them meet in junction
		//
		// STM:
		//  C1 R1 R2 C2
		//   \ |  | /
		//     \ /
		//      | <-- main tunnel (junction in stm but not in shop)
		//      | <-- penstock
		//      A
		//
		// Shop-emitter (shop_id in parens):
		//     R1 (3)  R2 (4)
		//     |       |
		//     C1 (5)  C2 (7)
		//       \   /
		//         J (9)
		//         |
		//         A
		//
		// Shop API/kernel:
		//    C1 R1 R2 C2
		//      \|  |/
		//       J  J <-- junction objects created automatically when adding the creek
		//        \/
		//         J <-- main tunnel junction
		//         |
		//         A
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		reservoir_ crk1 = builder.create_reservoir(id++, "creek 1"s, ""s);
		reservoir_ crk2 = builder.create_reservoir(id++, "creek 2"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);
		waterway_ wtr_rsv1 = builder.create_tunnel(id++, "waterroute rsv1 output"s, ""s);
		waterway_ wtr_crk1 = builder.create_tunnel(id++, "waterroute crk1 output"s, ""s);
		waterway_ wtr_rsv1_crk1 = builder.create_tunnel(id++, "waterroute rsv1 crk1"s, ""s);
		waterway_ wtr_rsv2 = builder.create_tunnel(id++, "waterroute rsv2 output"s, ""s);
		waterway_ wtr_crk2 = builder.create_tunnel(id++, "waterroute crk2 output"s, ""s);
		waterway_ wtr_rsv2_crk2 = builder.create_tunnel(id++, "waterroute rsv2 crk2"s, ""s);
		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main tunnel"s, ""s);
		waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock 1"s, ""s);
		connect(wtr_rsv1).input_from(rsv1).output_to(wtr_rsv1_crk1);
		connect(wtr_crk1).input_from(crk1).output_to(wtr_rsv1_crk1);
		connect(wtr_rsv2).input_from(rsv2).output_to(wtr_rsv2_crk2);
		connect(wtr_crk2).input_from(crk2).output_to(wtr_rsv2_crk2);
		connect(wtr_rsv1_crk1).output_to(wtr_main);
		connect(wtr_rsv2_crk2).output_to(wtr_main);
		connect(wtr_main).output_to(wtr_penstock);
		connect(wtr_penstock).output_to(g1);

		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00030);

		// Traverse tunnel topology (must manually fill shop_reservoirs and shop_creek_intakes since we are bypassing the parent function)
		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(rsv1, std::move(api.adapter.to_shop(rsv1)));
		api.emitter.objects.add(rsv2, std::move(api.adapter.to_shop(rsv2)));
		api.emitter.objects.add(crk1, std::move(api.adapter.to_shop_creek(crk1)));
		api.emitter.objects.add(crk2, std::move(api.adapter.to_shop_creek(crk2)));
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(input_sink_id.t_id == shop_junction::t_id);
		CHECK(api.emitter.objects.size<shop_reservoir>() == 2);
		CHECK(api.emitter.objects.size<shop_creek_intake>() == 2);
		CHECK(api.emitter.objects.size<shop_junction>() == 1);
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(input_sink_id.id == api.emitter.objects.id_of<shop_junction>(wtr_main).id);
	}

	SUBCASE("topology 04")
	{
		// Complex tree of junctions and creeks.
		//
		// STM:
		//                  R4
		//                  /
		//     R1 R2 R3 C3 /--C4
		//      \ /   \ |/
		//    C1-\     /---C2  <-- junctions
		//        \   /
		//          | <-- main/junction
		//          | <-- penstock
		//          A
		//
		// Shop-emitter (shop_id in parens):
		// (The way the Shop emitter does with new source/sink return values as of 25.01.2019,
		// recognizing that a branch with a reservoir source and creek inserts in shop is
		// connected as if reservoir was upstream the creek, so the creek represents an actual
		// reservoir-tunnel branch that can be part of a junction - if this is supported in Shop?)
		//                           R4 (6)
		//                           /
		//                   R3 (5) C4 (13)
		//                     \   /
		//     R1 (3) R2 (4)     J (16)
		//       \   /           |
		//         J (15)        C3 (11)
		//         |             |
		//         C1 (7)        C2 (9)
		//            \       /
		//               J (17)
		//               |
		//               A
		//
		// Shop API/kernel:
		//  Not supported? Shop API seems to accept it, connecting the creek and junction
		//  returns success response, but they are not being connected, and there is an
		//  error message in the log:
		//     DIAGNOSIS ERROR : 3408
		//     Attempted to connect object of type Junction, name waterroute main tunnel.
		//     Object not found.
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ r1 = builder.create_reservoir(id++, "reservoir 1"s, ""s);
		reservoir_ r2 = builder.create_reservoir(id++, "reservoir 2"s, ""s);
		reservoir_ r3 = builder.create_reservoir(id++, "reservoir 3"s, ""s);
		reservoir_ r4 = builder.create_reservoir(id++, "reservoir 4"s, ""s);
		reservoir_ c1 = builder.create_reservoir(id++, "creek 1"s, ""s);
		reservoir_ c2 = builder.create_reservoir(id++, "creek 2"s, ""s);
		reservoir_ c3 = builder.create_reservoir(id++, "creek 3"s, ""s);
		reservoir_ c4 = builder.create_reservoir(id++, "creek 4"s, ""s);
		unit_ g1 = builder.create_unit(id++, "aggregate"s, ""s);
		power_plant_ ps = builder.create_power_station(id++, "plant"s, ""s);
		power_plant::add_unit(ps, g1);

		waterway_ wtr_r1_out = builder.create_tunnel(id++, "waterroute reservoir 1 output tunnel"s, ""s);
		waterway_ wtr_r2_out = builder.create_tunnel(id++, "waterroute reservoir 2 output tunnel"s, ""s);
		waterway_ wtr_r1_r2_c1 = builder.create_tunnel(id++, "waterroute reservoir 1 and 2 junction with creek 1"s, ""s);
		connect(wtr_r1_out).input_from(r1).output_to(wtr_r1_r2_c1);
		connect(wtr_r2_out).input_from(r2).output_to(wtr_r1_r2_c1);
		waterway_ wtr_c1_out = builder.create_tunnel(id++, "waterroute creek 1 output tunnel"s, ""s);
		connect(wtr_c1_out).input_from(c1).output_to(wtr_r1_r2_c1);
		waterway_ wtr_r1_r2_c1_out = builder.create_tunnel(id++, "waterroute out from reservoir 1 and 2 junction"s, ""s);
		connect(wtr_r1_r2_c1_out).input_from(wtr_r1_r2_c1);

		waterway_ wtr_r4_out = builder.create_tunnel(id++, "waterroute reservoir 4 output tunnel"s, ""s);
		waterway_ wtr_r4_out_2 = builder.create_tunnel(id++, "waterroute reservoir 4 output tunnel segment 2"s, ""s);
		waterway_ wtr_c4_out = builder.create_tunnel(id++, "waterroute creek 4 output tunnel"s, ""s);
		waterway_ wtr_r4_c4 = builder.create_tunnel(id++, "waterroute reservoir 4 and creek 4"s, ""s);
		connect(wtr_r4_out).input_from(r4).output_to(wtr_r4_out_2);
		connect(wtr_r4_out_2).output_to(wtr_r4_c4);
		connect(wtr_c4_out).input_from(c4).output_to(wtr_r4_c4);
		
		waterway_ wtr_r4_c4_2 = builder.create_tunnel(id++, "waterroute reservoir 4 and creek 4 segment 2"s, ""s);

		waterway_ wtr_r3_out = builder.create_tunnel(id++, "waterroute reservoir 3 output tunnel"s, ""s);
		waterway_ wtr_c3_out = builder.create_tunnel(id++, "waterroute creek 3 output tunnel"s, ""s);
		waterway_ wtr_r3_r4_c2_c3_c4 = builder.create_tunnel(id++, "waterroute reservoir 3 and 4 with creek 2, 3 and 4"s, ""s);

		connect(wtr_r3_out).input_from(r3).output_to(wtr_r3_r4_c2_c3_c4);
		connect(wtr_c3_out).input_from(c3).output_to(wtr_r3_r4_c2_c3_c4);
		connect(wtr_r4_c4_2).input_from(wtr_r4_c4).output_to(wtr_r3_r4_c2_c3_c4);

		waterway_ wtr_c2_out = builder.create_tunnel(id++, "waterroute creek 2 output tunnel"s, ""s);
		connect(wtr_c2_out).input_from(c2).output_to(wtr_r3_r4_c2_c3_c4);

		waterway_ wtr_r3_r4_c2_c3_c4_2 = builder.create_tunnel(id++, "waterroute reservoir 3 and 4 with creek 2, 3 and 4 segment 2"s, ""s);
		connect(wtr_r3_r4_c2_c3_c4).output_to(wtr_r3_r4_c2_c3_c4_2);


		waterway_ wtr_main = builder.create_tunnel(id++, "waterroute main junction"s, ""s);
		waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
		connect(wtr_main).input_from(wtr_r1_r2_c1_out);
		connect(wtr_main).input_from(wtr_r3_r4_c2_c3_c4_2);
		connect(wtr_main).output_to(wtr_penstock);
		connect(wtr_penstock).output_to(g1);

		wtr_r1_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r1_out->head_loss_coeff.get()->emplace(t_begin, 0.00011);
		wtr_r2_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r2_out->head_loss_coeff.get()->emplace(t_begin, 0.00012);
		wtr_r3_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r3_out->head_loss_coeff.get()->emplace(t_begin, 0.00013);
		wtr_r4_out->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r4_out->head_loss_coeff.get()->emplace(t_begin, 0.00014);
		wtr_r1_r2_c1->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r1_r2_c1->head_loss_coeff.get()->emplace(t_begin, 0.00015);
		wtr_r3_r4_c2_c3_c4_2->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_r3_r4_c2_c3_c4_2->head_loss_coeff.get()->emplace(t_begin, 0.00016);
		wtr_main->head_loss_coeff = make_shared<map<utctime, double>>();
		wtr_main->head_loss_coeff.get()->emplace(t_begin, 0.00017);

		// Find junctions
		shop_system api{ t_period, t_step };
		api.set_silent_mode(silent_mode);
		api.emitter.objects.add(r1, std::move(api.adapter.to_shop(r1)));
		api.emitter.objects.add(r2, std::move(api.adapter.to_shop(r2)));
		api.emitter.objects.add(r3, std::move(api.adapter.to_shop(r3)));
		api.emitter.objects.add(r4, std::move(api.adapter.to_shop(r4)));
		api.emitter.objects.add(c1, std::move(api.adapter.to_shop_creek(c1)));
		api.emitter.objects.add(c2, std::move(api.adapter.to_shop_creek(c2)));
		api.emitter.objects.add(c3, std::move(api.adapter.to_shop_creek(c3)));
		api.emitter.objects.add(c4, std::move(api.adapter.to_shop_creek(c4)));
		auto[input_sink_id, input_source_id] = api.emitter.handle_plant_input(wtr_main);
		if (print_topology_graph) api.export_topology(std::cout);
		CHECK(input_sink_id.t_id == shop_junction::t_id);
		// Should now have 3 junctions, represented by wtr_r1_r2_c1, wtr_r3_r4_c2_c3_c4 and wtr_main
		CHECK(api.emitter.objects.size<shop_junction_gate>() == 0);
		CHECK(api.emitter.objects.size<shop_junction>() == 3);
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_r1_r2_c1));
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_r3_r4_c2_c3_c4));
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_main));
		CHECK(api.emitter.objects.id_of<shop_junction>(wtr_main).id == input_sink_id.id); // Return value should be the shop id of the lowest input object
	}
}

TEST_CASE("shop command")
{
	// NB: Using 6 decimal places for any floating point values in strings, because currently the
	// shop_command is using std::to_string to convert the double value into string and it seems
	// to always use 6 decimals without option to specify anything else.

	using shyft::energy_market::stm::shop::shop_command;

	SUBCASE("keyword only")
	{
		const char* const command_string = "quit";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "quit");
		CHECK(cmd.specifier.empty());
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
	}
	SUBCASE("keyword and specifier")
	{
		const char* const command_string = "save tunnelloss";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "save");
		CHECK(cmd.specifier == "tunnelloss");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::save_tunnelloss();
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and one option")
	{
		const char* const command_string = "set method /dual";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "method");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "dual");
		}
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_method_dual();
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and string value")
	{
		const char* const command_string = "log file filename.ext";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "log");
		CHECK(cmd.specifier == "file");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "filename.ext");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::log_file("filename.ext");
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and int value")
	{
		const char* const command_string = "set max_num_threads 8";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "max_num_threads");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "8");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_max_num_threads(8);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and double value")
	{
		const char* const command_string = "set fcr_n_band 0.400000";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "fcr_n_band");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "0.400000");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_fcr_n_band(0.4);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier option and string value")
	{
		const char* const command_string = "return simres /gen filename.ext";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "return");
		CHECK(cmd.specifier == "simres");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "gen");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "filename.ext");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::return_simres_gen("filename.ext");
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier option and double value")
	{
		const char* const command_string = "set mipgap /relative 0.300000";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "mipgap");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "relative");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "0.300000");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_mipgap(false, 0.3);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and three options")
	{
		const char* const command_string = "penalty flag /on /reservoir /ramping";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "penalty");
		CHECK(cmd.specifier == "flag");
		CHECK(cmd.options.size() == 3);
		if (cmd.options.size() == 3) {
			CHECK(cmd.options[0] == "on");
			CHECK(cmd.options[1] == "reservoir");
			CHECK(cmd.options[2] == "ramping");
		}
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::penalty_flag_reservoir_ramping(true);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier two options and double value")
	{
		const char* const input_command_string = "\t penalty   cost   /reservoir \t /ramping    123.500000   ";
		const char* const output_command_string = "penalty cost /reservoir /ramping 123.500000";
		shop_command cmd{ input_command_string };
		CHECK(cmd.keyword == "penalty");
		CHECK(cmd.specifier == "cost");
		CHECK(cmd.options.size() == 2);
		if (cmd.options.size() == 2) {
			CHECK(cmd.options[0] == "reservoir");
			CHECK(cmd.options[1] == "ramping");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects[0] == "123.500000");
		}
		CHECK((std::string)cmd == output_command_string);
		shop_command cmd2 = shop_command::penalty_cost_reservoir_ramping(123.50);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
#ifdef SHOP_COMMAND_MULTIPLE_OBJECTS
	SUBCASE("multiple objects")
	{
		const char* const command_string = "the quick brown fox jumps over the lazy dog";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "the");
		CHECK(cmd.specifier == "quick");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 7);
		if (cmd.objects.size() == 7) {
			CHECK(cmd.objects[0] == "brown");
			CHECK(cmd.objects[1] == "fox");
			CHECK(cmd.objects[2] == "jumps");
			CHECK(cmd.objects[3] == "over");
			CHECK(cmd.objects[4] == "the");
			CHECK(cmd.objects[5] == "lazy");
			CHECK(cmd.objects[6] == "dog");
		}
		CHECK((std::string)cmd == command_string);
	}
#endif
	SUBCASE("invalid syntax")
	{
#ifndef SHOP_COMMAND_MULTIPLE_OBJECTS
		CHECK_THROWS(shop_command cmd{ "The quick brown fox" });
		CHECK_THROWS(shop_command cmd{ "The quick /brown fox jumps" });
		CHECK_THROWS(shop_command cmd{ "The quick brown fox jumps over the lazy dog" });
#endif
		CHECK_THROWS(shop_command cmd{ "The /quick" });
		CHECK_THROWS(shop_command cmd{ "The /quick /brown" });
		CHECK_THROWS(shop_command cmd{ "The /quick /brown /fox" });
		CHECK_THROWS(shop_command cmd{ "The quick /brown fox /jumps" });
		CHECK_THROWS(shop_command cmd{ "The quick brown /fox" });
		CHECK_THROWS(shop_command cmd{ "The quick brown /fox /jumps" });
	}
}

}
#endif