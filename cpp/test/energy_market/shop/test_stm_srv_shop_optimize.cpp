#include <doctest/doctest.h>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include "model_simple.h"
#include <shyft/energy_market/stm/srv/server.h>
#include <shyft/energy_market/stm/srv/server_logger.h>
#include <shyft/energy_market/stm/srv/client.h>
#include <shyft/core/fs_compat.h>
#include <test/test_utils.h>
#include <shyft/energy_market/stm/srv/context.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <thread>
#include <csignal>
using test::utils::temp_dir;

namespace test {
    using std::string;
    using std::string_view;
    using std::vector;
    using std::make_shared;
    using shyft::energy_market::stm::srv::model_state;


    struct test_server: shyft::energy_market::stm::srv::server {
        shyft::web_api::energy_market::request_handler bg_server;
        std::future<int> web_srv;///< mutex,

        //-- to verify fx-callback
        string fx_mid;
        string fx_arg;
        bool fx_handler(string mid,string json_arg) {
            fx_mid=mid;
            fx_arg=json_arg;
            return true;
        }

        explicit test_server() : server() {
            bg_server.srv = this;
            this->fx_cb=[=](string m,string a)->bool {return this->fx_handler(m,a);};
        }

        explicit test_server(const string& root_dir) : server() {
            bg_server.srv = this;
            dtss->add_container("test", root_dir);
        }

        void start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads) {
            if(!web_srv.valid()) {
                web_srv= std::async(std::launch::async,
                    [this,host_ip,port,doc_root,fg_threads,bg_threads]()->int {
                        return shyft::web_api::run_web_server(
                        bg_server,
                        host_ip,
                        static_cast<unsigned short>(port),
                        make_shared<string>(doc_root),
                        fg_threads,
                        bg_threads);

                    }
                );
            }
        }
        bool web_api_running() const {return web_srv.valid();}
        void stop_web_api() {
            if(web_srv.valid()) {
                std::raise(SIGTERM);
                (void) web_srv.get();
            }
        }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;

    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }

    /** engine that perform a publish-subscribe against a specified host
     *
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class run_params_session : public std::enable_shared_from_this<run_params_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        test_server* const srv; ///< Hold the server so we can use its dtss.
        int num_waits=0; ///<How many expected releases of subscribed read pattern
        // report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec, diag) if((ec)) return fail((ec), (diag));
    public:
        // Resolver and socket require an io_context
        explicit run_params_session(boost::asio::io_context& ioc, test_server* const srv): resolver_(ioc), ws_(ioc), srv{srv} {}
        vector<string> responses_;
        string diagnostics() const { return fail_; }

        // Start the asynchronous operation
        void run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_, port_, // Look up the domain name
                                    [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                                        me->on_resolve(ec, results);
                                    }
                      );
        }

        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve");
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                                       std::bind(&run_params_session::on_connect, shared_from_this(), std::placeholders::_1)
                         );
        }

        void on_connect(error_code ec){
            fail_on_error(ec, "connect");
            ws_.async_handshake(host_, "/", // Perform websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->send_initial(ec);
                }
            );
        }

        void send_initial(error_code ec) {
            fail_on_error(ec, "send_initial");
            ws_.async_write(
                boost::asio::buffer(R"_(run_params {"request_id": "initial", "model_id": "simple", "subscribe": true})_"),
                [me=shared_from_this()](error_code ec, size_t bytes_transferred){
                    me->start_read(ec, bytes_transferred);
                }
            );
        }

        void start_read(error_code ec, size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "start_read");
            ws_.async_read(buffer_,
                        [me=shared_from_this()](error_code ec2, size_t bytes_transferred2){
                            me->on_read(ec2, bytes_transferred2);
                        }
                    );
        }

        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read");
            string response = boost::beast::buffers_to_string(buffer_.data());
            std::cout << "Got response: " << response << "\n";
            responses_.push_back(response);
            //std::cout << "Got response: " << response << "\n";
            buffer_.consume(buffer_.size());
            if(response.find("finale") != string::npos) {
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) { me->on_close(ec); }
                    );
            } else {
                if (response.find("initial")!=string::npos) {
                    if (num_waits == 0) { // First time we update a model. Here via a simple notify change
                        ++num_waits;
                        auto mdl = srv->do_get_model("simple");
                        auto & pa = mdl->run_params->n_inc_runs;
                        pa = 33;
                        srv->sm->notify_change(pa.url("dstm://Msimple/"));
                    } else if (num_waits == 1) { // Update model via optimization
                        ++num_waits;
                        string mdl_id("simple");
                        string host_port("localhost:" + std::to_string(srv->get_listening_port()));
                        shyft::energy_market::stm::srv::client c(host_port);
                        auto cmd = optimization_commands(1, false);
                        const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
                        const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
                        const auto t_step = shyft::core::deltahours(1);
                        const size_t n_steps = (t_end - t_begin) / t_step;
                        const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };
                        CHECK_EQ(true, c.optimize(mdl_id, ta, cmd));
                        auto t_exit=shyft::core::utctime_now() + std::chrono::seconds(30);// reasonable limit
                        while(c.get_state(mdl_id)!= model_state::idle && shyft::core::utctime_now()<t_exit)
                            std::this_thread::sleep_for(std::chrono::milliseconds(10));
                        REQUIRE(c.get_state(mdl_id) == model_state::idle);
                        auto stm = c.get_model(mdl_id);
                        auto rstm = c.get_model("simple_results");
                        check_results(stm, rstm, t_begin, t_end, t_step);
                    } else {
                        ws_.async_write(
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"initial"})_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    }
                }

                //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
                ws_.async_read(buffer_,
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_read(ec, bytes_transferred);
                    }
                );
            }
        }

        void on_close(error_code ec) {
            fail_on_error(ec, "close");
        }

    #undef fail_on_error
    };
}


TEST_SUITE("stm_srv_shop_optimize") {

	static const bool SKIP_OPTIMIZE = false;
	static const bool write_files = false; // Should the test write log and results to file for manual inspection?
	static const bool silent_mode = true;

	static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_files==true.

	using namespace shyft::energy_market::stm::srv;
	using namespace test;
	using std::exception;
    using shyft::core::utctime_now;

TEST_CASE("optimize simple model without inlet"
	* doctest::description("building and optmimizing simple model without inlet segment: aggregate-penstock-maintunnel-reservoir")
	* doctest::skip(SKIP_OPTIMIZE))
{   dlib::set_all_logging_levels(dlib::LALL);
	const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
	auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:
    temp_dir log_dir{"shyft.shop.tst"};
	server s(log_dir);
	s.set_listening_ip("127.0.0.1");
	auto port_no = s.start_server();
	REQUIRE_GT(port_no, 0);// require vs. test.abort this part of test if we fail here
	try {
		auto  host_port = string("localhost:") + to_string(port_no);
		client c(host_port);

		// get version info
		auto result = c.version_info();
		CHECK_EQ(result, s.do_get_version_info());

		// get model ids
		auto mids = c.get_model_ids();
		CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

		const char mdl_id[] = { "test_stm_model" };
		CHECK_EQ(c.add_model(mdl_id, stm), true);
		mids = c.get_model_ids();
		REQUIRE_EQ(mids.size(), 1);
		auto cmd = optimization_commands(run_id, write_files);
        CHECK_EQ(c.optimize(mdl_id, ta, cmd), true); // Starting optimization
        auto t_exit=utctime_now() + std::chrono::seconds(30);// reasonable limit
        while(c.get_state(mdl_id)!= model_state::idle && utctime_now()<t_exit)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        REQUIRE(c.get_state(mdl_id)== model_state::idle);
        auto stm2 = c.get_model(mdl_id);
        check_results(stm2, rstm, t_begin, t_end, t_step);
        
		auto attr = stm2->market.front()->buy;
		REQUIRE_EQ(attr.exists(), true);
		auto ts = attr.get();
		auto values = ts.values();
		REQUIRE_EQ(values[0], 10.0);

		attr = stm2->market.front()->sale;
		REQUIRE_EQ(attr.exists(), true);
		ts = attr.get();
		values = ts.values();
		REQUIRE_EQ(values[5], -10.0);
        // Check that we can't start optimizing on an 'already' optimizing model:
        //TODO: have to figure out better ways to do this check
        //s.do_set_state(mdl_id, model_state::optimizing);
        //CHECK_EQ(c.optimize(mdl_id, ta, cmd), false); // Should be seen as already running...
        

        c.close();
        s.clear();
	} catch (exception const& ex) {
		DOCTEST_MESSAGE(ex.what());
		CHECK_EQ(true, false);
        s.clear();
	}
}

TEST_CASE("get_shop_logger"
    * doctest::description("getting log from model, before and after running shop")
	* doctest::skip(SKIP_OPTIMIZE)){
    
    server_log_hook hook;
    configure_logger(hook, dlib::LALL);

    const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
	auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);

    temp_dir tmpdir{"stm_srv_shop_optimize.test."};
    server s(tmpdir);
	s.set_listening_ip("127.0.0.1");
	auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0);// require vs. test.abort this part of test if we fail here
    try {
		auto  host_port = string("localhost:") + to_string(port_no);
		client c(host_port);

		// get version info
		auto result = c.version_info();
		CHECK_EQ(result, s.do_get_version_info());

		// get model ids
		auto mids = c.get_model_ids();
		CHECK_EQ(mids.size(), 0); // it should be 0 models to start with

		const string mdl_id = "test_stm_model";
		CHECK_EQ(c.add_model(mdl_id, stm), true);
		mids = c.get_model_ids();
		REQUIRE_EQ(mids.size(), 1);

        
        // Check log before anything else:
        auto log = c.get_log(mdl_id);
        CHECK_EQ(log, "Unable to find log.");
        
		auto cmd = optimization_commands(run_id, write_files);
        CHECK_EQ(c.optimize(mdl_id, ta, cmd), true); // Starting optimization
        MESSAGE("waiting for optimize to conclude");
        auto t_exit=utctime_now() + std::chrono::seconds(30);// reasonable limit
        while(c.get_state(mdl_id)!= model_state::idle && utctime_now()<t_exit)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        MESSAGE("getting the model:"<<mdl_id);

        auto stm2 = c.get_model(mdl_id);
        check_results(stm2, rstm, t_begin, t_end, t_step);
        MESSAGE("getting the log:"<<mdl_id);
        log = c.get_log(mdl_id);
        
        CHECK_GT(log.length(), 200); // We expect something a little bit long.
        std::ifstream ifs( ( tmpdir / (string(mdl_id) + ".log")).c_str() );// to help ms c++
        std::stringstream ss;
        ss << ifs.rdbuf();
        CHECK_EQ(log, ss.str());
        c.close();
        s.clear();
	}
	catch (exception const& ex) {
		DOCTEST_MESSAGE(ex.what());
		CHECK_EQ(true, false);
		s.clear();
	}
}

TEST_CASE("optimize_unsafe_model"
    * doctest::description("Trying to run optimization on a model with bad input.")
    * doctest::skip(SKIP_OPTIMIZE)) {
    
    server_log_hook hook;
    configure_logger(hook, dlib::LALL);
    
    const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

    temp_dir tmpdir{"stm_srv_shop_optimize.test."};
    server s(tmpdir);
	auto port_no = s.start_server();
    REQUIRE_GT(port_no, 0);
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
		client c(host_port);

		// get version info
		auto result = c.version_info();
		CHECK_EQ(result, s.do_get_version_info());
        
        const string mdl_id = "test_stm_model";
        // Let's "destroy" the input model with a broken time series:
        auto rsv = std::dynamic_pointer_cast<reservoir>(stm->hps[0]->reservoirs[0]);
        rsv->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
        //rsv->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        //    std::vector<double>{ shyft::nan, 2.0, 3.0, 5.0, 4.0 },
        //    std::vector<double>{ shyft::nan, 90.0, 95.0, 100.0, 97.0 }));
        rsv->volume_descr.get()->emplace(t_begin, nullptr);
        rsv->lrl = make_constant_ts(t_begin, t_end, -1.0);
		CHECK_EQ(c.add_model(mdl_id, stm), true);
		auto mids = c.get_model_ids();
		REQUIRE_EQ(mids.size(), 1);
        
        // Start optimization on model that shouldn't work
        auto cmd = optimization_commands(run_id, write_files);
        CHECK_EQ(c.optimize(mdl_id, ta, cmd), false); // Starting optimization
        auto t_exit=utctime_now() + std::chrono::seconds(30);// reasonable limit
        while(c.get_state(mdl_id)!= model_state::idle && utctime_now()<t_exit)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

        // At this point we should have received a SIGSEGV signal
        CHECK_EQ(true, shop_segfault_handler::sigsegv_received);
        c.close();
        s.clear();
    } catch (exception const& ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true, false);
        s.clear();
    }
}

TEST_CASE("optimize_with_unbound_attributes"
    * doctest::description("Testing that dtss handles unbound time series properly before sending to SHOP")
    * doctest::skip(SKIP_OPTIMIZE)) {

    server_log_hook hook;
    configure_logger(hook, dlib::LALL);
    
    const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };
    
    temp_dir tmpdir{"stm_srv_shop_optimize.test."};
    server s(tmpdir);
    //dir_cleanup wipe{tmpdir};
	auto port_no = s.start_server();
    s.add_container("test", (tmpdir / "ts").string());
    
    auto stm = build_simple_model_with_dtss(*(s.dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    REQUIRE_GT(port_no, 0);
    try {
        auto host_port = string("localhost:") + to_string(port_no);
        client c(host_port);
        c.add_model("dtss_optimize_unbound", stm);
        c.add_model("dtss_optimize_bound", stm2);
        stm = c.get_model("dtss_optimize_unbound");
        // CHECK that stuff is unbound:
        auto hps = stm->hps[0];
        auto market = stm->market[0];
        CHECK_EQ(market->price.get().needs_bind(), true);
        CHECK_EQ(market->max_buy.get().needs_bind(), true);
        CHECK_EQ(market->max_sale.get().needs_bind(), true);
        CHECK_EQ(market->load.get().needs_bind(), true);
        
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_name("reservoir"));
        CHECK_EQ(rsv->lrl.get().needs_bind(), true);
        CHECK_EQ(rsv->hrl.get().needs_bind(), true);
        CHECK_EQ(rsv->volume_max_static.get().needs_bind(), true);
        CHECK_EQ(rsv->endpoint_desc.get().needs_bind(), true);
        CHECK_EQ(rsv->level_historic.get().needs_bind(), true);
        CHECK_EQ(rsv->inflow.get().needs_bind(), true);
        
        auto wtr_flood = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute flood river"));
        CHECK_EQ(wtr_flood->discharge_max_static.get().needs_bind(), true);
        
        auto wtr_tunnel = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
        CHECK_EQ(wtr_tunnel->head_loss_coeff.get().needs_bind(), true);
        
        auto wtr_penstock = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute penstock"));
        CHECK_EQ(wtr_penstock->head_loss_coeff.get().needs_bind(), true);
        
        auto ps = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_name("plant"));
        CHECK_EQ(ps->outlet_level.get().needs_bind(), true);
        
        auto gu = std::dynamic_pointer_cast<unit>(hps->find_unit_by_name("aggregate"));
        CHECK_EQ(gu->production_min_static.get().needs_bind(), true);
        CHECK_EQ(gu->production_max_static.get().needs_bind(), true);
        
        // CHECK that we cannot do optimization on unbound model:
        auto cmd = optimization_commands(run_id, write_files);
        CHECK_EQ(false, c.optimize("dtss_optimize_unbound", ta, cmd));
        auto t_exit=utctime_now() + std::chrono::seconds(30);// reasonable limit
        while(c.get_state("dtss_optimize_unbound")!= model_state::idle && utctime_now()<t_exit)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

        // Evaluate all unbound time series in the model:
        CHECK_EQ(true, c.evaluate_model("dtss_optimize_unbound", ta.total_period(), true, false));
        stm = c.get_model("dtss_optimize_unbound");
        market = stm->market[0];
        hps = stm->hps[0];
        rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_name("reservoir"));
        wtr_flood = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute flood river"));
        wtr_tunnel = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute input tunnel"));
        wtr_penstock = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_name("waterroute penstock"));
        ps = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_name("plant"));
        gu = std::dynamic_pointer_cast<unit>(hps->find_unit_by_name("aggregate"));

        auto m2 = stm2->market[0];
        auto hps2 = stm2->hps[0];
        CHECK_EQ(market->price.get(), m2->price.get());
        CHECK_EQ(market->max_buy.get(), m2->max_buy.get());
        CHECK_EQ(market->max_sale.get(), m2->max_sale.get());
        CHECK_EQ(market->load.get(), m2->load.get());
        
        auto rsv2 = std::dynamic_pointer_cast<reservoir>(hps2->find_reservoir_by_name("reservoir"));
        CHECK_EQ(rsv->lrl.get(), rsv2->lrl.get());
        CHECK_EQ(rsv->hrl.get(), rsv2->hrl.get());
        CHECK_EQ(rsv->volume_max_static.get(), rsv2->volume_max_static.get());
        CHECK_EQ(rsv->endpoint_desc.get(), rsv2->endpoint_desc.get());
        CHECK_EQ(rsv->level_historic.get(), rsv2->level_historic.get());
        CHECK_EQ(rsv->inflow.get(), rsv2->inflow.get());
        
        auto wtr_flood2 = std::dynamic_pointer_cast<waterway>(hps2->find_waterway_by_name("waterroute flood river"));
        auto wtr_tunnel2 = std::dynamic_pointer_cast<waterway>(hps2->find_waterway_by_name("waterroute input tunnel"));
        auto wtr_penstock2 = std::dynamic_pointer_cast<waterway>(hps2->find_waterway_by_name("waterroute penstock"));
        CHECK_EQ(wtr_flood->discharge_max_static.get(),wtr_flood2->discharge_max_static.get());
        CHECK_EQ(wtr_tunnel->head_loss_coeff.get(), wtr_tunnel2->head_loss_coeff.get());
        CHECK_EQ(wtr_penstock->head_loss_coeff.get(), wtr_penstock2->head_loss_coeff.get());
    
        auto ps2 = std::dynamic_pointer_cast<power_plant>(hps2->find_power_plant_by_name("plant"));
        auto gu2 = std::dynamic_pointer_cast<unit>(hps2->find_unit_by_name("aggregate"));
        CHECK_EQ(ps->outlet_level.get(), ps2->outlet_level.get());
        CHECK_EQ(gu->production_min_static.get(), gu2->production_min_static.get());
        CHECK_EQ(gu->production_max_static.get(), gu2->production_max_static.get());

        // Finally, check that we can run optimization after have bound model:
        CHECK_EQ(true, c.optimize("dtss_optimize_unbound", ta, cmd));
        t_exit=utctime_now() + std::chrono::seconds(30);// reasonable limit
        while(c.get_state("dtss_optimize_unbound")!= model_state::idle && utctime_now()<t_exit)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

        stm = c.get_model("dtss_optimize_unbound");
        check_results(stm, rstm, t_begin, t_end, t_step);
        c.close();
        s.clear();
    } catch(exception const& e) {
        DOCTEST_MESSAGE(e.what());
        CHECK_EQ(true, false);
        s.clear();
    }
}

TEST_CASE("dstm_stress_optimize"
    * doctest::description("Testing that dtss handles multiple unbound time series properly before sending to SHOP")
    * doctest::skip(SKIP_OPTIMIZE)) {

    server_log_hook hook;
    configure_logger(hook, dlib::LALL);

    const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

    temp_dir tmpdir{"stm_srv_shop_optimize.test."};
    server s(tmpdir);
    //dir_cleanup wipe{tmpdir};
	auto port_no = s.start_server();
    s.add_container("test", (tmpdir / "ts").string());

    auto stm = build_simple_model_with_dtss(*(s.dtss), t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto stm2 = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400);
    auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 2400, true); // Expected results
    REQUIRE_GT(port_no, 0);
#ifdef _WIN32
        const int n_connects=30;
#else
        const int n_connects=20;
#endif
    try {
        // Add models:
        for (size_t i = 0; i<n_connects; ++i) {
            s.do_add_model("m" + to_string(i), stm);
        }
        CHECK_EQ(s.model_map.size(), n_connects);
        // Result for each thread:
        auto cmd = optimization_commands(run_id, write_files);
        vector<std::future<bool>> res;
        res.reserve(n_connects);

        for (size_t i=0; i<n_connects; ++i) {
            res.emplace_back(std::async(std::launch::async,
                [i, port_no, ta, dt = t_step, &rstm, &cmd]()->bool {
                    auto host_port = string("localhost:") + to_string(port_no);
                    client c(host_port);
                    string
                    mid = "m" + to_string(i);
                    // Evaluate model:
                    c.evaluate_model(mid, ta.total_period(), false, false);
                    // Do optimization:
                    CHECK_EQ(true, c.optimize(mid, ta, cmd));
                    auto t_exit = utctime_now() + std::chrono::seconds(30);// reasonable limit
                    while (c.get_state(mid) != model_state::idle && utctime_now() < t_exit)
                        std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    // CHECK results:
                    auto stm = c.get_model(mid);
                    auto t0 = ta.total_period().start;
                    auto tN = ta.total_period().end;
                    check_results(stm, rstm, t0, tN, dt);
                    return true;
                }));
        }
        CHECK_EQ(res.size(), n_connects);
        std::for_each(res.begin(), res.end(), [](auto & el) { CHECK(el.get()); });

    } catch(exception const& e) {
        DOCTEST_MESSAGE(e.what());
        CHECK_EQ(true, false);
        s.clear();
    }
}

TEST_CASE("optimize with subscription"
    * doctest::description("Optimizing, while holding a subscription to run parameters of the system.")
    * doctest::skip(SKIP_OPTIMIZE)) {
    dlib::set_all_logging_levels(dlib::LALL);
	const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	const auto n_steps = (size_t)((t_end - t_begin) / t_step);
	const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
	auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with results:


    string host_ip{"127.0.0.1"};
    temp_dir log_dir{"shyft.shop.tst"};
    int port = get_free_port();
    string doc_root = (log_dir/"doc_root").string();
    test::test_server srv(doc_root);
	srv.set_listening_ip(host_ip);
	auto port_no = srv.start_server();
	REQUIRE_GT(port_no, 0);// require vs. test.abort this part of test if we fail here

    try {
        srv.do_add_model("simple", stm);
        srv.do_add_model("simple_results", rstm);

        srv.start_web_api(host_ip, port, doc_root, 1, 1);
        REQUIRE_EQ(true, srv.web_api_running());
        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        boost::asio::io_context ioc;
        auto s1 = std::make_shared<test::run_params_session>(ioc, &srv);
        s1->run(host_ip, port);
        ioc.run();
        // Set up expected responses and comparisons.
        vector<string> expected_responses{
            string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":0,"data":"not found"},{"attribute_id":1,"data":"not found"},{"attribute_id":2,"data":"not found"},{"attribute_id":3,"data":"not found"}]}})_"),
            string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":0,"data":33},{"attribute_id":1,"data":"not found"},{"attribute_id":2,"data":"not found"},{"attribute_id":3,"data":"not found"}]}})_"),
            string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":0,"data":3},{"attribute_id":1,"data":3},{"attribute_id":2,"data":false},{"attribute_id":3,"data":{"gt":FIXED,"f":{"t":1514768400.0,"dt":3600.0,"n":18}}}]}})_"),
            string(R"_({"request_id":"finale","subscription_id":"initial","diagnostics":""})_")
        };
        auto responses = s1->responses_;
        s1.reset();


        REQUIRE_EQ(responses.size(), expected_responses.size());
        for (int i = 0; i < responses.size(); ++i) {
            bool found_match=false; // order of responses might differ for the two last
            for(int j=0;j<responses.size() && !found_match;++j) {
                found_match= responses[j]==expected_responses[i];
            }
            if(!found_match) {
                MESSAGE("failed for the "<< i << "th response: "<<expected_responses[i]<<"!="<<responses[i]);
                CHECK(found_match);
            }
        }
    } catch(...) {}
    srv.stop_web_api();
}
}
