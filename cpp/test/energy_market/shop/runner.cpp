#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

#ifdef SHYFT_WITH_SHOP
// Define static members of shop_logger
#include "fixture.h"
std::function<void(const char*)> shop_logger_hook::info;
std::function<void(const char*)> shop_logger_hook::warning;
std::function<void(const char*)> shop_logger_hook::error;
std::function<void()> shop_logger_hook::exit;

// Define external log functions called from Shop.lib
void SHOP_log_info(char* msg) {
	if (msg && shop_logger_hook::info)
		shop_logger_hook::info(msg);
}
void SHOP_log_warning(char* msg) {
	if (msg && shop_logger_hook::warning)
		shop_logger_hook::warning(msg);
}
void SHOP_log_error(char* msg) {
	if (msg && shop_logger_hook::error)
		shop_logger_hook::error(msg);
}
void SHOP_exit(void*) {
	if (shop_logger_hook::exit)
		shop_logger_hook::exit();
}
#endif
