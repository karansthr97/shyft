#include "doctest/doctest.h"
#include "model_simple.h"

#ifndef PATH_MAX //#if _WIN32
#define PATH_MAX _MAX_PATH
#endif

#ifdef _DEBUG
// Set these to true for improved debugging (in case it gets included in commit we should keep default false for release builds)
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
static const bool write_file_commands = false; // Should the test write log and results to file for manual inspection?
static const bool SKIP_OPTIMIZE = false;
#endif

TEST_SUITE("stm_shop_optimize") {

	static std::size_t run_id = 1; // Unique number, to not overwrite files from other tests when write_file_commands==true.

TEST_CASE("optimize simple model without inlet"
	* doctest::description("building and optmimizing simple model without inlet segment: aggregate-penstock-maintunnel-reservoir")
	* doctest::skip(SKIP_OPTIMIZE))
{
	const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
	auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true); // Model with expected results
	// Run optimization
	// Alt 1: Fixed res
	shyft::energy_market::stm::shop::shop_system::optimize(*stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
	// Alt 2: Dynamic res
	//auto n_steps = (size_t)((t_end - t_begin) / t_step);
	//shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };
	//stm->optimize(ta, options);
	// Compare results:
	check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with inlet"
	* doctest::description("building and optmimizing simple model with inlet segment: aggregate-inlet-penstock-maintunnel-reservoir")
	* doctest::skip(SKIP_OPTIMIZE))
{
	const bool always_inlet_tunnels = false;
	const bool use_defaults = false;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1);
	auto rstm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults, 1, true);
	shyft::energy_market::stm::shop::shop_system::optimize(*stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
	check_results(stm, rstm, t_begin, t_end, t_step);
}

TEST_CASE("optimize simple model with defaults"
	* doctest::description("building and optmimizing simple model without explicit values for lrl/hrl/maxvol/p_min/p_max/p_nom")
	* doctest::skip(SKIP_OPTIMIZE))
{
	const bool always_inlet_tunnels = false;
	const bool use_defaults = true;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
	shyft::energy_market::stm::shop::shop_system::optimize(*stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), shop_console_output, shop_log_files);
}

TEST_CASE("optimize with log handler"
	* doctest::description("building and optmimizing simple model with log handler attached")
	* doctest::skip(SKIP_OPTIMIZE))
{
	const bool always_inlet_tunnels = false;
	const bool use_defaults = true;
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto t_step = shyft::core::deltahours(1);
	auto stm = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);

	//struct test_stm_shop_log_handler : shop_stream_logger {
	//	//std::ostringstream stream;
	//	test_stm_shop_log_handler() : shop_stream_logger{ "test_stm_shop_log_handler", std::cout } { enter(); }
	//	~test_stm_shop_log_handler() { exit(); }
	//};
	//auto log_handler = std::make_shared<test_stm_shop_log_handler>();

	auto log_handler = std::make_shared<shop_memory_logger>();

	//shyft::energy_market::stm::shop::shop_system::optimize(stm, t_begin, t_end, t_step, optimization_commands(run_id, write_file_commands), silent);

	shyft::energy_market::stm::shop::shop_system shop{ shyft::core::utcperiod{t_begin, t_end}, t_step };
	shop.install_logger(log_handler);
	shop.set_logging_to_stdstreams(shop_console_output);
	shop.set_logging_to_files(shop_log_files);
	shop.emit(*stm);
	shop.command(optimization_commands(run_id, write_file_commands));
	shop.collect(*stm);
	//shop.export_topology();

	REQUIRE(log_handler->messages.size() > 0);

#if 0
	// For debugging:
	size_t i = 0;
	for (const auto& msg : log_handler->messages) {
		std::cout << i++ << " [" << (int)msg.severity << "]:" << msg.message;
}
#endif

#if _WIN32
	REQUIRE(log_handler->messages.size() == 157); // Note: Shop API version 13.1.1.d reduced number of messages considerably: 157 vs 269 in previous versions
	CHECK(log_handler->messages[0].severity == shop_message::info);
	CHECK(log_handler->messages[0].message.rfind("Overview SHOP environment variables", 1) != std::string::npos);
	CHECK(log_handler->messages[2].message.rfind("Full description. No curve fitting.", 0) != std::string::npos);
	CHECK(log_handler->messages[5].message.rfind("1. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[31].message.rfind("2. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[57].message.rfind("3. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[80].message.rfind("Incremental description.", 0) != std::string::npos);
	CHECK(log_handler->messages[83].message.rfind("1. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[109].message.rfind("2. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[135].message.rfind("3. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[156].message.rfind("Time used in CPLEX", 0) != std::string::npos);
#else
	// Linux version 13.1.2.c with Cplex 12.8.0 includes 6 additional messages, one "Calling CPXcheckprob" for each iteration.
	REQUIRE(log_handler->messages.size() == 163);
	CHECK(log_handler->messages[0].severity == shop_message::info);
	CHECK(log_handler->messages[0].message.rfind("Overview SHOP environment variables",1) != std::string::npos);
	CHECK(log_handler->messages[2].message.rfind("Full description. No curve fitting.", 0) != std::string::npos);
	CHECK(log_handler->messages[5].message.rfind("1. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[32].message.rfind("2. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[59].message.rfind("3. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[83].message.rfind("Incremental description.", 0) != std::string::npos);
	CHECK(log_handler->messages[86].message.rfind("1. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[113].message.rfind("2. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[140].message.rfind("3. iteration.", 0) != std::string::npos);
	CHECK(log_handler->messages[162].message.rfind("Time used in CPLEX", 0) != std::string::npos);
#endif
}

}
