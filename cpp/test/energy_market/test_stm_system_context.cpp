#include <doctest/doctest.h>
#include "build_stm_system.h"
#include <shyft/energy_market/stm/srv/context.h>
#include <shyft/energy_market/stm/srv/context_enums.h>


TEST_SUITE("stm") {
    using namespace shyft::energy_market::stm;
    using shyft::energy_market::stm::srv::stm_system_context;
    using shyft::energy_market::stm::srv::model_state;

    TEST_CASE("stm_system_context") {
        auto a = test::create_simple_system(1, "test_mdl");
        stm_system_context ctx(model_state::idle, a);

        // 1. state
        CHECK_EQ(ctx.get_state(), model_state::idle);

        // 2. messages
        CHECK_EQ(true, ctx.mdl->run_params != nullptr);
        CHECK_EQ(false, ctx.mdl->run_params->fx_log.exists());
        CHECK_EQ(true, ctx.message("test message"));
        auto msgs = ctx.mdl->run_params->fx_log.get();
        CHECK_EQ(msgs.size(), 1);
        CHECK_EQ(msgs[0].second, "test message");
    }
}
