/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <map>

namespace shyft::energy_market {    

    using std::map;
    using std::vector;

    template <class T>// assume map<key,shared_ptr<x>>
    bool equal_map_content(const T&a, const T&b) {
        if (a.size() != b.size()) return false;
        for (const auto&kv : a) {
            auto f = b.find(kv.first);
            if (f == b.end())
                return false;
            if (!((*f->second) == (*kv.second)))
                return false;
        }
        return true;
    }

    template <class T> // assume vector<shared_ptr<T>>
    bool equal_vector_content(const T&a, const T&b) {
        if (a.size() != b.size()) return false;
        for (size_t i = 0;i<a.size();++i) {
            if (!(a[i]==b[i] || (*a[i] == *b[i]) ) )
                return false;
        }
        return true;
    }

        
}
