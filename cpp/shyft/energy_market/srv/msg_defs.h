/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <shyft/core/dlib_utils.h>

namespace shyft::energy_market::srv {
    using std::string;

    /** @brief  message-types
    *
    * The message types used for the wire-communication of ec::srv.
    *
    */
    enum class message_type : uint8_t {
        SERVER_EXCEPTION,
        //-- generic stuff pr stm-core model
        MODEL_INFO,
        MODEL_INFO_UPDATE,
        MODEL_STORE,
        MODEL_READ,
        MODEL_DELETE,
    };
    
    using msg=shyft::core::msg_util<message_type>; ///< so that we get low-level functions adapted to our enum type
}


