#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

    energy_market_area::energy_market_area(){};
    
    energy_market_area::energy_market_area(int id, const string& name, const string&json, const stm_system_& sys) 
        : super{ id, name,json }, sys{ sys }  {}

}

