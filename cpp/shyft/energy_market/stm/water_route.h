#pragma once
#include <map>
#include <string>
#include <stdexcept>
#include <sstream>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>

#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {

    using std::string;
    using std::shared_ptr;
    using std::runtime_error;
    using std::dynamic_pointer_cast;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;

    using core::proxy_attr;

    struct gate;
    using gate_ = shared_ptr<gate>;

    struct waterway;
    using waterway_ = shared_ptr<waterway>;

    /** @brief a water_route, tunnel or river 
        *
        */
    struct waterway:hydro_power::waterway {
        using super=hydro_power::waterway;
        using ds_collection_t=waterway_ds;
        using ids = hps_ids<waterway>;
        using rds = hps_rds<waterway>;
        using e_attr=wtr_attr; // enum attr type
        using e_attr_seq_t=wtr_attr_seq_t;// the full sequence of attr type

        string url(const string& prefix="") const {
            std::stringstream ss;
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) ss << tmp->url(prefix);
            else ss << prefix;
            ss << "/W" << id;
            return ss.str();
        }
        
        waterway(int id, const string& name, const string& json, stm_hps_ &hps):super(id,name,json,hps) {}
        waterway()=default;

        static gate_ add_gate(waterway_ const& w, int id, const string& name, const string& json);

        proxy_attr<waterway, apoint_ts, wtr_attr, wtr_attr::discharge_max_static, ids> discharge_max_static{*this};
        proxy_attr<waterway, apoint_ts, wtr_attr, wtr_attr::head_loss_coeff, ids> head_loss_coeff{*this};
        proxy_attr<waterway, t_xyz_list_, wtr_attr, wtr_attr::head_loss_func, ids> head_loss_func{*this};

        proxy_attr<waterway, apoint_ts, wtr_attr, wtr_attr::discharge, rds> discharge{*this};

        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=waterway;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(wtr_attr,discharge_max_static)
            def_proxy_map(wtr_attr,head_loss_coeff)
            def_proxy_map(wtr_attr,head_loss_func)
            def_proxy_map(wtr_attr,discharge)
        };

        
        x_serialize_decl();
    };


    /** Needs a special class for accessing the ids through the water-route*/
    template<class T>
    struct hps_gate_ids {
        static stm_hps_ get_hps(T*o) {
            auto w = o->wtr_();
            if (!w)
                throw runtime_error("gate " + o->name + ": has no wtr owner");
            auto hps = w->hps_();
            if (!hps)
                throw runtime_error("gate .wtr" + o->name + ": has no hps owner");
            return  dynamic_pointer_cast<stm_hps>(hps);
        }
        static typename T::ds_collection_t& ds(T*o) {
            return  get_hps(o)->ids->get(o);
        }
        static typename T::ds_collection_t const & ds(const T*o) {
            return  get_hps(o)->ids->get(o);
        }
    };

    /** @brief Gate or,  Hatch, - controls flow into water-routes
    *
    * Gate's are active/passive elements of the hydro-power-system.
    * They can be controlled, some are binary (open/closed), typically tunnel-gates.
    * Gates for flood/bypass and be operated from closed up to full opening in steps,
    * to fulfil a certian plan or requirement.
    * Passive gates, are gates where the water flows when above certain level, typically for flooding
    * reservoirs etc.
    *
    * There can be several gates into a water-route, and they can be operated separately.
    * 
    * gate-plans (or requirements), can be positional, or by wanted flow.
    *  For positional gate-plans (cm, m, % of max opening etc.), this influences the gate-opening
    *  and the other factors, such as water-level up/down-stream determines the resulting flow.
    *  For flow-based gate-plan,(m3/s), - it's the other way around, the system would then
    *  try to figure out what gate-position (if any) that would give the wanted flow.
    *
    */
    struct gate : hydro_power::gate {
        using super = hydro_power::gate;
        using ds_collection_t = gate_ids;
        using ids = hps_gate_ids<gate>;
        using e_attr=gate_attr; // enum attr type
        using e_attr_seq_t=gate_attr_seq_t;// the full sequence of attr type

        string url(const string& prefix="") const {
            return prefix + "/G" + std::to_string(id);
        }
        //using rds = hps_gate_rds<gate>;//sih: implment, and uncomment when you need result attributes

        gate(int id, const string& name, const string&json);
        gate();

        proxy_attr<gate, apoint_ts, gate_attr, gate_attr::opening_schedule, ids> opening_schedule{ *this };
        proxy_attr<gate, apoint_ts, gate_attr, gate_attr::discharge_schedule, ids> discharge_schedule{ *this };
        proxy_attr<gate, apoint_ts, gate_attr, gate_attr::discharge, ids> discharge{ *this };
        proxy_attr<gate, t_xyz_list_, gate_attr, gate_attr::gate_description, ids> gate_description{ *this };

        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=gate;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(gate_attr, opening_schedule)
            def_proxy_map(gate_attr, discharge_schedule)
            def_proxy_map(gate_attr, discharge)
            def_proxy_map(gate_attr, gate_description)
        };

        x_serialize_decl();
    };

}

//x_serialize_export_key(shyft::energy_market::stm::waterway);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::stm::waterway, BOOST_PP_STRINGIZE(shyft::energy_market::stm::water_route));
x_serialize_export_key(shyft::energy_market::stm::gate);
