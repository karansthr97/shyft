#pragma once
#include <memory>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
    struct run_parameters;
    struct run_ds {
        run_ds()=default;
        run_params_ds rpa;//run parameter attributes
        run_params_ds& get(run_parameters* = nullptr) { return rpa; }
        run_params_ds const& get(const run_parameters* = nullptr) const { return rpa; }
        x_serialize_decl();
    };
    using run_ds_ = shared_ptr<run_ds>;
}

x_serialize_export_key(shyft::energy_market::stm::run_ds);
