#pragma once
#include <string>
#include <functional>
#include <fstream>

struct shop_logger_hook {
	static std::function<void(const char*)> info;
	static std::function<void(const char*)> warning;
	static std::function<void(const char*)> error;
	static std::function<void()> exit;
};

struct shop_logger {
	virtual void info(const char* msg) = 0;
	virtual void warning(const char* msg) = 0;
	virtual void error(const char* msg) = 0;
	virtual void enter() = 0; // NOTE: There is not a callback from Shop API matching this, but implementers can typically call it from constructor.
	virtual void exit() = 0;  // NOTE: It seems the Shop API never calls the matching callback, but implementers can call it from destructor.
	virtual ~shop_logger() {};
};

struct shop_message {
	enum severity_level {
		info,
		warning,
		error
	} severity;
	std::string message;
	shop_message(severity_level severity, std::string message) : severity{severity}, message{message} {}
};

struct shop_memory_logger : shop_logger {
	std::vector<shop_message> messages;
	shop_memory_logger() { messages.reserve(1000); }
	void info(const char* msg) override { messages.emplace_back(shop_message::info, msg); }
	void warning(const char* msg) override { messages.emplace_back(shop_message::warning, msg); }
	void error(const char* msg) override { messages.emplace_back(shop_message::error, msg); }
	void enter() override { /* os << "[" << id << "] [ENTER]" << std::endl; */ }
	void exit() override { /* os << "[" << id << "] [EXIT]" << std::endl; */ }
};


template <class OutputStream = std::ostream>
struct shop_stream_logger : shop_logger {
	std::string id;
	OutputStream& os;
	shop_stream_logger(std::string id, OutputStream& os) : id{ id }, os{ os } {}
	void info(const char* msg) override {
		os << "[" << id << "] [INFO] " << msg;
	}
	void warning(const char* msg) override {
		os << "[" << id << "] [WARNING] " << msg;
	}
	void error(const char* msg) override {
		os << "[" << id << "] [ERROR] " << msg;
	}
	void enter() override {
		os << "[" << id << "] [ENTER]" << std::endl;
	}
	void exit() override {
		os << "[" << id << "] [EXIT]" << std::endl;
	}
};
