/*
 * Handling of log messages from shop api.
 */
#include <shyft/energy_market/stm/shop/shop_logger.h>

// Externals needed by Shop.lib
//void SHOP_log_error(char*) {}
//void SHOP_log_info(char*) {}
//void SHOP_log_warning(char*) {}
//void SHOP_exit(void*) {}

std::function<void(const char*)> shop_logger_hook::info;
std::function<void(const char*)> shop_logger_hook::warning;
std::function<void(const char*)> shop_logger_hook::error;
std::function<void()> shop_logger_hook::exit;

// Define external log functions called from Shop.lib
void SHOP_log_info(char* msg) {
	if (shop_logger_hook::info)
		shop_logger_hook::info(msg);
}
void SHOP_log_warning(char* msg) {
	if (shop_logger_hook::warning)
		shop_logger_hook::warning(msg);
}
void SHOP_log_error(char* msg) {
	if (shop_logger_hook::error)
		shop_logger_hook::error(msg);
}
void SHOP_exit(void*) { // NOTE: It seems the Shop API never calls this
	if (shop_logger_hook::exit)
		shop_logger_hook::exit();
}
