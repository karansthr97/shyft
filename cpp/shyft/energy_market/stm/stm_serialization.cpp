
#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>

#include <shyft/energy_market/dataset.h>

#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/water_route.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>
#include <shyft/energy_market/stm/system_ds.h>

// stuff needed to ensure vector<T>, map<K,V> etc. are automagically serializable
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>

#include <sstream>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

/** from  google how to serialize tuple using a straight forward tuple expansion */
namespace boost::serialization {

    /** generic recursive template version of tuple_serialize */
    template<int N>
    struct tuple_serialize {
        template<class Archive, typename ...tuple_types>
        static void serialize(Archive& ar, std::tuple<tuple_types...>& t, const unsigned version) {
            ar & make_nvp("tpl_m", std::get<N - 1>(t));
            tuple_serialize<N - 1>::serialize(ar, t, version); // recursive expand/iterate over members
        }
    };
    /** specialize recurisive template expansion termination at 0 args */
    template<>
    struct tuple_serialize<0> {
        template<class Archive, typename ...tuple_types>
        static void serialize(Archive&, std::tuple<tuple_types...>&, const unsigned) {
            ;// zero elements to serialize, noop, btw: should we instead stop at <1>??
        }
    };

    template<class Archive, typename ...tuple_types>
    void serialize(Archive& ar, std::tuple<tuple_types...>& t, const unsigned version) {
        tuple_serialize<sizeof ...(tuple_types)>::serialize(ar, t, version);
    }
}



/** provide serialization as free function for the enum */
namespace boost::serialization {
    
template <class Archive,typename A>
void serialize(Archive& ar, shyft::energy_market::core::ds_ref<A>&r, const unsigned int /*version*/) {
    ar
    &make_nvp("id",r.id)
    &make_nvp("aid",r.attr_id)
    ;
}

}

// instantiate serialization templates here, ref. shyft expression-serialization for the ids_c  type
using namespace boost::serialization;
using core_iarchive=boost::archive::binary_iarchive;
using core_oarchive=boost::archive::binary_oarchive;


template <typename I>
template <class Archive>
void shyft::energy_market::core::dataset<I>::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("name",name)
    & make_nvp("elements",elements)
    ;
}

template < class ...ds_types>
template <class Archive>
void shyft::energy_market::core::ds_collection<ds_types...>::serialize(Archive& ar, const unsigned int /*version*/) {
    ar 
    & make_nvp("all_id",all_id)
    ;
}

/** BACKWARD COMPATIBLE serialization history goes here 
 *
 */ 
/**atttribute_types.h */
namespace shyft::energy_market::stm {
    	/** We are replacing t_double_ with apoint_ts.
	 * The naive approach would break serialization, so we need
	 * to do some versioning, as suggested in
	 * https://www.boost.org/doc/libs/1_71_0/libs/serialization/doc/tutorial.html
	 */
	namespace v0 {
		/* Old ds_collection types, with t_double_, where applicable.
		 */
		using unit_ds = core::ds_collection<
		    core::ds_t<t_double_, unit_attr>,
		    core::ds_t<t_xy_, unit_attr>,
		    core::ds_t<t_turbine_description_, unit_attr>,
		    core::ds_t<apoint_ts, unit_attr>
		>;

		using catchment_ds = core::ds_collection<
		    core::ds_t<t_double_, catchment_attr>
		>;

		using power_plant_ds = core::ds_collection<
		    core::ds_t<t_double_, pp_attr>,
		    core::ds_t<t_xy_, pp_attr>,
		    core::ds_t<apoint_ts, pp_attr>
		>;

		using reservoir_ds = core::ds_collection<
		    core::ds_t<t_double_, rsv_attr>,
		    core::ds_t<t_xy_, rsv_attr>,
		    core::ds_t<apoint_ts, rsv_attr>
		>;

		using waterway_ds = core::ds_collection<
		    core::ds_t<t_double_, wtr_attr>,
		    core::ds_t<t_xyz_list_, wtr_attr>,
		    core::ds_t<apoint_ts, wtr_attr>
		>;

		using run_params_ds = core::ds_collection<
		    core::ds_t<int, run_params_attr>,
		    core::ds_t<bool, run_params_attr>,
		    core::ds_t<generic_dt, run_params_attr>
        >;

		/* Function for creating an apoint_ts, containing the same information as
		 * a t_double_
		 */
		static apoint_ts convert_t_double(const t_double_& t){
			vector<double> values;
			vector<utctime> times;
			for (auto const& x : *t){
				times.push_back(x.first);
				values.push_back(x.second);
			}
			// we assume semantic last value + 10years in the lack of better definitions
			return apoint_ts(time_axis::point_dt(times, times.back()+calendar::YEAR*10), values,POINT_AVERAGE_VALUE);
		}

		/** @brief class for converting every element in a ds_collection.all_id to
		 * a similiar ds_collection which doesn't contain any t_double_ data.
		 *
		 * @tparam heldDC: The ds_collection type to convert to.
		 */
		template<typename heldDC>
		struct dataset_converter {
			heldDC* ds_coll;
			/** @brief Constructor for the dataset converterter.
			 *
			 * @param ads_coll: pointer to a ds_collection of type HeldDC to write a dataset to.
			 */
			dataset_converter(heldDC* ads_coll): ds_coll{ads_coll} {}

			/** @brief Default converter
			 *
			 * @tparam T: Value type
			 * @tparam A: Attribute type
			 * @param ds: dataset to add to ds_coll. If dataset with value type T is already present in ds_coll,
			 *   this dataset is extended with the data from ds.
			 */
			template<typename T, typename A>
			void operator()(core::dataset<core::ds_t<T, A>> const& ds) const {
				// Here we just do a simple extension of the dataset:
				// We cannot do a copy assignment as to_ds might not be empty at the start.
				auto to_ds = std::get< core::dataset<core::ds_t<T, A>> >(ds_coll->all_id);
				// Iterate over ds.elements and copy over to to_ds:
				for (auto const& x: ds.elements){
					to_ds.elements[x.first] = x.second;
				}
				std::get< core::dataset<core::ds_t<T, A>> >(ds_coll->all_id) = to_ds;
			}

			/** @brief Converter for t_double_
			 *
			 * @tparam A: Attribute type
			 * @param ds: dataset with value type t_double_.
			 *  Is added into the dataset of value type apoint_ts for ds_coll,
			 *  using the conversion convert_t_double(...)
			 */
			template<typename A>
			void operator()(core::dataset<core::ds_t<t_double_, A>> const& ds) const {
				// We cannot do a copy assignment as to_ds might not be empty at the start.
				auto to_ds = std::get< core::dataset<core::ds_t<apoint_ts, A>> >(ds_coll->all_id);
				// Iterate over ds.elements, convert to apoint_ts and copy over to to_ds:
				for (auto const& x: ds.elements){
					to_ds.elements[x.first] = convert_t_double(x.second);
				}
				std::get< core::dataset<core::ds_t<apoint_ts, A>> >(ds_coll->all_id) = to_ds;
			}
		};

		/** @brief function for iterating over each element in ds_collection.all_ids
		 * to create a new ds_collection using the visitor to convert, as needed.
		 */
		template<typename fromDC, typename toDC>
		toDC convert_ds_collection(fromDC const& from_dc) {
			toDC to_dc;
			boost::fusion::for_each(from_dc.all_id, dataset_converter(&to_dc));
			return to_dc;
		}
	}
	
	 /** v1: replacing gate_ids with more attributes */
    namespace v1 {
        using gate_ids = core::ds_collection<
            core::ds_t<apoint_ts, gate_attr>
        >;
        
        /** @brief class for converting every element in a ds_collection.all_id to
		 * a similiar ds_collection which have v1 gate_ids
		 *
		 * @tparam heldDC: The ds_collection type to convert to.
		 */
		template<typename heldDC>
		struct dataset_converter {
			heldDC* ds_coll;
			/** @brief Constructor for the dataset converterter.
			 *
			 * @param ads_coll: pointer to a ds_collection of type HeldDC to write a dataset to.
			 */
			dataset_converter(heldDC* ads_coll): ds_coll{ads_coll} {}

			/** @brief Default converter
			 *
			 * @tparam T: Value type
			 * @tparam A: Attribute type
			 * @param ds: dataset to add to ds_coll. If dataset with value type T is already present in ds_coll,
			 *   this dataset is extended with the data from ds.
			 */
			template<typename T, typename A>
			void operator()(core::dataset<core::ds_t<T, A>> const& ds) const {
				// Here we just do a simple extension of the dataset:
				// We cannot do a copy assignment as to_ds might not be empty at the start.
				auto to_ds = std::get< core::dataset<core::ds_t<T, A>> >(ds_coll->all_id);
				// Iterate over ds.elements and copy over to to_ds:
				for (auto const& x: ds.elements){
					to_ds.elements[x.first] = x.second;
				}
				std::get< core::dataset<core::ds_t<T, A>> >(ds_coll->all_id) = to_ds;
			}
		};

		/** @brief function for iterating over each element in ds_collection.all_ids
		 * to create a new ds_collection using the visitor to convert, as needed.
		 */
		template<typename fromDC, typename toDC>
		toDC convert_ds_collection(fromDC const& from_dc) {
			toDC to_dc;
			boost::fusion::for_each(from_dc.all_id, dataset_converter(&to_dc));
			return to_dc;
		}

    }
  /** stm_hps*/
      namespace v1 {
        struct hps_ds {
            hps_ds()=default;
            
            reservoir_ds rsv;
            waterway_ds wtr;
            unit_ds unitds;
            catchment_ds ctchm;
            power_plant_ds pwr_plant;
            v1::gate_ids gt;
            
            // sih: typemap (consider tuple of the above instead (sukk.))
            reservoir_ds& get(reservoir* = nullptr) { return rsv; }
            reservoir_ds const& get(const reservoir* = nullptr) const { return rsv; }

            waterway_ds& get(waterway*  = nullptr) { return wtr; }
            waterway_ds const& get(const waterway* = nullptr) const { return wtr; }

            unit_ds& get(unit* = nullptr) { return unitds; }
            unit_ds const& get(const unit* = nullptr) const { return unitds; }

            catchment_ds& get(catchment* = nullptr) { return ctchm; }
            catchment_ds const& get(const catchment * = nullptr) const { return ctchm; }

            power_plant_ds& get(power_plant* = nullptr) { return pwr_plant; }
            power_plant_ds const& get(const power_plant* = nullptr) const { return pwr_plant; }

            gate_ids& get(gate* = nullptr) { return gt; }
            gate_ids const& get(const gate* = nullptr) const { return gt; };
            /** @brief Converter function from v0::hps_ds to stm::hps_ds.
                *
                * @return stm::hps_ds, where datasets with value type t_double_
                *  have been converted to datasets with value type apoint_ts.
                */
            shyft::energy_market::stm::hps_ds convert_to_v2() {
                shyft::energy_market::stm::hps_ds new_ds;
                new_ds.rsv = rsv;
                new_ds.wtr = wtr;
                new_ds.unitds = unitds;
                new_ds.ctchm = ctchm;
                new_ds.pwr_plant = pwr_plant;
                new_ds.gt = convert_ds_collection<v1::gate_ids, stm::gate_ids>(gt);
                return new_ds;
            }
            x_serialize_decl();
        };
        using hps_ds_ = shared_ptr<hps_ds>;

    }

    /** We are replacing t_double_ with apoint_ts.
	 * The naive approach would break serialization, so we need
	 * to do some versioning, as suggested in
	 * https://www.boost.org/doc/libs/1_71_0/libs/serialization/doc/tutorial.html
	 */
    namespace v0 {
		/** @brief The old version of hps_ds, where the datasets may contain t_double_
         *
         */
		struct hps_ds {
			hps_ds() = default;

			v0::reservoir_ds rsv;
			v0::waterway_ds wtr;
			v0::unit_ds unitds;
			v0::catchment_ds ctchm;
			v0::power_plant_ds pwr_plant;
			v1::gate_ids gt;

			// sih: typemap (consider tuple of the above instead (sukk.))
			reservoir_ds &get(reservoir * = nullptr) { return rsv; }

			reservoir_ds const &get(const reservoir * = nullptr) const { return rsv; }

			waterway_ds &get(waterway * = nullptr) { return wtr; }

			waterway_ds const &get(const waterway * = nullptr) const { return wtr; }

			unit_ds &get(unit * = nullptr) { return unitds; }

			unit_ds const &get(const unit * = nullptr) const { return unitds; }

			catchment_ds &get(catchment * = nullptr) { return ctchm; }

			catchment_ds const &get(const catchment * = nullptr) const { return ctchm; }

			power_plant_ds &get(power_plant * = nullptr) { return pwr_plant; }

			power_plant_ds const &get(const power_plant * = nullptr) const { return pwr_plant; }

			v1::gate_ids &get(gate * = nullptr) { return gt; }

			v1::gate_ids const &get(const gate * = nullptr) const { return gt; };

			/** @brief Converter function from v0::hps_ds to stm::hps_ds.
             *
             * @return stm::hps_ds, where datasets with value type t_double_
             *  have been converted to datasets with value type apoint_ts.
             */
			shyft::energy_market::stm::v1::hps_ds convert_to_v1() {
				shyft::energy_market::stm::v1::hps_ds new_ds;
				new_ds.rsv = convert_ds_collection<v0::reservoir_ds, stm::reservoir_ds>(rsv);
				new_ds.wtr = convert_ds_collection<v0::waterway_ds, stm::waterway_ds>(wtr);
				new_ds.unitds = convert_ds_collection<v0::unit_ds, stm::unit_ds>(unitds);
				new_ds.ctchm = convert_ds_collection<v0::catchment_ds, stm::catchment_ds>(ctchm);
				new_ds.pwr_plant = convert_ds_collection<v0::power_plant_ds, stm::power_plant_ds>(pwr_plant);
				new_ds.gt = convert_ds_collection<v1::gate_ids, v1::gate_ids>(gt);
				return new_ds;
			}

			x_serialize_decl();
		};

		using hps_ds_ = shared_ptr<hps_ds>;

		struct run_ds {
			run_ds() = default;

			v0::run_params_ds rpa;

			run_params_ds &get(run_parameters * = nullptr) { return rpa; }

			run_params_ds const &get(const run_parameters * = nullptr) const { return rpa; }

			shyft::energy_market::stm::run_ds convert_to_v1() {
				shyft::energy_market::stm::run_ds new_ds;
				new_ds.rpa = convert_ds_collection<v0::run_params_ds, stm::run_params_ds>(rpa);
				return new_ds;
			}

			x_serialize_decl();
		};

		using run_ds_ = shared_ptr<run_ds>;
	}
}

x_serialize_export_key(shyft::energy_market::stm::v0::hps_ds);
x_serialize_export_key(shyft::energy_market::stm::v1::hps_ds);
x_serialize_export_key(shyft::energy_market::stm::v0::run_ds);

/****/

template<class Archive>
void shyft::energy_market::stm::reservoir::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::unit::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& 	make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::power_plant::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& 	make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::catchment::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	& make_nvp("super",	base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::waterway::serialize(Archive & ar, const unsigned int /*version*/) {
	ar 
	&  make_nvp("super",base_object<super>(*this))
    ;
}

template<class Archive>
void shyft::energy_market::stm::gate::serialize(Archive & ar, const unsigned int /*version*/) {
	ar
	&  make_nvp("super", base_object<super>(*this))
	;
}

template<class Archive>
void shyft::energy_market::stm::stm_hps::serialize(Archive & ar, const unsigned int version) {
    ar
    & make_nvp("super",base_object<super>(*this)); // core hydro power goes here(core model,topology)
    if(version < 2 && !Archive::is_saving::value) {
        // Here we need to serialize into the old version of the dataset, and then convert...
        if(version<1) {
            shyft::energy_market::stm::v0::hps_ds_ old_ids, old_rds;
            ar & old_ids& old_rds;
            // Now we need to convert...
            ids = std::make_shared<shyft::energy_market::stm::hps_ds>(old_ids->convert_to_v1().convert_to_v2());
            rds = std::make_shared<shyft::energy_market::stm::hps_ds>(old_rds->convert_to_v1().convert_to_v2());
        } else {
            shyft::energy_market::stm::v1::hps_ds_ old_ids, old_rds;
            ar & old_ids& old_rds;
            // Now we need to convert...
            ids = std::make_shared<shyft::energy_market::stm::hps_ds>(old_ids->convert_to_v2());
            rds = std::make_shared<shyft::energy_market::stm::hps_ds>(old_rds->convert_to_v2());
        }
    } else {
        ar & make_nvp("ids",ids)
           & make_nvp("rds", rds)
           ;
    }
}


template<class Archive>
void shyft::energy_market::stm::run_parameters::serialize(Archive& ar, const unsigned int version) {
	ar
	& make_nvp("super", base_object<super>(*this))
	& make_nvp("mdl", mdl);
	if (version < 1 && !Archive::is_saving::value) {
		shyft::energy_market::stm::v0::run_ds_ old_ds;
		ar & old_ds;
		ds = std::make_shared<shyft::energy_market::stm::run_ds>(old_ds->convert_to_v1());
	} else {
		ar & make_nvp("ds", ds);
	}
}

template<class Archive>
void shyft::energy_market::stm::stm_system::serialize(Archive& ar, const unsigned int version) {
    ar
    & make_nvp("super",base_object<super>(*this))
    & make_nvp("hps",hps)
    & make_nvp("market",market)
    & make_nvp("ids",ids)
    & make_nvp("rds",rds)
    ;
    if (version <1 && !Archive::is_saving::value) {
    	run_params = std::make_shared<shyft::energy_market::stm::run_parameters>(this);
    } else {
    	ar & make_nvp("run_params", run_params);
    }
}

template<class Archive>
void shyft::energy_market::stm::hps_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("rsv",rsv)
    & make_nvp("wtr",wtr)
    & make_nvp("pwr_stn", pwr_plant)
    & make_nvp("agg",unitds)
    & make_nvp("catchment", ctchm)
	& make_nvp("gt",gt)
    ;
}

template<class Archive>
void shyft::energy_market::stm::v0::hps_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("rsv",rsv)
    & make_nvp("wtr",wtr)
    & make_nvp("pwr_stn", pwr_plant)
    & make_nvp("agg",unitds)
    & make_nvp("catchment", ctchm)
	& make_nvp("gt",gt)
    ;
}
template<class Archive>
void shyft::energy_market::stm::v1::hps_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("rsv",rsv)
    & make_nvp("wtr",wtr)
    & make_nvp("pwr_stn", pwr_plant)
    & make_nvp("agg",unitds)
    & make_nvp("catchment", ctchm)
	& make_nvp("gt",gt)
    ;
}

template<class Archive>
void shyft::energy_market::stm::market_ds::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("ema",ema)
    ;
}

template<class Archive>
void shyft::energy_market::stm::energy_market_area::serialize(Archive& ar, const unsigned int /*version*/) {
    ar
    & make_nvp("super",base_object<super>(*this))
    & make_nvp("sys",sys)//weak_ptr Hmm. did that really work ..
    ;
}

template<class Archive>
void shyft::energy_market::stm::run_ds::serialize(Archive& ar, const unsigned int /*version*/){
    ar
    & make_nvp("rpa", rpa);
}

template<class Archive>
void shyft::energy_market::stm::v0::run_ds::serialize(Archive& ar, const unsigned int /*version*/) {
	ar & make_nvp("rpa", rpa);
}

//
// 4. Then include the archive supported
//

// repeat template instance for each archive class
#define xxx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)

xxx_arch(shyft::energy_market::stm::unit);
xxx_arch(shyft::energy_market::stm::power_plant);
xxx_arch(shyft::energy_market::stm::reservoir);
xxx_arch(shyft::energy_market::stm::catchment);
xxx_arch(shyft::energy_market::stm::waterway);
xxx_arch(shyft::energy_market::stm::gate);
xxx_arch(shyft::energy_market::stm::stm_hps);
xxx_arch(shyft::energy_market::stm::run_parameters);
xxx_arch(shyft::energy_market::stm::stm_system);
xxx_arch(shyft::energy_market::stm::energy_market_area);


namespace shyft::energy_market::stm {

/** polymorphic types needed some pre-registration to stream
 * If you get 'unknown class' error while doing serialization,
 * this is the place.
 */
template <class Archive>
void register_types(Archive& a) {
    a.template register_type<reservoir>();
    a.template register_type<waterway>();
    a.template register_type<unit>();
	a.template register_type<gate>();
	a.template register_type<power_plant>();
    a.template register_type<catchment>();
    a.template register_type<stm_hps>();
    //a.template register_type<run_parameters>();
    a.template register_type<stm_system>();
    a.template register_type<energy_market_area>();
}


/**fx_to_blob simply serializes an object to a blob */
template <class T>
static string fx_to_blob(const shared_ptr<T>&s) {
    using namespace std;
    std::ostringstream xmls;
    {
        core_oarchive oa(xmls,core_arch_flags);
        register_types(oa);
        oa << boost::serialization::make_nvp("hps", s);
    }
    xmls.flush();
    return xmls.str();
}

/** fx_from_blob de-serializes a blob to a fully working object*/
template<class T>
static shared_ptr<T> fx_from_blob(const string &xmls) {
    shared_ptr<T> s;
    std::istringstream xmli(xmls); {
        //boost::archive::xml_iarchive ia(xmli);
        core_iarchive ia(xmli,core_arch_flags);
        register_types(ia);
        ia >> boost::serialization::make_nvp("hps", s);
	}
    return s;
}

// implementation for main classes, using the templates above

string  stm_hps::to_blob(const shared_ptr<stm_hps>&s) {return fx_to_blob<stm_hps>(s);}
shared_ptr<stm_hps> stm_hps::from_blob(const string &xmls) {return fx_from_blob<stm_hps>(xmls);}
string  stm_system::to_blob(const shared_ptr<stm_system>&s) { return fx_to_blob<stm_system>(s);}
shared_ptr<stm_system> stm_system::from_blob(const string &xmls) { return fx_from_blob<stm_system>(xmls);}
    
}


