#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/aggregate.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using std::map;
	using std::true_type;
	using std::false_type;
	using shyft::core::utctime;
	using shyft::time_series::dd::apoint_ts;
	using core::proxy_attr;

	struct power_plant : hydro_power::power_plant {
		using super = hydro_power::power_plant;
		using ds_collection_t =power_plant_ds;
		using ids = hps_ids<power_plant>;
		//using rds = hps_rds<power_station>;// sih: when needed, uncomment this
        using e_attr=pp_attr; // enum attr type
        using e_attr_seq_t=pp_attr_seq_t;// the full sequence of attr type
        
        string url(const string& prefix="") const {
            std::stringstream ss;
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) ss << tmp->url(prefix);
            else ss << prefix;
            ss << "/P" << id;
            return ss.str();
        }
        
		power_plant(int id, const string& name, const string&json, const stm_hps_ &hps) :super(id, name, json, hps) {}
		power_plant()=default;

		static void add_aggregate(const power_plant_&ps,const unit_&a);

		void remove_aggregate(const unit_&a);

		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::outlet_level, ids> outlet_level{*this};

		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::mip, ids> mip{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::unavailability, ids> unavailability{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::production_min, ids> production_min{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::production_max, ids> production_max{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::production_schedule, ids> production_schedule{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::discharge_min, ids> discharge_min{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::discharge_max, ids> discharge_max{ *this };
		proxy_attr<power_plant, apoint_ts, pp_attr, pp_attr::discharge_schedule, ids> discharge_schedule{ *this };

        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=power_plant;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(pp_attr,outlet_level)
            def_proxy_map(pp_attr,mip)
            def_proxy_map(pp_attr,unavailability)
            def_proxy_map(pp_attr,production_min)
            def_proxy_map(pp_attr,production_max)
            def_proxy_map(pp_attr,production_schedule)
            def_proxy_map(pp_attr,discharge_min)
            def_proxy_map(pp_attr,discharge_max)
            def_proxy_map(pp_attr,discharge_schedule)
        };
        
		x_serialize_decl();
	};
	using power_plant_ = shared_ptr<power_plant>;

}
//x_serialize_export_key(shyft::energy_market::stm::power_plant);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::stm::power_plant, BOOST_PP_STRINGIZE(shyft::energy_market::stm::power_station));

