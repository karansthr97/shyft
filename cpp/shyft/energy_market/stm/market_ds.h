#pragma once
#include <memory>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>


namespace shyft::energy_market::stm {
    struct energy_market_area;
    struct market_ds {
        market_ds()=default;
        energy_market_area_ds ema;
        energy_market_area_ds& get(energy_market_area* =nullptr) { return ema;}
        energy_market_area_ds const& get(const energy_market_area* = nullptr) const { return ema; }
        x_serialize_decl();
    };
    using market_ds_ = shared_ptr<market_ds>;

}

x_serialize_export_key(shyft::energy_market::stm::market_ds);
