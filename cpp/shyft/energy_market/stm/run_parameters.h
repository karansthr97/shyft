#pragma once
#include <memory>
#include <string>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/system_ds.h>

namespace shyft::energy_market::stm {
    using std::string;
    using shyft::energy_market::id_base;
    using shyft::core::utctime;
    using std::pair;

    struct run_ds_accessor; // Forward declaration of accessor class
    struct stm_system;

    struct run_parameters: public id_base {
        using super = id_base;
        using ds_collection_t = run_params_ds;
        using e_attr=run_params_attr;// enum attr type
        using e_attr_seq_t=run_params_attr_seq_t;//Full sequence of attr type
        using ds_t = run_ds_accessor;

        stm_system * mdl; // The pointer should be const, but the model can be modified.
        run_ds_ ds;

        run_parameters() = default;
        run_parameters(stm_system* mdl): mdl{mdl} { ds = std::make_shared<run_ds>(); }

        string url(const string& prefix);
        // Attributes:
        proxy_attr<run_parameters, int, run_params_attr, run_params_attr::n_inc_runs, ds_t> n_inc_runs{*this}; // Number of runs with incremental
        proxy_attr<run_parameters, int, run_params_attr, run_params_attr::n_full_runs, ds_t> n_full_runs{*this}; // Number of full runs
        proxy_attr<run_parameters, bool, run_params_attr, run_params_attr::head_opt, ds_t> head_opt{*this}; // Number of head optimization runs.
        proxy_attr<run_parameters, generic_dt, run_params_attr, run_params_attr::run_time_axis, ds_t> run_time_axis{*this};
        proxy_attr<run_parameters, vector<pair<utctime, string>>, run_params_attr, run_params_attr::fx_log, ds_t> fx_log{*this};
        // meta-programming support, list all mappings of the above proxy attributes here
        struct a_map {
            using proxy_container=run_parameters;
            // static constexpr auto ref(run_params_attr_c<run_params_attr::n_inc_runs>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(run_params_attr, n_inc_runs);
            def_proxy_map(run_params_attr, n_full_runs);
            def_proxy_map(run_params_attr, head_opt);
            def_proxy_map(run_params_attr, run_time_axis);
            def_proxy_map(run_params_attr, fx_log);
        };

        x_serialize_decl();
    };
    using run_parameters_ = std::shared_ptr<run_parameters>;
    using run_parameters__ = std::weak_ptr<run_parameters>;

    /** @brief accessor class for an stm_system dataset.
     *
     *  Used to provide access to the dataset storing
     *  an stm_system's own proxy_attributes.
     */
    struct run_ds_accessor {
        static run_parameters::ds_collection_t& ds(run_parameters* o){
            return o->ds->get(o);
        }

        static run_parameters::ds_collection_t const& ds(const run_parameters* o){
            return o->ds->get(o);
        }
    };
}

x_serialize_export_key(shyft::energy_market::stm::run_parameters);

BOOST_CLASS_VERSION(shyft::energy_market::stm::run_parameters, 1);