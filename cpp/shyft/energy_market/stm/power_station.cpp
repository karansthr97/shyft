#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

    using std::make_shared;
    using std::static_pointer_cast;

    void power_plant::add_aggregate(const power_plant_&ps,const unit_&a) {
        hydro_power::power_plant::add_unit(ps,a);
    }
        
    void power_plant::remove_aggregate(const unit_&a) {
        super::remove_unit(a);
    }

}
