#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/hps_ds.h>

namespace shyft::energy_market::stm {
    
    using std::static_pointer_cast;
    using std::dynamic_pointer_cast;
    using std::make_shared;
    using std::runtime_error;


    gate::gate(int id, const string& name, const string& json) :super{ id,name,json }{}
    gate::gate() = default;


    gate_ waterway::add_gate(waterway_ const& w, int id, const string& name, const string& json) {
        auto g = make_shared<gate>(id, name, json);
        super::add_gate(w, g);
        return g;
    }
}
