#include <shyft/energy_market/stm/srv/context.h>
#include <shyft/energy_market/stm/srv/server.h>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::srv {
    using shyft::core::utctime_now;

    bool stm_system_context::message(const string& msg) {
        if (mdl) {
            if (mdl->run_params) {
                vector<pair<utctime, string>> msgs;
                if (mdl->run_params->fx_log.exists())
                    auto msgs = mdl->run_params->fx_log.get();
                msgs.push_back({utctime_now(), msg});
                mdl->run_params->fx_log.set(msgs);
                return true;
            }
            return false;
        }
        return false;
    }
}
