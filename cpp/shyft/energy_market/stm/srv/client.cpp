#include <shyft/energy_market/stm/srv/client.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>


namespace shyft::energy_market::stm::srv {
    
using shyft::core::core_iarchive;
using shyft::core::core_oarchive;

client::client(string host_port,int timeout_ms):c{host_port,timeout_ms}{}

string client::version_info() {
    scoped_connect sc(c);
    string r{};
    do_io_with_repair_and_retry(c,[this,&r](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::VERSION_INFO,io);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::VERSION_INFO) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::create_model(string const& mid){
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[this,&r,&mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::CREATE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::CREATE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::add_model(string const& mid, stm_system_ mdl){
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[this,&r,&mid,&mdl](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::ADD_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << mdl;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::ADD_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::remove_model(string const&mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[this,&r,&mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::REMOVE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REMOVE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::rename_model(string const& old_mid, string const& new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[this,&r,&old_mid,&new_mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::RENAME_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << old_mid << new_mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::RENAME_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::clone_model(string const& old_mid, string const& new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[this,&r,&old_mid,&new_mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::CLONE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << old_mid << new_mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::CLONE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

vector<string> client::get_model_ids() {
    scoped_connect sc(c);
    vector<string> r;
    do_io_with_repair_and_retry(c,[this,&r](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_MODEL_IDS,io);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_MODEL_IDS) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

map<string, model_info> client::get_model_infos() {
    scoped_connect sc(c);
    map<string, model_info> r;
    do_io_with_repair_and_retry(c,[this,&r](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_MODEL_INFOS,io);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_MODEL_INFOS) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

stm_system_ client::get_model(string const& mid) {
    scoped_connect sc(c);
    stm_system_ r;
    do_io_with_repair_and_retry(c,[this,&r,&mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

#ifdef SHYFT_WITH_SHOP
bool client::optimize(const string& mid, const generic_dt& ta, const vector<shyft::energy_market::stm::shop::shop_command>& cmd) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c,[this,&r,&mid,&ta,&cmd](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::OPTIMIZE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << ta << cmd;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::OPTIMIZE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}
#endif
/** @brief get state of a model:
    */
model_state client::get_state(const string& mid) {
    scoped_connect sc(c);
    model_state r;
    do_io_with_repair_and_retry(c,[this,&r,&mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_STATE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_STATE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}


/** @brief get SHOP log for model.
    */
string client::get_log(const string& mid) {
    scoped_connect sc(c);
    string r;
    do_io_with_repair_and_retry(c,[this,&r,&mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_LOG,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_LOG) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

    /** @brief exeute fx(mid,fx_arg) on the server side
    */
bool client::fx(const string& mid, const string& fx_arg) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c,[this,&r,&mid,&fx_arg](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::FX,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<fx_arg;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::FX) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

/** @brief evaluate any unbound time series attributes in a model.
    */
bool client::evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c, [this, &r, &mid, &bind_period, use_ts_cached_read, update_ts_cache, &clip_period](srv_connection& c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::EVALUATE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << bind_period << use_ts_cached_read << update_ts_cache << clip_period;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::EVALUATE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response: ") + to_string((int)response_type));
        }
    });
    return r;
}
    


/** @brief close, until needed again, the server connection
*
*/
void client::close() {
    c.close();//just close-down connection, it will auto-open if needed
}

}
