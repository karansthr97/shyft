#pragma once
#include <functional>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/srv/server.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/web_api/json_struct.h>
#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/time_series/dd/ipoint_ts.h>

namespace shyft::energy_market::stm::subscription {
    using std::string;
    
    using shyft::time_series::dd::ats_vector;
    using shyft::time_series::dd::ts_as;
    using shyft::time_series::dd::gpoint_ts;
    
    using shyft::core::subscription::observer_base;
    using shyft::dtss::subscription::ts_expression_observer_;
    using shyft::dtss::subscription::ts_expression_observer;
    using shyft::core::subscription::manager_;
    using shyft::core::subscription::observable_;
    using shyft::energy_market::core::proxy_attr;
    using shyft::web_api::energy_market::json;
    using shyft::web_api::bg_work_result;
    
    using shyft::energy_market::stm::srv::server;    
    using shyft::energy_market::stm::stm_system_;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::catchment;

    
    /** @brief observer class for attributes in an stm_system/stm_hps */
    struct proxy_attr_observer: observer_base {
       
        proxy_attr_observer(server *const srv, string const& request_id, json const& data, std::function<bg_work_result(json const&)>&& cb):
                observer_base(srv->sm, request_id), request_data{data}, response_cb{std::move(cb)} {
            mid = boost::get<string>(request_data.required("model_id"));
            // In the case that we use the dtss to keep subscriptions for time series:
            if (srv->dtss) {
                ts_sm = srv->dtss->sm;
            } else
                ts_sm = srv->sm;
        }
        
        virtual bool recalculate() override {
            auto updated = has_changed();
            published_version = terminal_version();
            return updated;
        }
        
        virtual int64_t terminal_version() const noexcept {
            int64_t r = observer_base::terminal_version();
            for (auto& sub : ts_subs) r += sub->terminal_version();
            return r;
        }

        bg_work_result re_emit_response() const {
            return response_cb(request_data);
        }
        
        /** @brief Add an observable to terminals based on a proxy attribute. Returns true if new subscription was added.
         *  This is called whenever we are not adding a time series
         *
         * @tparam ProxyAttr: Type of proxy attribute.
         *      requirements:
         *          - Must have method ProxyAttr::ae() with return-type convertible to int.
         *          - Must contain attribute T* o, where T is the type of the owning entity (reservoir/waterway/&c.)
         *          - Must contain type value ProxyAttr::value_type and method value_type ProxyAttr::get()
         */
        template<class ProxyAttr,
            typename std::enable_if_t<!std::is_same<typename ProxyAttr::value_type,apoint_ts>::value, int> = 0 >
        bool add_subscription(ProxyAttr const& pa){
            auto subject = sm->add_subscription(pa.url(string("dstm://M") + mid + "/"))[0];
            // Check that it's not already part of terminals
            auto it = std::find_if(terminals.begin(), terminals.end(),
                                   [&subject](auto el) { return el->id == subject->id; });
            if (it == terminals.end()){
                terminals.emplace_back(subject);
                return true;
            } else
                return false;
        }
        
        /** @brief Add an observable to terminals based on a proxy attribute. Returns true if new subscription was added.
         *  This is called whenever we are adding a time series. If the server contains a dtss, we let the dtss
         *  handle the subscription.
         *
         * @tparam ProxyAttr: Type of proxy attribute.
         *      requirements:
         *          - Must have method ProxyAttr::ae() with return-type convertible to int.
         *          - Must contain attribute T* o, where T is the type of the owning entity (reservoir/waterway/&c.)
         *          - Must contain type value ProxyAttr::value_type and method value_type ProxyAttr::get()
         */
        template<class ProxyAttr,
            typename std::enable_if_t<std::is_same<typename ProxyAttr::value_type,apoint_ts>::value, int> = 0 >
        bool add_subscription(ProxyAttr const& pa){
            auto sub_id = pa.url("dstm://M" + mid + "/");
            auto it = std::find_if(ts_subs.begin(), ts_subs.end(),
                                   [&sub_id](auto el) { return el->request_id == sub_id; });
            if (it == ts_subs.end()){
                dtss::ts_vector_t tsv;
                apoint_ts const& ts = pa.get();
                if (ts_as<gpoint_ts>(ts.ts))
                    tsv.emplace_back(apoint_ts(sub_id, pa.get()));
                else
                    tsv.emplace_back(pa.get());
                auto new_sub = std::make_shared<ts_expression_observer>(ts_sm, sub_id, tsv, [](ats_vector)->ats_vector { return ats_vector{}; });
                ts_subs.emplace_back(new_sub);
                return true;
            } else
                return false;
        }
                
        // Member variables:
        manager_ ts_sm;             //<- subscription manager for time series
        vector<ts_expression_observer_> ts_subs; //<- Subscriptions to time series, which should be handled by the dtss, if present
        json request_data;          //<- The data containing what attributes, what components, what model to subscribe to.
        string mid;
        std::function<bg_work_result(json const&)> response_cb; ///<- callback function to be called when value of observed is updated and response has to be re emitted.
    };
    using proxy_attr_observer_ = std::shared_ptr<proxy_attr_observer>;
}
