#include <shyft/energy_market/stm/srv/server.h>

#include <signal.h> // For custom handling of SIGSEGV
#include <setjmp.h> // For longjmp and setjmp. See https://stackoverflow.com/questions/8401689/best-practices-for-recovering-from-a-segmentation-fault
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/reservoir.h>

#include <shyft/energy_market/stm/srv/context.h>

namespace shyft::energy_market::stm::srv {
	using shyft::core::core_iarchive;
	using shyft::core::core_oarchive;

    using shyft::energy_market::core::proxy_attr_m;
    template<typename T, typename V, typename A, A a, typename C> using stm_attr_t = proxy_attr<T, V, A, a, C>;
    
    const dlib::logger server::slog{"dstm"};

    //atomic_uint shop_segfault_handler::curr_opt = 0;
    //bool shop_segfault_handler::sigsegv_received = false;
    thread_local bool shop_segfault_handler::shop_call = false;

    jmp_buf buf;
    /** @brief Any shop call from server, on any thread, should now be enclosed in 
     * 
     * if (!setjmp(buf)) {
     *   seg_handler.shop_call = true;
     *  <Do stuff with shop>
     *   seg_handler.shop_call = false;
     * } else {
     *   <This code block is run if SIGSEGV is received from shop>
     * }
     */
    void shop_segfault_handler::sigsegv_handler(int signum) {
            if (shop_call) {
                if (!sigsegv_received) // We haven't encountered a SIGSEGV from SHOP before
                    sigsegv_received = true;
                longjmp(buf, curr_opt);
            } else {
                SIG_DFL(signum);
            }
    }
    
    shop_segfault_handler::shop_segfault_handler() {
        curr_opt++;
        if (curr_opt > 0) {
            signal(SIGSEGV, sigsegv_handler);
        }
    }

    shop_segfault_handler::~shop_segfault_handler() {
        if (curr_opt) curr_opt--;
        if (curr_opt == 0) {
            signal(SIGSEGV, SIG_DFL);
        }
    } 
    
    
    server::server(): log_dir{fs::temp_directory_path() / fs::path("logdir")}{
        if (!fs::exists(log_dir))
            fs::create_directory(log_dir);
        setup_dtss();
    };
    server::server(fs::path const& alog_dir): log_dir{alog_dir} {
        if (!fs::exists(log_dir))
            fs::create_directory(log_dir);
        setup_dtss();
    }
    
    
    /** start the server in background, return the listening port used in case it was set unspecified */
    int server::start_server() {
        if(get_listening_port()==0) {
            
            start_async();
            while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
        } else {
            start_async();
        }
        auto port_num = get_listening_port();
        slog << dlib::LINFO << "Started server on port " << port_num;
        return port_num;
    }

    /** Set up the dtss */
    void server::setup_dtss() {
        // TODO: Insert a callback to read non shyft:// unbound time-series.
        dtss = make_unique<ts_server>([this](const id_vector_t& ts_ids, utcperiod p) -> ts_vector_t { return this->dtss_read_callback(ts_ids, p); });
    }
    
    /** add container to dtss */
    void server::add_container(const string& container_name, const string& root_dir) {
        if (!dtss)
            throw runtime_error("Dtss hasn't been set. Call server::setup_dtss() before adding container.");
        dtss->add_container(container_name, root_dir);
    }
    
    /** @brief callback function for reading dstm:// time series */
    ts_vector_t server::dtss_read_callback(const id_vector_t& ts_ids, utcperiod p) {
        // recordthe event here
        ts_vector_t r;
        r.reserve(ts_ids.size());
        shyft::web_api::grammar::dstm_ts_url_grammar<const char*> grammar{this};
        apoint_ts ts;
        bool ok_parse = false;
        for (auto ts_id : ts_ids) {
            ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), grammar, ts);
            if (ok_parse)
                r.push_back(ts);
            else
                throw std::runtime_error(string("Unable to parse ts_id '") + ts_id + "'");
        }
        return r;
    }
    
    /** @brief get current api version */
    string server::do_get_version_info(){
        return "1.0";
    }

    /** @brief create a new model with id */
    bool server::do_create_model(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "create_model: Model with name '" << mid << "' already exists.";
            throw runtime_error("dstm: model with specified name '"+mid+"' already exists, please remove it before (re)create");
        }
        model_map[mid]=make_context(model_state::idle, make_shared<stm_system>());
        slog << dlib::LINFO << "Successfully created model '" << mid << "'";
        return true;
    }

    /** @brief add existing model with id */
    bool server::do_add_model(string const& mid, stm_system_ mdl){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "add_model: Model with name '" << mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+mid+"' already exists, please remove it before (re)add");
        }
        model_map[mid]=make_context(model_state::idle, mdl);
        slog << dlib::LINFO << "Successfully added model '" << mid << "'";
        return true;
    }

    /** @brief remove (free up mem etc) model by id */
    bool server::do_remove_model(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "remove_model: No model with name '" << mid << "'";
            throw runtime_error("dstm: no model with specified name '"+mid +"'");
        }
        model_map.erase(mid);
        slog << dlib::LINFO << "Successfully removed model '" << mid << "'";
        return true;
    }
    

    /** @brief rename a model by id */
    bool server::do_rename_model(string old_mid, string new_mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(new_mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "rename_model: Model with name '" << new_mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+new_mid+"' already exists");
        }
        i=model_map.find(old_mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "rename_model: Unable to find model '" << old_mid << "'";
            throw runtime_error("dstm: not able to find model '"+old_mid+"'");
        }
        auto ctx_old = (*i).second;
        model_map.erase(old_mid);
        model_map[new_mid] = ctx_old;
        slog << dlib::LINFO << "Successfully renamed '" << old_mid << "' --> '" << new_mid << "'";
        return true;
    }

    /** @brief clone existing model with id */
    bool server::do_clone_model(string const& old_mid, string new_mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(new_mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "clone_model: Model with name '" << new_mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+new_mid+"' already exists");
        }
        i=model_map.find(old_mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "clone_model: Unable to find model '" << old_mid << "'";
            throw runtime_error("dstm: not able to find model '"+old_mid+"'");
        }
        auto old_mdl = (*i).second->mdl;
        auto new_mdl = stm_system::clone_stm_system(old_mdl);
        model_map[new_mid] = make_context(model_state::idle, new_mdl);
        // Check for attribute refs and rename.
        //rsv.level = apoint_ts("shyft://smg/...");
        //rsv.level = apoint_ts("dstm://Mold_mid/R1/A14") --> apoint_ts("dstm://Mnew_mid/...")
        slog << dlib::LINFO << "Successfully cloned model '" << old_mid << "' == '" << new_mid << "'";
        return true;
    }

    /** @brief get models, returns a string list with model identifiers */
    vector<string> server::do_get_model_ids() {
        vector<string> r;
        unique_lock<mutex> sl(srv_mx);
        for(auto e=model_map.begin();e!=model_map.end();++e)
            r.push_back(e->first);
        slog << dlib::LINFO << "Returning all model IDs";
        return r;
    }

    map<string, model_info> server::do_get_model_infos() {
        map<string, model_info> mis;
        unique_lock<mutex> sl(srv_mx);
        for(auto e=model_map.begin(); e != model_map.end(); ++e){
            auto mdl = e->second->mdl;
            auto key = e->first;
            mis[key] = model_info(mdl->id, mdl->name, no_utctime, mdl->json);
        }
        slog << dlib::LINFO << "Returning info for all models";
        return mis;
    }

    
    /** @brief get model with its context */
    stm_system_context_ server::do_get_context(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if (i == model_map.end()){
            slog << dlib::LERROR << "get_context: Unable to find model '" << mid << "'";
            throw runtime_error("dstm: not able to find model '" + mid + "'");
        }
        return i->second;
    }

    void server::do_set_state(string const& mid, model_state const& state){
        auto ctx = do_get_context(mid);
        //shared_lock sl(*((ctx->mtx_))); // Thread safety is here ensured by state attribute being atomic
        ctx->state = state;
        slog << dlib::LTRACE << "set_state: State of '" << mid << "' is now " << (int)state;
    }
    
    model_state server::do_get_state(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if (i == model_map.end()){
            slog << dlib::LERROR << "get_state: Unable to find model '" << mid << "'";
            throw runtime_error("dstm: not able to find model '" + mid + "'");
        }
        return i->second->state;
    }
    stm_system_ server::do_get_model(string const & mid){
        auto ctx = do_get_context(mid);
        return ctx->mdl;
    }
    
#ifdef SHYFT_WITH_SHOP
    /** @brief start SHOP optimization on a model
     *  returns whether the shop optimization was started or not.
     */
    bool server::do_optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd){
        auto ctx = do_get_context(mid);

        srv_upgradable_lock sl(ctx->mtx);
        auto shop = std::make_unique<shop_system>(ta, ctx->mdl.get(), "dstm://M" + mid + "/");
        // Set up signal handler:
        shop_segfault_handler sh;
        vector<shop_command> ncmd = cmd; ///< copy of shop commands, so we can prepend log files.
        // Check if ctx has any unbound series. If so, we shouldn't start an optimization.
        if (stm_system_needs_bind(*(ctx->mdl))){
            slog << dlib::LERROR << "optimize('" << mid << "'): Cannot run optimization on model with unbound attributes.\n"
                << "\tConsider evaluating model before running SHOP optimization.";
            return false;
        }
        if (ctx->state == model_state::optimizing) {
            slog << dlib::LWARN << "optimize: Optimization is already running on '" << mid << "'";
            return false;
        }
        /* scope a unique lock here to ensure proper modify access to local variables*/ { 
            srv_upgrade_lock  ul(sl);// pass in the upgradable lock here.
            ctx->logpos = 0;
            auto logfile = log_dir /(mid + ".log");
            // Clear logfile if present:
            if (fs::exists(logfile))
                fs::remove(logfile);
            // Preprocessing:
            ncmd.insert( ncmd.begin(), shop_command::log_file( logfile.string() ) );
            
            if (!setjmp(buf)) {
                // Emit to shop
                sh.shop_call = true; // If we get SIGSEGV from now on, use custom handling
                shop->set_logging_to_stdstreams(false);
                shop->set_logging_to_files(true);
                auto cmdl = std::const_pointer_cast<const stm_system>(ctx->mdl);
                shop->emit(*cmdl);
                sh.shop_call = false;
            } else {
                slog << dlib::LERROR << "optimize('" << mid << "'): SIGSEGV signal received while emitting model to shop.";
                return false;
            }
            // Finally, we set the optimizing flag, to signify that the model should only be read_only from this point on.
            ctx->state = model_state::optimizing;
        }
        // We can now release the unique lock, keep the shared lock so that we keep model consistent(unchanged) while optimizing
        ctx->current_run = std::async(std::launch::async,
            [this, ctx, sl{std::move(sl)}, cmd = std::move(ncmd), shop{std::move(shop)}, sh{std::move(sh)}, mid]() mutable -> shop_flag {
                slog << dlib::LINFO << "Starting optimization on '" << mid << "'";
                if (!setjmp(buf)) {
                    sh.shop_call = true;
                    try {
                        shop->command(cmd);// could segfault, then we drop to else part, or throw, then current_run keep the exception
                    } catch(std::exception const&e) {
                        slog<<dlib::LWARN<<"optimize('"<<mid<<"): threw exception "<<e.what();
                        ctx->state=model_state::idle;// important .. put back to idle
                        sh.shop_call=false;// and this as well.
                        return ctx->shop_result=shop_flag::other;
                    }
                    // Optimization is now done.
                    slog << dlib::LINFO << "optimize('" << mid << "'): SHOP run completed, upgrade to write-lock on model.";
                    
                    // We now upgrade the lock-type
                    srv_upgrade_lock  ul(sl);
                    slog << dlib::LINFO << "optimize('" << mid << "'): SHOP run completed, collecting results.";
                    try {
                        shop->collect(*(ctx->mdl));// note.. this can also segfault (jump to else part), or throw
                    }catch(std::exception const&e) {
                        slog<<dlib::LWARN<<"optimize('"<<mid<<"): threw exception while reading result "<< e.what();
                        ctx->state=model_state::idle;// important .. put back to idle
                        sh.shop_call=false;// and this as well.
                        // Before returning, we notify changes based on what's been reported to shop's visitor:
                        return ctx->shop_result=shop_flag::other;
                    }
                    shop->set_logging_to_files(false);// do we dare to do this in the exception sections?
                    sh.shop_call = false;
                    ctx->state = model_state::idle;
                    slog << dlib::LINFO << "optimize('" << mid << "'): completed.";
                    shop->vis->notify_changes(*sm);
                    return ctx->shop_result=shop_flag::success;
                } else {
                    // We received a SIGSEGV from shop.
                    slog << dlib::LERROR << "optimize('" << mid << "'): Received SIGSEGV signal while executing shop.";
                    sh.shop_call=false;
                    ctx->state=model_state::idle;// important .. put back to idle
                    return ctx->shop_result= shop_flag::segfault;
                }
            }
        );
        return true; // To signify that optimization has started correctly.
    }       
#endif
    //------------------------------------------
    // HELPER FUNCTIONS FOR evaluate_stm_system
    //------------------------------------------
    // Default case: Do nothing
    template<typename T, typename V, typename A, A a, typename C>
    void add_id_to_tsv(const stm_attr_t<T, V, A, a, C>& pa, ts_vector_t& tsv) {
        return;
    }
    // In the case of time series:
    template<typename T, typename A, A a, typename C>
    void  add_id_to_tsv(const stm_attr_t<T, apoint_ts, A, a, C>& pa, ts_vector_t& tsv) {
        if (pa.exists()) {
            auto ts = pa.get();
            if (ts.needs_bind())
                tsv.emplace_back(ts);
        }
    }
    
    template <typename C, typename E, E e>
    void proxy_attr_needs_bind(C const& r, ts_vector_t& tsv) noexcept {
        add_id_to_tsv(r.*proxy_attr_m<C,E,e>::member(), tsv);
    }
    
    template<typename C, typename E, int...a>
    constexpr std::array<void (*)(C const&, ts_vector_t&), sizeof...(a)>
    proxy_attr_bind_table(std::integer_sequence<int, a...>) {
        return {{&proxy_attr_needs_bind<C, E, static_cast<E>(a)>...}};
    }
    
    template<typename C, typename E, int...a>
    void add_unbound_for_component(std::integer_sequence<int,a...>, C const& r, ts_vector_t& tsv) {
        auto temporary = {(proxy_attr_needs_bind<C, E, static_cast<E>(a)>(r, tsv),0)...};
    }
    
    template<typename D, typename C>
    void add_unbound_for_vector(vector<shared_ptr<C>> const& comps, ts_vector_t& tsv) {
        using attr_seq = typename D::e_attr_seq_t;
        for (auto const& c : comps) {
            add_unbound_for_component<D, typename D::e_attr>(attr_seq{}, *std::dynamic_pointer_cast<D>(c), tsv);
        }
    }
    
    // Needs to be specialized for waterways:
    template<>
    void add_unbound_for_vector<waterway, shyft::energy_market::hydro_power::waterway>(vector<shared_ptr<shyft::energy_market::hydro_power::waterway>> const& wtrs, ts_vector_t& tsv) {
        using attr_seq = waterway::e_attr_seq_t;
        for (auto const& wtr : wtrs) {
            add_unbound_for_component<waterway, waterway::e_attr>(attr_seq{}, *std::dynamic_pointer_cast<waterway>(wtr), tsv);
            add_unbound_for_vector<gate>(wtr->gates, tsv);
        }
    }
    
    void add_unbound_for_hps(const stm_hps& hps, ts_vector_t& tsv) {
        add_unbound_for_vector<reservoir>(hps.reservoirs, tsv);
        add_unbound_for_vector<unit>(hps.units, tsv);
        add_unbound_for_vector<power_plant>(hps.power_plants, tsv);
        add_unbound_for_vector<waterway>(hps.waterways, tsv);
        add_unbound_for_vector<catchment>(hps.catchments, tsv);
    }
    
    /** @brief Evaluate any unbound time series of a model
     */
    ts_vector_t server::evaluate_stm_system(stm_system const & mdl, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
        ts_vector_t tsv;
        for (auto & h: mdl.hps)
            add_unbound_for_hps(*h, tsv);
        add_unbound_for_vector<energy_market_area>(mdl.market, tsv);
        return dtss->do_evaluate_ts_vector(bind_period, tsv, use_ts_cached_read, update_ts_cache, clip_period);
    }
    
    /** @brief Check whether any attributes in an stm_system needs bind_period
     */
    bool server::stm_system_needs_bind(const stm_system& mdl) {
        ts_vector_t tsv;
        for (auto & h : mdl.hps)
            add_unbound_for_hps(*h, tsv);
        add_unbound_for_vector<energy_market_area>(mdl.market, tsv);
        return tsv.size() > 0;
    }
    
    bool server::do_evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
        auto ctx = do_get_context(mid);
        srv_unique_lock sl(ctx->mtx); // We are potentially rewriting attributes.
        auto tsv = evaluate_stm_system(*(ctx->mdl), bind_period, use_ts_cached_read, update_ts_cache, clip_period);
        slog << dlib::LINFO << "Evaluated model '" << mid << "'";
        return tsv.size() > 0;
    }
    
    /** @brief Get SHOP log for a model
        */
    string server::do_get_log(const string& mid) {
        auto ctx = do_get_context(mid);
        srv_shared_lock ul(ctx->mtx); // ok. shared to get the log, hmm. we should have atomic logpos.
        // Check if file exists:
        if (auto log_file = log_dir / (mid + ".log"); fs::exists(log_file)) {
            std::ifstream ifs(log_file.c_str(), std::ifstream::binary); // to silence ms c++ that does not yet impl. std stuff
            // Get fpos for end of file:
            ifs.seekg(0, ifs.end);
            auto logend = ifs.tellg();
            // Set position in stream
            ifs.seekg(ctx->logpos);
            // Initialize string:
            string s;
            s.resize(logend-ctx->logpos);
            ifs.read(&s[0], logend - ctx->logpos);
            // Update position in log before returning string:
            ctx->logpos = logend;
            return s;
        }
        slog << dlib::LWARN << "get_log: Unable to find log file for model '" << mid << "'";
        return "Unable to find log.";
    }
    
    bool server::do_fx(string mid, string action) {
            return fx_cb? fx_cb(mid,action):false;
    }
    
    /** @brief handle one client connection 
    *
    * Reads messages/requests from the clients,
    * - act and perform request,
    * - return response
    * for as long as the client keep the connection 
    * open.
    * 
    */
    void server::on_connect(
        std::istream & in,
        std::ostream & out,
        const std::string & foreign_ip,
        const std::string & local_ip,
        unsigned short foreign_port,
        unsigned short local_port,
        dlib::uint64 /*connection_id*/
    ) {

        using shyft::core::core_iarchive;
        using shyft::core::core_oarchive;
        try {
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try {
                    switch (msg_type) {
                        case message_type::VERSION_INFO: {
                            auto result=do_get_version_info();// get result
                            msg::write_type(message_type::VERSION_INFO,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::CREATE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            ia>>mid;
                            auto result=do_create_model(mid);// get result
                            msg::write_type(message_type::CREATE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::ADD_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            stm_system_ mdl;
                            ia>>mid>>mdl;
                            auto result=do_add_model(mid, mdl);// get result
                            msg::write_type(message_type::ADD_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::REMOVE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto result=do_remove_model(mid);// get result
                            msg::write_type(message_type::REMOVE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::RENAME_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid; 
                            ia>>old_mid>>new_mid;
                            auto result=do_rename_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::RENAME_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::CLONE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid;
                            ia>>old_mid>>new_mid;
                            auto result=do_clone_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::CLONE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL_IDS: {
                            auto result=do_get_model_ids();// get result
                            msg::write_type(message_type::GET_MODEL_IDS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL_INFOS: {
                            auto result=do_get_model_infos();// get result
                            msg::write_type(message_type::GET_MODEL_INFOS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto ctx=do_get_context(mid);
                            /* scoped lock for the ctx->mtx_ protecting the model, so it's read-only while streaming  */{
                                boost::shared_lock<srv_shared_mutex> sl(ctx->mtx);
                                msg::write_type(message_type::GET_MODEL, out);// then send
                                core_oarchive oa(out, core_arch_flags);
                                oa<<ctx->mdl;// this is safe, now it's read-only and consistent while we stream it back
                            }
                        } break;

    #ifdef SHYFT_WITH_SHOP
                        case message_type::OPTIMIZE: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            generic_dt ta;
                            vector<shop_command> cmd;
                            ia >> mid >> ta >> cmd;
                            auto result = do_optimize(mid, ta, cmd);
                            msg::write_type(message_type::OPTIMIZE, out);// then send
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
    #endif
                        case message_type::GET_STATE: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ia >> mid;
                            auto result = do_get_state(mid);
                            msg::write_type(message_type::GET_STATE, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;

                        case message_type::GET_LOG: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ia >> mid;
                            auto result = do_get_log(mid);
                            msg::write_type(message_type::GET_LOG, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::FX: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid,fx_arg;
                            ia >> mid>>fx_arg;
                            auto result=do_fx(mid,fx_arg);
                            msg::write_type(message_type::FX,out);
                            core_oarchive oa(out, core_arch_flags);
                            oa<<result;
                        } break;
                        case message_type::EVALUATE_MODEL: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            bool use_cache, update_cache;
                            utcperiod bind_period, clip_period;
                            ia >> mid >> bind_period >> use_cache >> update_cache >> clip_period;
                            auto result = do_evaluate_model(mid, bind_period, use_cache, update_cache, clip_period);
                            msg::write_type(message_type::EVALUATE_MODEL, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        // other 
                        default:
                            throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string((int)msg_type));
                    }
                } catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        } catch(...) {
                // exit the loop and close connection
                slog<<dlib::LERROR<< "dstm-service: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
        }

    }
}
