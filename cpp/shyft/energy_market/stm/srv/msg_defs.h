#pragma once
#include <shyft/core/dlib_utils.h>

namespace shyft::energy_market::stm::srv {

	/** @brief dems message-types
	 *
	 * The message types used for the wire-communication of dems
	 *
	 */
	enum class message_type : uint8_t {
		SERVER_EXCEPTION,
		VERSION_INFO,
		CREATE_MODEL,
		ADD_MODEL,
		REMOVE_MODEL,
		RENAME_MODEL,
		CLONE_MODEL,
		GET_MODEL_IDS,
		GET_MODEL_INFOS,
		GET_MODEL,
		OPTIMIZE,
        GET_STATE,
        GET_LOG,
        FX, // execute server-side command fx(model_id,args)
        EVALUATE_MODEL, // bind any unbound attributes of a model.
		//SET_INPUT,
		//GET_INPUT,
		//START_OPTIMIZATION,
		//GET_RESULTS,
	};

	/** @brief adapt low-level and message-type handling from the core/dblib_utils.h */
	using msg=shyft::core::msg_util<message_type>; 
}
