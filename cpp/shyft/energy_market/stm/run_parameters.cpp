#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

    string run_parameters::url(const string& prefix) {
            if (mdl) {
                return mdl->url(prefix);
            }
            std::stringstream ss;
            ss << prefix << "RP";
            return ss.str();
    }
}
