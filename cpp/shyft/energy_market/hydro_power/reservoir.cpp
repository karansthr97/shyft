/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>

namespace shyft::energy_market::hydro_power {

    using std::dynamic_pointer_cast;
            

    reservoir_ const& reservoir::input_from(reservoir_ const& me, waterway_ const& w) {
        connect(w->shared_from_this(), me->shared_from_this());
        return me; 
    }
    reservoir_ const& reservoir::output_to(reservoir_ const& me, waterway_ const& w, connection_role role) {
        connect(me->shared_from_this(), role, w->shared_from_this());
        return me;
    }
    reservoir_ reservoir::shared_from_this() const {
        auto hps=hps_();
        return hps?shared_from_me(this,hps->reservoirs):nullptr;
    }
    vector<unit_> reservoir::downstream_units() const {
        vector<unit_> downstream_units;
        // recursive helper lambda (needs to pass self as argument)
        auto collect_ds_units = [&downstream_units](auto&& self, const auto& ww) -> void {
            if (ww != nullptr)
                for (const auto& ds: ww->downstreams) {
                    auto ds_unit = dynamic_pointer_cast<unit>(ds.target_());
                    if (ds_unit) {
                        downstream_units.push_back(ds_unit);
                        continue;
                    }
                    auto ds_waterway = dynamic_pointer_cast <const waterway>(ds.target_());
                    if (ds_waterway)
                        self(self, ds_waterway);
                }
        };
        for(const auto& ds:downstreams)
            collect_ds_units(collect_ds_units, dynamic_pointer_cast<const waterway>(ds.target_()));

        return downstream_units;
    }

    vector<unit_> reservoir::upstream_units() const {
        vector<unit_> r;
        for(const auto &us:upstreams)
            upstream_units_closure(dynamic_pointer_cast<waterway>(us.target_()), r);
        return r;
    }

    vector<unit_> reservoir::upstream_units_closure(const waterway_c& w, vector<unit_>& r) const {
        if (w == nullptr || w->upstreams.size() == 0)
            return r;
        for(const auto& us: w->upstreams) {
            if (dynamic_pointer_cast<unit>(us.target_()))
                r.push_back(dynamic_pointer_cast<unit>(us.target_()));
            else if (dynamic_pointer_cast<waterway const>(us.target_()))
                upstream_units_closure(dynamic_pointer_cast<waterway const >(us.target_()), r);
        }
        return r;
    }

    bool reservoir::operator==(const reservoir&o)const {
        return id_base::operator==(o);
    }

}
