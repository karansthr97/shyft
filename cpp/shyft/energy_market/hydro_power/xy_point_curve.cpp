/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <algorithm>
#include <stdexcept>
#include <boost/format.hpp>

#include <shyft/energy_market/hydro_power/xy_point_curve.h>



namespace shyft::energy_market::hydro_power {

    using std::sort;
    using std::runtime_error;
    using std::transform;
    using std::min_element;
    using std::max_element;

    xy_point_curve::xy_point_curve(vector <point> const &points_a) : points(points_a) {
        sort(points.begin(), points.end());
    }

    xy_point_curve::xy_point_curve(const vector<double> &x, const vector<double> &y) {
        if (x.size() != y.size())
            throw runtime_error((boost::format("x points size %1% and y points size %2% lists must have same dimension") % x.size() % y.size()).str());
        points.reserve(x.size());
        for (size_t i = 0; i < x.size(); ++i)
            points.emplace_back(x[i], y[i]);
        sort(points.begin(), points.end());
    }

    bool xy_point_curve::is_xy_mono_increasing() const {
        if (points.size() < 2)
            return false;
        for (size_t i = 1; i < points.size(); i++) {
            if (points[i - 1].x >= points[i].x || points[i - 1].y >= points[i].y)
                return false;
        }
        return true;
    }

    double xy_point_curve::calculate_y(double from_x) const {
        if (points.size() == 0)
            return std::numeric_limits<double>::quiet_NaN();
        if (points.size() == 1)
            return points[0].y;
        auto r = lower_bound(points.cbegin(), points.cend(), from_x, [] (const point &pt, double x) -> bool { return pt.x < x; });
        int ix = static_cast<int>(r - points.cbegin()) - 1;
        if (ix < 0) { //below lowest point ?
            ix = 0;// extrapolate downwards
        }
        if (ix + 1 == points.size()) {// above highest point
            ix = ix - 1;//extrapolate upwards.
        }
        // here we got two points on a linear curve we are going to use:
        auto a = (points[ix + 1].y - points[ix].y) / (points[ix + 1].x - points[ix].x);
        auto b = points[ix].y - a * points[ix].x;
        return a * from_x + b;
    }

    double xy_point_curve::calculate_x(double from_y) const {
        if (points.size() == 0)
            return std::numeric_limits<double>::quiet_NaN();
        if (points.size() == 1)
            return points[0].x;
        auto r = lower_bound(points.cbegin(), points.cend(), from_y, [] (const point &pt, double y) -> bool { return pt.y < y; });
        int ix = static_cast<int>(r - points.cbegin()) - 1;
        if (ix < 0) { //below lowest point ?
            ix = 0;// extrapolate downwards
        }
        if (ix + 1 == points.size()) {// above highest point
            ix = ix - 1;//extrapolate upwards.
        }
        // here we got two points on a linear curve we are going to use:
        auto a = (points[ix + 1].x - points[ix].x) / (points[ix + 1].y - points[ix].y);
        auto b = points[ix].x - a * points[ix].y;
        return a * from_y + b;
    }

    // Functions for determining min and max of curves are implement as free functions. The reason for this is that
    // the data structure for the R^2 -> R mapping is a std::vector and this bars implementation as class functions.

    auto compare_x = [] (const auto& a, const auto& b) -> bool {return a.x < b.x; };
    auto compare_y = [] (const auto& a, const auto& b) -> bool {return a.y < b.y; };
    auto compare_z = [] (const auto& a, const auto& b) -> bool {return a.z < b.z; };

    auto NaN = std::numeric_limits<double>::quiet_NaN();

    // Return nan for empty tables. Using std::optional or throwing error could be better choices here.
    double x_min(const xy_point_curve& xy) { return xy.points.size() ? min_element(xy.points.begin(), xy.points.end(), compare_x)->x : NaN; }
    double x_max(const xy_point_curve& xy) { return xy.points.size() ? max_element(xy.points.begin(), xy.points.end(), compare_x)->x : NaN; }
    double y_min(const xy_point_curve& xy) { return xy.points.size() ? min_element(xy.points.begin(), xy.points.end(), compare_y)->y : NaN; }
    double y_max(const xy_point_curve& xy) { return xy.points.size() ? max_element(xy.points.begin(), xy.points.end(), compare_y)->y : NaN; }

    double x_min(const xy_point_curve_with_z& xyz) { return x_min(xyz.xy_curve); };
    double x_max(const xy_point_curve_with_z& xyz) { return x_max(xyz.xy_curve); };
    double y_min(const xy_point_curve_with_z& xyz) { return y_min(xyz.xy_curve); };
    double y_max(const xy_point_curve_with_z& xyz) { return y_max(xyz.xy_curve); };

    double x_min(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> x;
        transform(xyz.begin(), xyz.end(), back_inserter(x), [] (const auto& xy) -> double { return x_min(xy); });
        return x.size() ? *min_element(x.begin(), x.end()) : NaN;
    }

    double x_max(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> x;
        transform(xyz.begin(), xyz.end(), back_inserter(x), [] (const auto& xy) -> double { return x_max(xy); });
        return x.size() ? *max_element(x.begin(), x.end()) : NaN;
    }

    double y_min(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> y;
        transform(xyz.begin(), xyz.end(), back_inserter(y), [] (const auto& xy) -> double { return y_min(xy); });
        return y.size() ? *min_element(y.begin(), y.end()) : NaN;
    }

    double y_max(const vector<xy_point_curve_with_z>& xyz) {
        vector<double> y;
        transform(xyz.begin(), xyz.end(), back_inserter(y), [] (const auto& xy) -> double { return y_max(xy); });
        return y.size() ? *max_element(y.begin(), y.end()) : NaN;
    }

    double z_min(const vector<xy_point_curve_with_z>& xyz) {
        return xyz.size() ? min_element(xyz.begin(), xyz.end(), compare_z)->z : NaN;
    }

    double z_max(const vector<xy_point_curve_with_z>& xyz) {
        return xyz.size() ? max_element(xyz.begin(), xyz.end(), compare_z)->z : NaN;
    }

}
