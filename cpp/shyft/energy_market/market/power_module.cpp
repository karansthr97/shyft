/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/energy_market/market/power_module.h>

namespace shyft::energy_market::market {

    bool power_module::equal_structure(const power_module& b) const {
        const power_module& a = *this;
        return id_base::operator==(b);
    }

}
