/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#if defined(_WINDOWS)
#pragma warning (disable : 4267)
#pragma warning (disable : 4244)
#pragma warning (disable : 4503)
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Winsock2.h> // WSACleanup
#include <Windows.h>

#endif
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/multi_array.hpp>
#include <shyft/core/core_serialization.h>

// experiment with python doc standard macro helpers
#define doc_intro(intro) intro  "\n"
#define doc_attributes() "\nAttributes:"
#define doc_attribute(name_str,type_str,descr_str) "\n    " name_str  " ( "  type_str  "):  "  descr_str  "\n"
#define doc_parameters() "\nArgs:"
#define doc_parameter(name_str,type_str,descr_str) "\n    " name_str  " ( "  type_str  "):  "  descr_str  "\n"
#define doc_paramcont(doc_str) "    "  doc_str  "\n"
#define doc_returns(name_str,type_str,descr_str) "\nReturns:\n"  type_str  ": " name_str "." descr_str "\n"
#define doc_notes() "\nNote:"
#define doc_note(note_str) "    " note_str  "\n"
#define doc_see_also(ref) "\n.. _see also:\n    " ref  "\n"
#define doc_reference(ref_name,ref) "\n.. _" ref_name ":\n    " ref "\n"
#define doc_ind(doc_str) "    "  doc_str
#define doc_raises() "\nRaises:"
#define doc_raise(type_str,descr_str) "\n    " type_str ": " descr_str  
