#include <shyft/py/energy_market/api_utils.h>
#include <shyft/py/energy_market/py_model_client_server.h>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/water_route.h>
#include <shyft/energy_market/hydro_power/power_station.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/srv/db.h>
#include <shyft/energy_market/srv/run.h>

namespace shyft::energy_market::srv {
	template<>
	struct receive_patch<shyft::energy_market::market::model> {
		static void apply(std::shared_ptr<shyft::energy_market::market::model> const& m) {
			//-- now we have to patch into place all
			//   weak-refs that we use.
			//   we *could* serialize the weak-refs, but
			//   this is tricky (and does not work to well on boost 1.63)
			//   so a practical approach is just to make fixes here
			for (auto& pl : m->power_lines) pl->mdl = m;
			for (auto& ak : m->area) {
				ak.second->mdl = m;
				for (auto& pk : ak.second->power_modules) pk.second->area = ak.second;
				if (auto const& hps = ak.second->detailed_hydro) {
					hps->mdl_area = ak.second;
					for (auto const& res : hps->reservoirs) res->hps = hps;
					for (auto const& agg : hps->units) agg->hps = hps;
					for (auto const& wr : hps->waterways) wr->hps = hps;
					for (auto const& ps : hps->power_plants) ps->hps = hps;
					for (auto const& c : hps->catchments) c->hps = hps;
				}
			}
		}
	};
}

namespace expose {
    namespace py=boost::python;
    using std::string;
    using shyft::core::utctime;
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::srv::run;
    using shyft::energy_market::srv::run_state;
    using std::vector;
    using std::shared_ptr;
    
    void ex_model_info () {
            using py::self;
            py::class_<model_info>("ModelInfo",
                doc_intro("Provides model-information useful for selection and filtering")
            )
            .def(py::init<int64_t,string const&,utctime,string>(
                (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("created"),py::arg("json")=string{""}))
            )
            .def_readwrite("id",&model_info::id,"the unique model id, can be used to retrieve the real model")
            .def_readwrite("name",&model_info::name,"any useful name or description")
            .def_readwrite("created",&model_info::created,"the time of creation, or last modification of the model")
            .def_readwrite("json",&model_info::json,"a json formatted string to enable scripting and python to store more information")
            .def(self==self)
            .def(self!=self)
            ;
            using ModelInfoVector=vector<model_info>;
            py::class_<ModelInfoVector>("ModelInfoVector", "A strongly typed list, vector, of ModelInfo")
            .def(py::vector_indexing_suite<ModelInfoVector, true>())
            ;
    }
    void ex_run () {
            using py::self;
            py::class_<run,py::bases<>,shared_ptr<run>>("Run",
                    doc_intro("Provides a Run concept, goes through states, created->prepinput->running->collect_result->frozen")
                )
                .def(py::init<int64_t,string const&,utctime,py::optional<string,int64_t>>(
                    (py::arg("self"),py::arg("id"),py::arg("name"),py::arg("created"),py::arg("json")=string{""},py::arg("mid")=0),
                    doc_intro("create a run")
                    )
                )
                .def_readwrite("id",&run::id,"the unique model id, can be used to retrieve the real model")
                .def_readwrite("name",&run::name,"any useful name or description")
                .def_readwrite("created",&run::created,"the time of creation, or last modification of the model")
                .def_readwrite("json",&run::json,"a json formatted string to enable scripting and python to store more information")
                .def_readwrite("mid",&run::mid,"model id (attached) for this run")
                .def_readwrite("state",&run::state,"the current observed state for the run, like created, running,finished_run etc")
                .def(self==self)
                .def(self!=self)
            ;
         
            using RunVector=vector<shared_ptr<run>>;
            py::class_<RunVector>("RunVector", "A strongly typed list, vector, of Run")
                .def(py::vector_indexing_suite<RunVector, true>())
            ;
            py::enum_<run_state>("run_state",
                doc_intro("Describes the possible state of the run")
                )
                .value("R_CREATED",run_state::created)
                .value("R_PREP_INPUT",run_state::prepare_input)
                .value("R_RUNNING",run_state::running)
                .value("R_FINISHED_RUN",run_state::finished_run)
                .value("R_READ_RESULT",run_state::reading_results)
                .value("R_FROZEN",run_state::frozed)
                .value("R_FAILED",run_state::failed)
                .export_values()
            ;

        
    }
    
    
 

  
    void ex_client_server() {
        using shyft::energy_market::market::model;
        using shyft::energy_market::srv::run;
        ex_model_info();
        
        shyft::py::energy_market::expose_client<model>("Client",
            doc_intro("The client-api for the energy_market")
        );
        shyft::py::energy_market::expose_server<shyft::energy_market::srv::db<model>>("Server",
            doc_intro("The server-side component for the skeleton energy_market model repository")
        );
  
        ex_run();
 
        shyft::py::energy_market::expose_client<run>("RunClient",
            doc_intro("The client-api for the generic run-repository")
        );
        shyft::py::energy_market::expose_server<shyft::energy_market::srv::db<run>>("RunServer",
            doc_intro("The server-side component for the skeleton generic run repository")
        );
    }
    
}
