# ensure to have python last, since it could contain ref to another boost version
include_directories(
  ${PROJECT_SOURCE_DIR}/cpp 
  ${SHYFT_DEPENDENCIES}/include
  ${python_include} 
  ${python_numpy_include}
)

set(py_ltm  "ltm" )
set(cpps 
    ltm_api.cpp
)

add_library(${py_ltm} SHARED ${cpps})
    set_target_properties(${py_ltm} PROPERTIES
        OUTPUT_NAME ${py_ltm} VISIBILITY_INLINES_HIDDEN TRUE
        PREFIX "_" # Python extensions do not use the 'lib' prefix
        INSTALL_RPATH "$ORIGIN/../../lib")
    if(MSVC)
        set_target_properties(${py_ltm} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    elseif(APPLE)
        set_target_properties(${py_ltm} PROPERTIES SUFFIX ".so")
    endif()
    target_link_libraries(${py_ltm} em_model_core ${boost_py_link_libraries})
    set_property(TARGET ${py_ltm} APPEND PROPERTY COMPILE_DEFINITIONS  BOOST_BIND_GLOBAL_PLACEHOLDERS) # silence boost.py warnin
install(TARGETS ${py_ltm} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/energy_market/ltm)

