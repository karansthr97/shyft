#pragma once
#include <vector>
#include <future>
#include <thread>
#include <shyft/time_series/dd/gpoint_ts.h>

namespace shyft::time_series::dd {
/** @brief Compute a ts-vector multi-threaded
 *
 *@detail Given a vector of expressions, deflate(evaluate) the expressions and return the
* equivalent concrete point-time-series of the expressions in the
* preferred destination type Ts
* Useful for the dtss,
* evaluates the expressions in parallell
*/
template <class Ts,class TsV>
vector<Ts>
deflate_ts_vector(TsV &&tsv1) {
    vector<Ts> tsv2(tsv1.size());

    auto deflate_range=[&tsv1,&tsv2](size_t i0,size_t n) {
        eval_ctx c;
        // first register ref.count for all expressions
        for(size_t i=i0;i<i0+n;++i)
            if (tsv1[i].ts) tsv1[i].ts->prepare(c);
        // secondly, do the work
        for(size_t i=i0;i<i0+n;++i) {
            auto gts = dynamic_pointer_cast<const gpoint_ts>(c.evaluate(tsv1[i].ts));
            tsv2[i]= Ts(gts->time_axis(),gts->rep.v,gts->point_interpretation());
        }
    };
    auto n_threads =  thread::hardware_concurrency();
    if(n_threads <2) n_threads=2;// hard coded minimum
    vector<std::future<void>> calcs;
    size_t ps= 1 + tsv1.size()/n_threads;
    for (size_t p = 0;p < tsv1.size(); ) {
        size_t np = p + ps <= tsv1.size() ? ps : tsv1.size() - p;
        calcs.push_back(std::async(std::launch::async, deflate_range, p, np));
        p += np;
    }
    for (auto &f : calcs) f.get();
    return tsv2;
}
}
