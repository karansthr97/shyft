
#include <shyft/hydrology/srv/server.h>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/core/fs_compat.h>
#include <shyft/hydrology/api/api_statistics.h>

namespace shyft::hydrology::srv {
using shyft::core::core_iarchive;
using shyft::core::core_oarchive;

using std::ifstream;
using std::ofstream;

namespace /* detail with no symbols out of this file/scope  */  {
    
    /** helper class to multiplex in cellstate to a model, using boost::apply_visitor  */
    struct set_state_visitor:boost::static_visitor<bool> {
        state_variant_t const& s;
        
        explicit set_state_visitor(state_variant_t const&s):s(s){}
        
        /** helper to do the apply state based on types.*/
        template <class S, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
        bool apply(M const& m) const {
            if(s.type()== typeid(shared_ptr<vector<cell_state_with_id<S>>>)) {
                shyft::api::cids_t cids;
                shyft::api::state_io_handler<typename M::element_type::cell_t> cs_handler(m->get_cells());
                cs_handler.apply_state(boost::get< shared_ptr<vector<cell_state_with_id<S>>> >(s),cids);
                return true;//TODO: possible to check results?
            }
            throw runtime_error("Illegal:missmatch between state type and model type. E.g.: pt_gs_k state must match pt_gs_k models");
            
        }
        
        //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
        
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::state_t>(m);}
        
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::state_t>(m);}

        bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::state_t>(m);}

        bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::state_t>(m);}

        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::state_t>(m);}

        bool operator()(shared_ptr<region_model<pt_st_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::state_t>(m);}

        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::state_t>(m);}
        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::state_t>(m);}
    };

    /** helper class to multiplex in parameter to a model, using boost::apply_visitor TODO: consider binary visitor instead! */
    struct set_region_model_parameter_visitor:boost::static_visitor<bool> {
        parameter_variant_t const& p;
        
        explicit set_region_model_parameter_visitor(parameter_variant_t const&p):p(p){}
        
        /** helper to do the apply state based on types.*/
        template <class P, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
        bool apply(M const& m) const {
            if(p.type()== typeid(shared_ptr<P>)) {
                m->set_region_parameter(*boost::get< shared_ptr<P> >(p));
                return true;//TODO: possible to check results?
            }
            throw runtime_error("Illegal:missmatch between region parameter type and model type. E.g.: pt_gs_k parameter must match pt_gs_k models");
        }
        
        //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
        
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
        
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
        
        bool operator()(shared_ptr<region_model<pt_st_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::parameter>(m);}
        
    };

    /** helper class to multiplex in parameter to a model, using boost::apply_visitor TODO: consider binary visitor instead! */
    struct set_catchment_model_parameter_visitor:boost::static_visitor<bool> {
        parameter_variant_t const& p;
        size_t cid;
        set_catchment_model_parameter_visitor(parameter_variant_t const&p,size_t cid):p{p},cid{cid}{}
        
        /** helper to do the apply state based on types.*/
        template <class P, class M> // M like pt_gs_k::cell_complete_response_t, S like pt_gs_k::state_t
        bool apply(M const& m) const {
            if(p.type()== typeid(shared_ptr<P>)) {
                m->set_catchment_parameter(cid,*boost::get< shared_ptr<P> >(p));
                return true;//TODO: possible to check results?
            }
            throw runtime_error("Illegal:missmatch between region parameter type and model type. E.g.: pt_gs_k parameter must match pt_gs_k models");
        }
        
        //  using boost variant visitor on the model type, we get one of the below methods called, that dispatches to the above template impl */
        
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_gs_k::parameter>(m);}
        
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_ss_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hs_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_hps_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pm_gs_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<pt_st_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<pt_st_k::parameter>(m);}

        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::parameter>(m);}
        bool operator()(shared_ptr<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>> const& m) const { return apply<r_pt_gs_k::parameter>(m);}
        
    };
    

} // end detail
    
/** @brief a server for serializable (parts of) hydrology forecasting models,
*
* Currently using dlib server_iostream, 
*/

/** start the server in background, return the listening port used in case it was set unspecified */
int server::start_server() {
    if(get_listening_port()==0) {
        start_async();
        while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
            std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
    } else {
        start_async();
    }
    return get_listening_port();
}

string server::do_get_version_info(){
    return "1.1";
}

/**  create a model based on geo_cell_data,  default parameter of specified type
    * TODO: consider using table-driven meta-programming approach to minimize maintenance
    */

model_variant_t server::make_shared_model_of_type(rmodel_type &mtype,vector <core::geo_cell_data> const& gcd) {
    model_variant_t r;
    if(mtype == rmodel_type::pt_gs_k_opt) {
        pt_gs_k::parameter p;
        r= make_shared<region_model<pt_gs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::pt_gs_k) {
        pt_gs_k::parameter p;
        r= make_shared<region_model<pt_gs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype == rmodel_type::pt_ss_k) {
        pt_ss_k::parameter p;
        r= make_shared<region_model<pt_ss_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::pt_ss_k_opt) {
        pt_ss_k::parameter p;
        r= make_shared<region_model<pt_ss_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype == rmodel_type::pt_st_k) {
        pt_st_k::parameter p;
        r= make_shared<region_model<pt_st_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::pt_st_k_opt) {
        pt_st_k::parameter p;
        r= make_shared<region_model<pt_st_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype == rmodel_type::pt_hs_k) {
        pt_hs_k::parameter p;
        r= make_shared<region_model<pt_hs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::pt_hs_k_opt) {
        pt_hs_k::parameter p;
        r= make_shared<region_model<pt_hs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype ==rmodel_type::pt_hps_k) {
        pt_hps_k::parameter p;
        r= make_shared<region_model<pt_hps_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::pt_hps_k_opt) {
        pt_hps_k::parameter p;
        r= make_shared<region_model<pt_hps_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype == rmodel_type::r_pm_gs_k) {
        r_pm_gs_k::parameter p;
        r= make_shared<region_model<r_pm_gs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::r_pm_gs_k_opt) {
        r_pm_gs_k::parameter p;
        r= make_shared<region_model<r_pm_gs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else if(mtype == rmodel_type::r_pt_gs_k) {
        r_pt_gs_k::parameter p;
        r= make_shared<region_model<r_pt_gs_k::cell_complete_response_t,a_region_environment>>(gcd,p);
    } else if ( mtype == rmodel_type::r_pt_gs_k_opt) {
        r_pt_gs_k::parameter p;
        r= make_shared<region_model<r_pt_gs_k::cell_discharge_response_t,a_region_environment>>(gcd,p);
    } else {
        throw runtime_error("currently not supported model type");
    }
    
    return r;
}

/** @brief creates a new model, with model-id and specified type */
bool server::do_create_model(string const& mid, 
                        rmodel_type mtype, 
                        vector <core::geo_cell_data> const& gcd){
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(mid);
    if(i!=model_map.end()) {
        throw runtime_error("drms:model with specified name'"+ mid+"' already exists, please remove it before (re)create");
    }

    model_map[mid] = make_shared_model_of_type(mtype,gcd);//<ptgsk_model_t>(gcd, rp);
    mx_map[mid]=make_shared<mutex>();// also create the mutex that we use to control one at a time access to the model
    return true;
}

/** @brief remove (free up mem etc) of region-model  model-id */
bool server::do_remove_model(string const& mid){
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(mid);
    if(i==model_map.end()) {
        throw runtime_error("drms: remove, -  no model with specified name'"+ mid+ "'");
    }
    model_map.erase(mid);
    mx_map.erase(mid);
    return true;
}

/** @brief get models, returns a string list with model identifiers */
vector<string> server::do_get_model_ids() {
    vector<string> r;
    unique_lock<mutex> sl(srv_mx);
    for(auto e=model_map.begin();e!=model_map.end();++e)
        r.push_back(e->first);
    return r;
}


/** @brief given model id, safely get a shared_ptr to that */
model_variant_t server::get_model(string mid) {
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(mid);
    if(i!=model_map.end())
        return (*i).second;
    else
        throw runtime_error("drms: not able to find model "+ mid);
}

/** @brief returns a shared_ptr to the mutex for model id */
shared_ptr<mutex> server::get_model_mx(string mid) {
    unique_lock<mutex> sl(srv_mx);
    auto i=mx_map.find(mid);
    if(i!=mx_map.end())
        return (*i).second;
    else
        throw runtime_error("drms:not able to find mx for model "+ mid);
}

/** @brief rename a model */
bool server::do_rename_model(string old_mid, string new_mid){
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(new_mid);
    if(i!=model_map.end()) {
        throw runtime_error("drms:model with specified name'"+ new_mid+"' already exists");
    }
    i=model_map.find(old_mid);
    if(i==model_map.end())
        throw runtime_error("drms: not able to find model "+ old_mid);
    auto m_old = (*i).second;
    shared_ptr<mutex> mx = mx_map[old_mid];
    model_map.erase(old_mid);
    mx_map.erase(old_mid);
    model_map[new_mid] = m_old;
    mx_map[new_mid] = mx;
    return true;
}

bool server::do_set_state(string const& mid, state_variant_t const& csv){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor(set_state_visitor{csv},mv);
}

state_variant_t server::do_get_state(string const& mid, shyft::api::cids_t& cids){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    state_variant_t state;
    boost::apply_visitor(
        [&state, &cids](const auto&m) -> void{
            auto cells= m->get_cells();
            shyft::api::state_io_handler<typename decltype(cells)::element_type::value_type> cs_handler(cells);
            state = cs_handler.extract_state(cids);
        }
        ,mv);
    return state;
}

bool server::do_set_region_parameter(string const&mid, parameter_variant_t const&p) {
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor(set_region_model_parameter_visitor{p},mv);
}

bool server::do_set_catchment_parameter(string const&mid, parameter_variant_t const&p,size_t cid) {
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor(set_catchment_model_parameter_visitor{p,cid},mv);
}

bool server::do_run_interpolation(string const& mid, 
                                const shyft::core::interpolation_parameter& ip_parameter,
                                const shyft::time_axis::generic_dt& ta,
                                const shyft::api::a_region_environment& r_env,
                                bool best_effort){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([&ip_parameter,&ta,&r_env,best_effort](const auto&m) { return m->run_interpolation_g(ip_parameter, ta, r_env, best_effort);},mv);
}

bool server::do_run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps){
    unique_lock sl(*get_model_mx(mid));
    auto mv =get_model(mid);
    return boost::apply_visitor([use_ncore,start_step,n_steps](auto const&m)->bool {m->run_cells(use_ncore,start_step,n_steps);return true;},mv);//TODO: consider other retval handling
}

q_adjust_result server::do_adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step,double scale_range,double scale_eps,size_t max_iter,size_t n_steps) {
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);//size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1..)
    q_adjust_result q_result = boost::apply_visitor(
        [&indexes,wanted_q,start_step,scale_range,scale_eps,max_iter,n_steps]
        (auto const& m){
            return m->adjust_state_to_target_flow(wanted_q,indexes,start_step,scale_range,scale_eps,max_iter,n_steps);
        },
        mv
    );
    return q_result;
}

template <class Fx>
apoint_ts do_get_fx(server& srv, string const& mid, Fx&& fx){
    unique_lock sl(*srv.get_model_mx(mid));
    auto mv=srv.get_model(mid);
    return boost::apply_visitor(
            [&fx](auto const&m)->apoint_ts{
                return fx(m->get_cells());
            }
        ,mv);
}

template<class C>
static inline api::basic_cell_statistics<C> api_basic_cell_statistics(shared_ptr<vector<C>> const &cells) {
    return api::basic_cell_statistics<C>(cells);
}

apoint_ts server::do_get_discharge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).discharge(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_temperature(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).temperature(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_precipitation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).precipitation(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_snow_swe(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).snow_swe(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_charge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).charge(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_snow_sca(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).snow_sca(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_radiation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).radiation(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_wind_speed(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).wind_speed(cids, ix_type);
        }
        );
}

apoint_ts server::do_get_rel_hum(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type){
        return do_get_fx(*this,mid,[&cids,ix_type](auto const&cells) {
            return api_basic_cell_statistics(cells).rel_hum(cids, ix_type);
        }
        );
}

bool server::do_set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([&catchment_id_list](const auto&m) {m->set_catchment_calculation_filter(catchment_id_list); return true;},mv);
}

bool server::do_revert_state(string const& mid) {
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([](const auto&m) {m->revert_to_initial_state(); return true;},mv);
}

bool server::do_clone_model(string const& old_mid, string const& new_mid) {
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(new_mid);
    if(i!=model_map.end()) {
        throw runtime_error("drms:model with specified name'"+ new_mid+"' already exists");
    }
    i=model_map.find(old_mid);
    if(i==model_map.end())
        throw runtime_error("drms: not able to find model "+ old_mid);
    auto m_old = (*i).second;
    unique_lock<mutex> ml(*mx_map[old_mid]);
    return  boost::apply_visitor([&new_mid,this](const auto&m)->bool {
        model_variant_t model = m->clone_model();
        model_map[new_mid] = model;
        mx_map[new_mid] = make_shared<mutex>(); 
        return true;
    },m_old);
}

bool server::do_copy_model(string const& old_mid, string const& new_mid) {
    unique_lock<mutex> sl(srv_mx);
    auto i=model_map.find(new_mid);
    if(i!=model_map.end()) {
        throw runtime_error("drms:model with specified name'"+ new_mid+"' already exists");
    }
    i=model_map.find(old_mid);
    if(i==model_map.end())
        throw runtime_error("drms: not able to find model "+ old_mid);
    auto m_old = (*i).second;
    unique_lock<mutex> ml(*mx_map[old_mid]);
    return  boost::apply_visitor([&new_mid,this](const auto&m)->bool {
        model_variant_t model = m->copy_model();
        model_map[new_mid] = model;
        mx_map[new_mid] = make_shared<mutex>(); 
        return true;
    },m_old);
}

bool server::do_set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([&catchment_id, &on_or_off](const auto&m) {m->set_state_collection(catchment_id, on_or_off); return true;},mv);
}

bool server::do_set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([&catchment_id, &on_or_off](const auto&m) {m->set_snow_sca_swe_collection(catchment_id, on_or_off); return true;},mv);
}

bool server::do_is_cell_env_ts_ok(string const& mid){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([](const auto&m) {return m->is_cell_env_ts_ok();},mv);
}

bool server::do_set_initial_state(string const& mid) {
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([](const auto&m) { m->get_states(m->initial_state);return true;},mv);
}

bool server::do_is_calculated(string const& mid, size_t cid){
    unique_lock sl(*get_model_mx(mid));
    auto mv=get_model(mid);
    return boost::apply_visitor([&cid](const auto&m) {return m->is_calculated(cid);},mv);
}

template< class Fx>
static void do_fx(std::istream & in,
    std::ostream & out,
    message_type msgtype,Fx&&fx) {
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    core_iarchive ia(in,core_arch_flags);// create the stream
    string mid; shyft::api::cids_t indexes;// decl. variables to read(args to do_fx-call)
    shyft::core::stat_scope ix_type;
    ia>>mid>>indexes>>ix_type;
    auto result=fx(mid, indexes,ix_type);// get result
    msg::write_type(msgtype,out);// then send
    core_oarchive oa(out,core_arch_flags);
    oa<<result;
}
/**@brief handle one client connection 
*
* Reads messages/requests from the clients,
* - act and perform request,
* - return response
* for as long as the client keep the connection 
* open.
* 
*/
void server::on_connect(
    std::istream & in,
    std::ostream & out,
    const std::string & foreign_ip,
    const std::string & local_ip,
    unsigned short foreign_port,
    unsigned short local_port,
    dlib::uint64 /*connection_id*/
) {

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    try {
        while (in.peek() != EOF) {
            auto msg_type= msg::read_type(in);
            try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                    //TODO:: implement switch for each added message-type
                    // dispatch to this class do_method_impl(args)->return_type
                    
                    case message_type::VERSION_INFO: {
                        //core_iarchive ia(in,core_arch_flags);// create the stream
                        //int64_t mid;model_info mi;// decl. variables to read(args to do_fx-call)
                        //ia>>mid>>mi;
                        auto result=do_get_version_info();// get result
                        msg::write_type(message_type::VERSION_INFO,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::CREATE_MODEL: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; rmodel_type mtype; vector <core::geo_cell_data> gcd;// decl. variables to read(args to do_fx-call)
                        ia>>mid>>mtype>>gcd;
                        auto result=do_create_model(mid, mtype, gcd);// get result
                        msg::write_type(message_type::CREATE_MODEL,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::SET_STATE: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; state_variant_t csv;// decl. variables to read(args to do_fx-call)
                        ia>>mid>>csv;
                        auto result=do_set_state(mid, csv);// get result
                        msg::write_type(message_type::SET_STATE,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::GET_STATE: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; shyft::api::cids_t cids;
                        ia>>mid>>cids;
                        auto result=do_get_state(mid, cids);// get result
                        msg::write_type(message_type::GET_STATE,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::SET_INITIAL_STATE: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid;
                        ia>>mid;
                        auto result=do_set_initial_state(mid);// get result
                        msg::write_type(message_type::SET_INITIAL_STATE,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;

                    case message_type::SET_REGION_PARAMETER: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; parameter_variant_t pv;// decl. variables to read(args to do_fx-call)
                        ia>>mid>>pv;
                        auto result=do_set_region_parameter(mid, pv);// get result
                        msg::write_type(message_type::SET_REGION_PARAMETER,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;

                    case message_type::SET_CATCHMENT_PARAMETER: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; parameter_variant_t pv;int64_t cid;// decl. variables to read(args to do_fx-call)
                        ia>>mid>>pv>>cid;
                        auto result=do_set_catchment_parameter(mid, pv, size_t(cid));// get result
                        msg::write_type(message_type::SET_CATCHMENT_PARAMETER,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::RUN_INTERPOLATION: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; shyft::core::interpolation_parameter ip_parameter; shyft::time_axis::generic_dt ta; api::a_region_environment r_env; bool best_effort=true;// decl. variables to read(args to do_fx-call)
                        ia>>mid>>ip_parameter>>ta>>r_env>>best_effort;
                        auto result=do_run_interpolation(mid, ip_parameter, ta, r_env, best_effort);// get result
                        msg::write_type(message_type::RUN_INTERPOLATION,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::RUN_CELLS: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid;// decl. variables to read(args to do_fx-call)
                        int64_t use_ncore,start_step, n_steps;
                        ia>>mid>>use_ncore>>start_step>>n_steps;
                        auto result=do_run_cells(mid,size_t(use_ncore),start_step,n_steps);// get result
                        msg::write_type(message_type::RUN_CELLS,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::ADJUST_Q: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; shyft::api::cids_t indexes; double wanted_q;// decl. variables to read(args to do_fx-call)
                        int64_t start_step,max_iter,n_steps;
                        double scale_range,scale_eps;
                        //double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1
                        ia>>mid>>indexes>>wanted_q>>start_step>>scale_range>>scale_eps>>max_iter>>n_steps;
                        auto result=do_adjust_q(mid, indexes, wanted_q,size_t(start_step),scale_range,scale_eps,size_t(max_iter),size_t(n_steps));// get result
                        msg::write_type(message_type::ADJUST_Q,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::GET_DISCHARGE: {
                        do_fx(in,out,message_type::GET_DISCHARGE,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_discharge(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_TEMPERATURE: {
                        do_fx(in,out,message_type::GET_TEMPERATURE,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_temperature(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_PRECIPITATION: {
                        do_fx(in,out,message_type::GET_PRECIPITATION,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_precipitation(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_SNOW_SWE: {
                        do_fx(in,out,message_type::GET_SNOW_SWE,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_snow_swe(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_CHARGE: {
                        do_fx(in,out,message_type::GET_CHARGE,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_charge(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_SNOW_SCA: {
                        do_fx(in,out,message_type::GET_SNOW_SCA,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_snow_sca(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_RADIATION: {
                        do_fx(in,out,message_type::GET_RADIATION,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_radiation(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_WIND_SPEED: {
                        do_fx(in,out,message_type::GET_WIND_SPEED,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_wind_speed(mid,indexes,ix_type);
                            }
                        );
                    } break;
                    
                    case message_type::GET_REL_HUM: {
                        do_fx(in,out,message_type::GET_REL_HUM,[this](string mid,shyft::api::cids_t indexes,shyft::core::stat_scope ix_type) {
                                return do_get_rel_hum(mid,indexes,ix_type);
                            }
                        );
                    } break;                        
                    
                    case message_type::SET_CATCHMENT_CALCULATION_FILTER: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; 
                        std::vector<int64_t> catchment_id_list;
                        ia>>mid>>catchment_id_list;
                        auto result=do_set_catchment_calculation_filter(mid, catchment_id_list);// get result
                        msg::write_type(message_type::SET_CATCHMENT_CALCULATION_FILTER,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::REMOVE_MODEL: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; 
                        ia>>mid;
                        auto result=do_remove_model(mid);// get result
                        msg::write_type(message_type::REMOVE_MODEL,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::GET_MODEL_IDS: {
                        auto result=do_get_model_ids();// get result
                        msg::write_type(message_type::GET_MODEL_IDS,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::RENAME_MODEL: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string old_mid, new_mid; 
                        ia>>old_mid>>new_mid;
                        auto result=do_rename_model(old_mid, new_mid);// get result
                        msg::write_type(message_type::RENAME_MODEL,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::CLONE_MODEL: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string old_mid, new_mid; 
                        ia>>old_mid>>new_mid;
                        auto result=do_clone_model(old_mid, new_mid);// get result
                        msg::write_type(message_type::CLONE_MODEL,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::COPY_MODEL: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string old_mid, new_mid; 
                        ia>>old_mid>>new_mid;
                        auto result=do_copy_model(old_mid, new_mid);// get result
                        msg::write_type(message_type::COPY_MODEL,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::REVERT_TO_INITIAL_STATE: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; 
                        ia>>mid;
                        auto result=do_revert_state(mid);// get result
                        msg::write_type(message_type::REVERT_TO_INITIAL_STATE,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::SET_STATE_COLLECTION: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; int64_t catchment_id; bool on_or_off;
                        ia>>mid>>catchment_id>>on_or_off;
                        auto result=do_set_state_collection(mid, catchment_id, on_or_off);// get result
                        msg::write_type(message_type::SET_STATE_COLLECTION,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::SET_SNOW_SCA_SWE_COLLECTION: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; int64_t catchment_id; bool on_or_off;
                        ia>>mid>>catchment_id>>on_or_off;
                        auto result=do_set_snow_sca_swe_collection(mid, catchment_id, on_or_off);// get result
                        msg::write_type(message_type::SET_SNOW_SCA_SWE_COLLECTION,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::IS_CELL_ENV_TS_OK: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; 
                        ia>>mid;
                        auto result=do_is_cell_env_ts_ok(mid);// get result
                        msg::write_type(message_type::IS_CELL_ENV_TS_OK,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    case message_type::IS_CALCULATED: {
                        core_iarchive ia(in,core_arch_flags);// create the stream
                        string mid; size_t cid;
                        ia>>mid>>cid;
                        auto result=do_is_calculated(mid, cid);// get result
                        msg::write_type(message_type::IS_CALCULATED,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;
                    } break;
                    
                    // other 
                    default:
                        throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string((int)msg_type));
                }
            } catch (std::exception const& e) {
                msg::send_exception(e,out);
            }
        }
    } catch(...) { // when we reach here.. we are going to close down the socket
    // so we just log to std err, then return.
    // the dlib thread, will forcibly close the socket
    // but survive the server it self.
    std::cerr<< "drms: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
    }
}
}
