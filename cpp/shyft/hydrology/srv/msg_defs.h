/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <cstdint>
#include <exception>
#include <shyft/core/dlib_utils.h>

namespace shyft::hydrology::srv  {

    /** @brief drms message-types
     *
     * The message types used for the wire-communication of drms
     *
     */
    enum class message_type : uint8_t {
        SERVER_EXCEPTION,// mandatory, 
        VERSION_INFO,// practical since we plan to  to a lot of upgrades
        CREATE_MODEL,
        SET_STATE,
        GET_STATE,
        RUN_INTERPOLATION,
        RUN_CELLS,
        ADJUST_Q,
        GET_DISCHARGE,
        GET_TEMPERATURE,
        GET_PRECIPITATION,
        GET_SNOW_SWE,
        GET_CHARGE,
        GET_SNOW_SCA,
        GET_RADIATION,
        GET_WIND_SPEED,
        GET_REL_HUM,
        SET_REGION_PARAMETER,
        SET_CATCHMENT_PARAMETER,
        SET_CATCHMENT_CALCULATION_FILTER,
        GET_MODEL_IDS,
        REMOVE_MODEL,
        RENAME_MODEL,
        CLONE_MODEL,
        COPY_MODEL,
        REVERT_TO_INITIAL_STATE,
        SET_STATE_COLLECTION,
        SET_SNOW_SCA_SWE_COLLECTION,
        IS_CELL_ENV_TS_OK,
        IS_CALCULATED,
        SET_INITIAL_STATE,
        //-- new drms messages goes here
        //  like GET_TIME_AXIS etc.

    };
    
    /** @brief  adapt low-level and message-type handling from the core/dblib_utils.h */
     using msg=shyft::core::msg_util<message_type>; 
}
