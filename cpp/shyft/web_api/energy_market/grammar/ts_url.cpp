#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/srv/context.h>

namespace shyft::web_api::grammar {

    using shyft::time_series::dd::apoint_ts;
    using shyft::energy_market::core::proxy_attr_m;

    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::catchment;
    //using shyft::energy_market::stm::srv::srv_shared_lock;
    using shyft::energy_market::stm::stm_system;
    
    template<typename C, typename E, E e>
    proxy_attr_range proxy_attr_get(C *r) noexcept {
        return (r->*proxy_attr_m<C, E, e>::member()).get();
    }
    
    template<typename C, typename E, int...a>
    constexpr std::array<proxy_attr_range(*)(C *),sizeof...(a)>
    cmake_attr_get_table(std::integer_sequence<int,a...>){
        return {{&proxy_attr_get<C,E,static_cast<E>(a)>...}}; // Generates a list of function pointers:
    }

    constexpr auto rsv_get_table = cmake_attr_get_table<reservoir, reservoir::e_attr>(reservoir::e_attr_seq_t{});
    constexpr auto unit_get_table = cmake_attr_get_table<unit, unit::e_attr>(unit::e_attr_seq_t{});
    constexpr auto wtr_get_table = cmake_attr_get_table<waterway, waterway::e_attr>(waterway::e_attr_seq_t{});
    constexpr auto pp_get_table = cmake_attr_get_table<power_plant, power_plant::e_attr>(power_plant::e_attr_seq_t{});
    constexpr auto ctm_get_table = cmake_attr_get_table<catchment, catchment::e_attr>(catchment::e_attr_seq_t{});
    
    apoint_ts get_time_series(server *const srv, const string& mid, int hps_id, char comp_type, int comp_id, int attr_id) {
        if (!srv) {
            throw std::runtime_error("server must be set to successfully parse a ts_url");
        }
        // Get context and then model, 
        // notice: that we already do have a lock on the model, 
        //         since the code-path for this (currently) goes through web-api, or subscription.
        //         thus no model-lock required,
        auto ctx= srv->do_get_context(mid);// this will require a lock on srv, hard to avoid.
        // srv_shared_lock sl(ctx->mtx); // this one is not needed, we *should* already have, we will enforce that later.
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading

        // Get hps:
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw std::runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;
        // Get component:
        proxy_attr_range attr;
        if (comp_type == 'R') {
            auto comp = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(comp_id));
            attr = rsv_get_table[attr_id](comp.get());
        } else if (comp_type == 'U') {
            auto comp = std::dynamic_pointer_cast<unit>(hps->find_unit_by_id(comp_id));
            attr = unit_get_table[attr_id](comp.get());
        } else if (comp_type == 'W') {
            auto comp = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_id(comp_id));
            attr = wtr_get_table[attr_id](comp.get());
        } else if (comp_type == 'P') {
            auto comp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(comp_id));
            attr = pp_get_table[attr_id](comp.get());
        } else if (comp_type == 'C') {
            auto comp = std::dynamic_pointer_cast<catchment>(hps->find_catchment_by_id(comp_id));
            attr = ctm_get_table[attr_id](comp.get());
        } else {
            throw std::runtime_error(string("Invalid component type ") + comp_type + ". Valid options are R|U|W|P|C.");
        }
        // Get attribute and return
        try {
            return boost::get<apoint_ts>(attr);
        } catch (boost::bad_get e) {
            throw std::runtime_error("Error when parsing dstm ts_url. Attribute " + std::to_string(attr_id) + " of component type " + comp_type + " is not a time series.");
        }
        return apoint_ts();
    }
    
    template<typename Iterator, typename Skipper>
    dstm_ts_url_grammar<Iterator, Skipper>::dstm_ts_url_grammar(server *const srv):
    dstm_ts_url_grammar::base_type(ts_, "dstm_ts_url"), srv{srv} {
        mid_ = lexeme[+(char_- "/")];
        ts_ = (lit("dstm://M") > mid_ 
                > "/HPS" > int_ 
                > "/" > char_("RUWPCGM") > int_
                > "/A" >> int_ )[_val = phx::bind(get_time_series, srv, _1, _2, _3, _4, _5)];

        on_error<fail>(ts_, error_handler(_4, _3, _2));
    }

    template struct dstm_ts_url_grammar<request_iterator_t, request_skipper_t>;
}
