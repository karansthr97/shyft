#pragma once
#ifdef _WIN32
// Boost asio uses Windows API and recommends setting target version
// See: boost\asio\detail\config.hpp
#include <sdkddkver.h> // Detect Windows SDK version
#endif
