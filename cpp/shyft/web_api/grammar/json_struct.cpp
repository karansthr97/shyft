#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/grammar.h>

namespace shyft::web_api::grammar {

    void insert_element(json& j, std::pair<string, value_type> kvp) {
        j.m.emplace(kvp.first, kvp.second);
    }

    template<typename Iterator, typename Skipper>
    json_grammar<Iterator, Skipper>::json_grammar():
    json_grammar::base_type(json_, "json") {
        json_list_ = lit('[') >> -(json_ % ',') >> lit(']');
        value_ = (int_ | integer_list_ | quoted_string_  | bool_ | period_ | time_axis_ |
            proxy_ | proxy_list_ | // Proxy- and proxy_list should also be late in the parser, as it contains for instance string.
			json_ | json_list_ ); // Json- and json-list have to be last in these conditionals to handle grammar ambiguities correctly.

        pair_ = (quoted_string_ >> ":" >> value_);

        json_ = (lit("{")
            >> -(pair_[phx::bind(insert_element, _val, _1)] % ",")
            >> lit("}") );

        json_list_.name("json list");
        value_.name("json value type");
        pair_.name("key-value pair");
        on_error<fail>(json_, error_handler(_4, _3, _2));
    }

    template struct json_grammar<request_iterator_t, request_skipper_t>;
}
