#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
    
        
    template<typename Iterator,typename Skipper>
    time_points_grammar<Iterator,Skipper>::time_points_grammar():time_points_grammar::base_type(start,"time_points") {
            start =
			lit('[') > (t_ % lit(',') ) > lit(']')
            ;
            start.name("time_points");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

    template struct time_points_grammar<request_iterator_t,request_skipper_t>;

}
