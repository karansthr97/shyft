#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
        
    template<typename Iterator,typename Skipper>
    ats_vector_grammar<Iterator,Skipper>::ats_vector_grammar():ats_vector_grammar::base_type(start,"ats_vector") {

            start =
            lit('[')> -( ts_ %',')> lit(']')
            ;
            start.name("ats_vector");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }
        
        template struct ats_vector_grammar<request_iterator_t,request_skipper_t>;
}
