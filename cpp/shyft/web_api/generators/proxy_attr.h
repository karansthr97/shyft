#pragma once
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/request_handler.h>

namespace shyft::web_api::generator {

    using shyft::web_api::energy_market::proxy_attr_range;
    /** @brief Emitter class for the boost variant containing
     * all employed types for proxy attributes
     *
     * @tparam OutputIterator
     */
    template<class OutputIterator>
    struct emit<OutputIterator, proxy_attr_range> {
        emit(OutputIterator& oi, proxy_attr_range const& pa) {
            boost::apply_visitor(emit_visitor(&oi), pa);
        }
    };
    x_emit_vec(proxy_attr_range);


}
