#!/usr/bin/env bash
#
# Notice that this file could need customization to adapt to the server that it runs.
#
#
ltm_root_dir=/home/energycorp.com/srv_ltmapprod
web_doc_root=${ltm_root_dir}/STM-AP/web
port_num=30010
api_port_num=30011
log_file=${ltm_root_dir}/STM-AP/log/dstm_server.log
log_dir=${ltm_root_dir}/STM-AP/log
# with the above parameters set, start the action
#
cd ${log_dir}
source ${ltm_root_dir}/miniconda/etc/profile.d/conda.sh
conda activate _ltm
# parameter to ensure we limit vmem usage so that we avoid oom killer:
# not needed for model-store(yet). : export MALLOC_ARENA_MAX=1
python -u -c "from shyft.energy_market.service.sts_server_boot import start_service; start_service(port_num=${port_num}, doc_root='${web_doc_root}', api_port_num=${api_port_num}, log_file='${log_file}', server_log_root='${log_dir}')"
