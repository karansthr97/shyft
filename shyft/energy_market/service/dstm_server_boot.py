from shyft.energy_market import stm
from typing import Dict
import logging
import time
from shyft.energy_market.service.boot import _configure_logger, Exit


def start_service(port_num: int,
                  doc_root: str,
                  api_port_num: int,
                  log_file: str,
                  server_log_root: str,
                  containers: Dict[str, str] = {}):
    """
    This function start the DStmServer instance for StmSystems.

    :param port_num: Port number to serve StmSystems
    :param doc_root: Path to root directory for web documents.
    :param api_port_num: Port number for the web API.
    :param log_file: Log file
    :param server_log_root: Directory where the server should store log files from optimization runs &c.
    :return: None
    """
    _configure_logger(log_file)
    log = logging.getLogger('')

    log.info(f'Starting server on port {port_num}.')
    srv = stm.DStmServer(server_log_root)
    srv.set_listening_port(port_num)
    for cname, cpath in containers.items():
        srv.add_container(cname, cpath)
    srv.start_server()
    log.info(f'Starting web API on port {api_port_num}.')
    srv.start_web_api(host_ip='0.0.0.0', port=api_port_num, doc_root=doc_root)

    ex = Exit()
    while True:
        time.sleep(1)
        if ex.now:
            break

    log.info(f'terminating services')
    srv.stop_web_api()
    srv.close()
