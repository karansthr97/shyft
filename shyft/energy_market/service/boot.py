import signal
import time
import logging
from pathlib import Path
from ..core import Server, RunServer
from ..stm import HpsServer, StmServer


def start_service(*, model_root: str, log_file: str) -> None:
    """
    This function starts the model-repository services for
    core, and stm hps and stm models.

    It starts the socket-servers on ports 30006(core),30007(stm_hps),30008(stm_sys),30009(run)


    :param model_root: points to the root directory
    :param log_file: log-file
    :return: None
    """
    _configure_logger(log_file)
    log = logging.getLogger('')

    root_path = Path(model_root)
    if not root_path.exists():
        root_path.mkdir()

    def mk_service(srv, sub_dir: str, port: int):
        m_root = root_path / Path(sub_dir)
        log.info(f'Creating service named {srv.__name__} at port {port}, model_root={m_root}')
        _ms = srv(str(m_root))
        _ms.set_listening_port(port)
        return _ms

    log.info("creating services")
    services = [mk_service(Server, 'core_mdl', 30006),
                mk_service(HpsServer, 'stm_hps', 30007),
                mk_service(StmServer, 'stm_sys', 30008),
                mk_service(RunServer, 'run', 30009)
                ]
    for s in services:
        s.start_server()

    ex = Exit()
    t_report = time.time()
    report_interval = 5 * 60  # emmit log entry every x min
    while True:
        time.sleep(1)
        if ex.now:
            break
        if time.time() - t_report > report_interval:
            srv_status = [s.is_running() for s in services]
            log.info(f'service_status_running:{srv_status}')
            t_report = time.time()

    log.info(f'terminating services')
    for ms in services:
        log.info(f'Terminating {ms.__class__.__name__}@{ms.get_listening_port()} ')
        ms.stop_server(1000)


def _configure_logger(filename: str = None, filemode: str = "w", level: int = logging.DEBUG,
                      log_format: str = '[%(asctime)s] %(name)-12s %(levelname)-8s %(message)s',
                      datefmt: str = '%m-%d %H:%M:%S'):
    for h in logging.root.handlers[:]:
        logging.root.removeHandler(h)
    logging.basicConfig(level=level,
                        format=log_format,
                        datefmt=datefmt,
                        filename=filename,
                        filemode=filemode)
    if filename is not None:
        console = logging.StreamHandler()
        console.setLevel(level)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)-8s: %(name)-12s %(message)s',
                                      datefmt='%m-%d %H:%M:%S')
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)


class Exit:
    """
    A small helper-class to raise a flag if SIGINT or SIGTERM is received (from aqua project)
    """
    now = False
    signum = 0

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit)
        signal.signal(signal.SIGTERM, self.exit)

    def exit(self, signum, frame):
        self.now = True
        self.signum = signum
