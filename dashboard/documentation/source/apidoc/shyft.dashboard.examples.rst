
shyft.dashboard.examples
========================


.. automodule:: shyft.dashboard.examples
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.examples.app
   shyft.dashboard.examples.basic_bokeh_app
   shyft.dashboard.examples.date_selector
   shyft.dashboard.examples.gates
   shyft.dashboard.examples.label_selector
   shyft.dashboard.examples.message_viewer
   shyft.dashboard.examples.multi_select_tabs
   shyft.dashboard.examples.ports
   shyft.dashboard.examples.selector_model
   shyft.dashboard.examples.selector_views
   shyft.dashboard.examples.slider_selector
   shyft.dashboard.examples.time_series_period_selector
   shyft.dashboard.examples.time_series_viewer
   shyft.dashboard.examples.view_period_selector
