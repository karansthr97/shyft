
shyft.dashboard.time\_series.sources
====================================


.. automodule:: shyft.dashboard.time_series.sources
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.time_series.sources.source
   shyft.dashboard.time_series.sources.ts_adapter
