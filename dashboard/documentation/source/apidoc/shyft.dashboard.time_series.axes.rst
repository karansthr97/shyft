time\_series.axes
=================

.. automodule:: shyft.dashboard.time_series.axes
   :members:
   :undoc-members:
   :show-inheritance:
