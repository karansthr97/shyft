import {Component, OnInit} from '@angular/core';

import {TimeSeriesService} from '../time-series.service';
import {MessageService} from '../message.service';
import {TimeSeries, TsInfo} from '../ts-info';

import * as Highcharts from 'highcharts';
import * as Highstocks from 'highcharts/highstock';
import {Observable, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Time} from '@angular/common';

declare var require: any;
const Boost = require('highcharts/modules/boost');
const noData = require('highcharts/modules/no-data-to-display');
const More = require('highcharts/highcharts-more');

// Boost(Highcharts);
// noData(Highcharts);
// More(Highcharts);
// noData(Highcharts);

@Component({
  selector: 'app-ts-graph',
  templateUrl: './ts-graph.component.html',
  styleUrls: ['./ts-graph.component.css']
})
export class TsGraphComponent implements OnInit {
  // highcharts = Highcharts;
  public options: any = {
    chart: {
      type: 'line',
      height: 700,
      styledMode: false, // Ideally we'd want styling somewhere else, but ok for now.
    },

    // For the scale buttons and input fields
    rangeSelector: {
      enabled: true,
      selected: 1,
      buttons: [
        {
          type: 'month',
          count: 1,
          text: '1mo'
        },
        {
          type: 'ytd',
          text: 'YTD'
        },
        {
          type: 'all',
          text: 'All'
        }
      ],
      inputEnabled: true,
      inputDateFormat: '%e-%b-%Y',
      inputEditDateFormat: '%e-%b-%Y',
      inputDateParser(value) {
        value = value.split(/[-]/);
        const result = Date.UTC(
          value[2],
          value[1],
          value[0]
        ) / 1000;
        return result;
      },
    },

    // For the bottom chart, showing the plot in miniature, but with full range.
    navigator: {
      enabled: true,
      maskFill: 'rgba(0, 198, 220, 0.1)',
      xAxis: {
        type: '',
        labels: {
          formatter() {
            return Highstocks.dateFormat('%e %b %y', this.value * 1000);
          }
        }
      },
      handles: {}
    },

    title: {
      text: 'Plot of time series'
    },

    credits: {
      enabled: false
    },

    // For what appears when hovering over the plot.
    tooltip: {
      formatter() {
        return 'x: ' + Highstocks.dateFormat('%e %b %y %H:%M:%S', this.x * 1000)
          + ' y: ' + this.y.toFixed(2);
      }
    },
    xAxis: {
      type: '',
      labels: {
        formatter() {
          return Highstocks.dateFormat('%e %b %y', this.value * 1000);
        }
      }
    },
    plotOptions: {
      linewidth: 1,
      states: {
        hover: {
          linewidth: 1.0,
        },
      },
    },
    series: [
      {
        name: '',
        turboThreshold: 500000,
        type: 'line',
        className: 'time-series-line',
        color: '#88cc00',
        data: [[]]
      },
    ]
  };

  private chart: Highstocks.Chart;
  private currentSubscription: Subscription = null;
  constructor(
    private timeSeriesService: TimeSeriesService,
    private messageService: MessageService
  ) {
  }


  ngOnInit() {
    // TODO: instead provide a subject from this class that other can insert selection into
    this.timeSeriesService.selectedTsInfo.pipe(
      distinctUntilChanged(), // so only when new selection arrives
    ).subscribe(
      tsInfo => { // when a new valid tsInfo is selected, then trigger fetch of the ts
        if (this.currentSubscription) { // unsubscribe current time-series
          this.currentSubscription.unsubscribe();
        }
        this.currentSubscription = this.timeSeriesService.getTimeSeries(tsInfo) // returns an observable that will feed us values
          .subscribe( // we subscribe, and get a subscription to changes
            tsv => {
              const timeSeries = tsv[0]; // for now, just pick the first one, later: add to list
              this.chart.series[0].setData(timeSeries.data, true);
            }
          );
      });
    this.chart = Highstocks.stockChart('container', this.options);
  }

  private log(message: string) {
    this.messageService.add(`TsGraphComponent: ${message}`);
  }

}
