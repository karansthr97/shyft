import { Component, OnInit } from '@angular/core';
import {Observable, Subscription, Subject} from 'rxjs';
import {TimeSeriesService} from '../time-series.service';
import {distinctUntilChanged} from 'rxjs/operators';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-ts-table',
  templateUrl: './ts-table.component.html',
  styleUrls: ['./ts-table.component.css']
})
export class TsTableComponent implements OnInit {
  private currentSubscription: Subscription = null;

  constructor(
    private timeSeriesService: TimeSeriesService,
    private messageService: MessageService
  ) {
        // this.rows = new Subject<any[]>();
  }

  ngOnInit() {
      this.timeSeriesService.selectedTsInfo.pipe(
        distinctUntilChanged(), // so only when new selection arrives
      ).subscribe(
        tsInfo => { // when a new valid tsInfo is selected, then trigger fetch of the ts
          if (this.currentSubscription) { // unsubscribe current time-series
            this.currentSubscription.unsubscribe();
          }
          // this.messageService.add('got a new ts')
          this.currentSubscription = this.timeSeriesService.getTimeSeries(tsInfo) // returns an observable that will feed us values
            .subscribe( // we subscribe, and get a subscription to changes
              tsv => {
                const timeSeries = tsv[0]; // for now, just pick the first one, later: add to list
                // this.messageService.add('got ts data ');
                // this.rows = timeSeries.data;
              }
            );
        });
  }

}
