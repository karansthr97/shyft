from shyft.energy_market import stm
import argparse
from create_mock_stm_system import create_filled_stm_model


class StmService:
    def __init__(self, port_number: int, doc_root: str, api_port_num: int):
        self.srv = stm.DStmServer()
        self.srv.set_listening_port(port_number)
        self.client = None
        self.doc_root = doc_root

    def __enter__(self):
        self.srv.start_server()
        self.srv.start_web_api("0.0.0.0", 44445, self.doc_root, 1, 1)
        self.client = stm.DStmClient(host_port=f"localhost:{self.srv.get_listening_port()}", timeout_ms=1000)
        print(f"Started DStmServer with web API.")
        return self.client

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.srv.stop_web_api()
        print(f"Stopped DStmServer and web API.")
        self.client.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Argument parser for starting STM server")
    parser.add_argument('--port-num', metavar='port_num', type=int, nargs='+',
                        help="Port number for server", default=4444)
    parser.add_argument('--doc-root', metavar='doc_root', type=str, nargs='+',
                        help="Document root to serve web api", default="")
    parser.add_argument('--web-port', metavar='web_port', default=4445,
                        help="Port number for web api", type=int, nargs='+')

    args = parser.parse_args()
    with StmService(args.port_num, args.doc_root, args.web_port) as client:
        # add a filled model:
        mdl = create_filled_stm_model(id=1, name="test_model")
        client.add_model(mdl.name, mdl)
        print(f"Added model to DStmServer.")
        while True:
            pass
